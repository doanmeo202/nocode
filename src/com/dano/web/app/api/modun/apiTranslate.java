package com.dano.web.app.api.modun;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.darkprograms.speech.translator.GoogleTranslate;
public class apiTranslate {
	public List<Map> doTranslate(String LANGUAGE,String TEXT) {
		List<Map> vo = new ArrayList();
		Map map = new HashMap();
		try {
			String result=translateText(LANGUAGE, TEXT);
			map.put("returnCode", 0);
			map.put("data", result);		
		
		} catch (Exception e) {
			map.put("returnCode", -1);
			map.put("data", null);
			e.printStackTrace();
		}
		vo.add(map);
		return vo;
	}
	public String translateText(String language, String text) throws IOException {
	    int wordsPerBatch = 100;
	    if(!"".equals(language)) {
	    	language="en";
	    }
	    String[] words = text.split("\\s+");
	    StringBuilder result = new StringBuilder();
	    for (int i = 0; i < words.length; i += wordsPerBatch) {
	        int end = Math.min(i + wordsPerBatch, words.length);
	        String[] batch = Arrays.copyOfRange(words, i, end);
	        String batchText = String.join(" ", batch);
	        String translationResult =    GoogleTranslate.translate(language, batchText);
	        result.append(translationResult);
            result.append(" ");
	    }
	    return result.toString().trim();
	}
	public static void main(String[] args) throws IOException {
		apiTranslate api= new apiTranslate();
		System.out.println(api.doTranslate("en", "mỗi tháng vào lúc 6 giờ sáng ngày thứ 6"));
	}
}

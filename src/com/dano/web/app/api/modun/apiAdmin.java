package com.dano.web.app.api.modun;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.modun.AAB0_0100_mod;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class apiAdmin {
	private String Select_Menu = "com.telegram.template.getmenuwebsite";
	private String getDTZSAP03 = "com.system.apisevice.getDTZSAP03";
	private String getDTZSAP02 = "com.system.apisevice.getDTZSAP02";
	private String getDTZSAP01 = "com.system.apisevice.getDTZSAP01";
	private String getURI = "com.system.apisevice.getURI";
	private static Map mapMethod=new HashMap();
	static {
		mapMethod.put("I", "");
		mapMethod.put("U", "");
		mapMethod.put("D", "");
	}
	public List<Map<String, Object>> getMenu(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		List<Map<String, Object>> li = new ArrayList();
		Map map = new HashMap();
		ds.setField("WEB_USED", webUsed);
		map.put("menu",ds.searchAndRetrieve(Select_Menu));
		li.add(map);
		return li;
	}
	public List<Map<String, Object>> getApiList(String webUsed) {
		List<Map<String, Object>> li = new ArrayList();
		JdbcHelper ds = new JdbcHelper();
		Map map = new HashMap();
		Map set = new HashMap();
		AAB0_0100_mod mod = new AAB0_0100_mod();
		// map.put("menu", mod.getMenu(webUsed));
		map.put("getDTZSAP03", ds.getDataFromKey(getDTZSAP03, set));
		map.put("getDTZSAP02", ds.getDataFromKey(getDTZSAP02, set));
		map.put("getDTZSAP01", ds.getDataFromKey(getDTZSAP01, set));
		map.put("getURI", ds.getDataFromKey(getURI, set));

		li.add(map);
		return li;
	}

	public List<Map<String, Object>> setDTZSAP01(String API_KEY, String URI_PATH, String NEED_LOG, String NEED_CHECK,String STATUS) throws Exception {
		List<Map<String, Object>> li = new ArrayList();
		// com.system.apisevice.setDTZSAP01
		JdbcHelper ds = new JdbcHelper();
		Map map = new HashMap();
		Map set = new HashMap();
		AAB0_0100_mod mod = new AAB0_0100_mod();
		
		ds.setField("API_KEY", API_KEY);
		ds.setField("NEED_CHECK", NEED_CHECK);
		ds.setField("NEED_LOG", NEED_LOG);
		ds.setField("URI_PATH", URI_PATH);
	   
		try {
			int insert=ds.update("com.system.apisevice.setDTZSAP01");
			if (insert >= 0) {
				map.put("errCode", "0");
			} else if(insert==-2){
				throw new Exception("Dữ liệu đã tồn tại");
			}
		} catch (Exception e) {
			map.put("errCode", "-1");
			
			throw e;
		}

		li.add(map);
		return li;
	}

	public List<Map<String, Object>> setDTZSAP02(String API_KEY, String CALL_MOD, String CALL_METHOD,
			String RETURN_TYPE,String STATUS) {
		List<Map<String, Object>> li = new ArrayList();
		// com.system.apisevice.setDTZSAP02
		JdbcHelper ds = new JdbcHelper();
		Map map = new HashMap();
		Map set = new HashMap();
		set.put("API_KEY", API_KEY);
		set.put("CALL_MOD", CALL_MOD);
		set.put("CALL_METHOD", CALL_METHOD);
		set.put("RETURN_TYPE", RETURN_TYPE);
		try {
			if (ds.setDataFromKey("com.system.apisevice.setDTZSAP02", set) >= 0) {
				map.put("errCode", "0");
			} else {
				map.put("errCode", "-1");
			}
		} catch (Exception e) {
			map.put("errCode", "-1");
			throw e;
		}

		li.add(map);
		return li;
	}

	public List<Map<String, Object>> setDTZSAP03(String API_KEY, String CALL_MOD, String CALL_METHOD, String SER_NO,
			String PARAM_NAME, String PARAM_TYPE,String STATUS) {
		List<Map<String, Object>> li = new ArrayList();
		// com.system.apisevice.setDTZSAP03

		JdbcHelper ds = new JdbcHelper();
		Map map = new HashMap();
		Map set = new HashMap();
		set.put("API_KEY", API_KEY);
		set.put("CALL_MOD", CALL_MOD);
		set.put("CALL_METHOD", CALL_METHOD);
		set.put("SER_NO", SER_NO);
		set.put("PARAM_NAME", PARAM_NAME);
		set.put("PARAM_TYPE", PARAM_TYPE);
		try {
			if (ds.setDataFromKey("com.system.apisevice.setDTZSAP03", set) >= 0) {
				map.put("errCode", "0");
			} else {
				map.put("errCode", "-1");
			}
		} catch (Exception e) {
			map.put("errCode", "-1");
			throw e;
		}

		li.add(map);
		return li;
	}

	public static void main(String[] args) {
		apiAdmin admin = new apiAdmin();
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(admin.getMenu("Webdano")));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

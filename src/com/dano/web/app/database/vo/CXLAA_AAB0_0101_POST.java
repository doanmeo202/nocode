package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0101_POST {
  private String POST_ID;

  private String POST_TITLE;

  private String CATE_ID;

  private String IMAGE;

  private String POST_CONT;

  private String AUTH;

  private String VIEWS;

  private String CREATE_TIME;

  public String getPOST_ID() {
    return this.POST_ID;
  }

  public void setPOST_ID(String POST_ID) {
    this.POST_ID =POST_ID;
  }

  public String getPOST_TITLE() {
    return this.POST_TITLE;
  }

  public void setPOST_TITLE(String POST_TITLE) {
    this.POST_TITLE =POST_TITLE;
  }

  public String getCATE_ID() {
    return this.CATE_ID;
  }

  public void setCATE_ID(String CATE_ID) {
    this.CATE_ID =CATE_ID;
  }

  public String getIMAGE() {
    return this.IMAGE;
  }

  public void setIMAGE(String IMAGE) {
    this.IMAGE =IMAGE;
  }

  public String getPOST_CONT() {
    return this.POST_CONT;
  }

  public void setPOST_CONT(String POST_CONT) {
    this.POST_CONT =POST_CONT;
  }

  public String getAUTH() {
    return this.AUTH;
  }

  public void setAUTH(String AUTH) {
    this.AUTH =AUTH;
  }

  public String getVIEWS() {
    return this.VIEWS;
  }

  public void setVIEWS(String VIEWS) {
    this.VIEWS =VIEWS;
  }

  public String getCREATE_TIME() {
    return this.CREATE_TIME;
  }

  public void setCREATE_TIME(String CREATE_TIME) {
    this.CREATE_TIME =CREATE_TIME;
  }
}

package com.dano.web.app.database.vo;

import java.lang.String;

public class GOOGLE_EMAIL_SETTING_MAIL_LOG {
  private String FROM_USER;

  private String TO_USER;

  private String TIME_SEND;

  private String STATUS_SEND;

  public String getFROM_USER() {
    return this.FROM_USER;
  }

  public void setFROM_USER(String FROM_USER) {
    this.FROM_USER =FROM_USER;
  }

  public String getTO_USER() {
    return this.TO_USER;
  }

  public void setTO_USER(String TO_USER) {
    this.TO_USER =TO_USER;
  }

  public String getTIME_SEND() {
    return this.TIME_SEND;
  }

  public void setTIME_SEND(String TIME_SEND) {
    this.TIME_SEND =TIME_SEND;
  }

  public String getSTATUS_SEND() {
    return this.STATUS_SEND;
  }

  public void setSTATUS_SEND(String STATUS_SEND) {
    this.STATUS_SEND =STATUS_SEND;
  }
}

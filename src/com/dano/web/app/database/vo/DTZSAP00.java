package com.dano.web.app.database.vo;

import java.lang.String;

public class DTZSAP00 {
  private String PRIVATE_KEY;

  private String PUBLIC_KEY;

  private String PARNER_ID;

  private String STOP_TIME;

  private String IS_CHECK;

  private String REQ_COUNT;

  public String getPRIVATE_KEY() {
    return this.PRIVATE_KEY;
  }

  public void setPRIVATE_KEY(String PRIVATE_KEY) {
    this.PRIVATE_KEY =PRIVATE_KEY;
  }

  public String getPUBLIC_KEY() {
    return this.PUBLIC_KEY;
  }

  public void setPUBLIC_KEY(String PUBLIC_KEY) {
    this.PUBLIC_KEY =PUBLIC_KEY;
  }

  public String getPARNER_ID() {
    return this.PARNER_ID;
  }

  public void setPARNER_ID(String PARNER_ID) {
    this.PARNER_ID =PARNER_ID;
  }

  public String getSTOP_TIME() {
    return this.STOP_TIME;
  }

  public void setSTOP_TIME(String STOP_TIME) {
    this.STOP_TIME =STOP_TIME;
  }

  public String getIS_CHECK() {
    return this.IS_CHECK;
  }

  public void setIS_CHECK(String IS_CHECK) {
    this.IS_CHECK =IS_CHECK;
  }

  public String getREQ_COUNT() {
    return this.REQ_COUNT;
  }

  public void setREQ_COUNT(String REQ_COUNT) {
    this.REQ_COUNT =REQ_COUNT;
  }
}

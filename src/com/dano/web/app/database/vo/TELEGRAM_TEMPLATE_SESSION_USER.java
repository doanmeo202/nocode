package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_SESSION_USER {
  private String USERNAME;

  private String TOKEN;

  private String LOGIN_TIME;

  public String getUSERNAME() {
    return this.USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
    this.USERNAME =USERNAME;
  }

  public String getTOKEN() {
    return this.TOKEN;
  }

  public void setTOKEN(String TOKEN) {
    this.TOKEN =TOKEN;
  }

  public String getLOGIN_TIME() {
    return this.LOGIN_TIME;
  }

  public void setLOGIN_TIME(String LOGIN_TIME) {
    this.LOGIN_TIME =LOGIN_TIME;
  }
}

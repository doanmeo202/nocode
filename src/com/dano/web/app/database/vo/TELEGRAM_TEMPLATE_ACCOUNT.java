package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_ACCOUNT {
  private String USERNAME;

  private String PASSWORD;

  private String EMAIL;

  private String IS_ACTIVE;

  private String ROLE;

  private String FULL_NAME;

  private String BIRTHDAY;

  private String MOBILE;

  public String getUSERNAME() {
    return this.USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
    this.USERNAME =USERNAME;
  }

  public String getPASSWORD() {
    return this.PASSWORD;
  }

  public void setPASSWORD(String PASSWORD) {
    this.PASSWORD =PASSWORD;
  }

  public String getEMAIL() {
    return this.EMAIL;
  }

  public void setEMAIL(String EMAIL) {
    this.EMAIL =EMAIL;
  }

  public String getIS_ACTIVE() {
    return this.IS_ACTIVE;
  }

  public void setIS_ACTIVE(String IS_ACTIVE) {
    this.IS_ACTIVE =IS_ACTIVE;
  }

  public String getROLE() {
    return this.ROLE;
  }

  public void setROLE(String ROLE) {
    this.ROLE =ROLE;
  }

  public String getFULL_NAME() {
    return this.FULL_NAME;
  }

  public void setFULL_NAME(String FULL_NAME) {
    this.FULL_NAME =FULL_NAME;
  }

  public String getBIRTHDAY() {
    return this.BIRTHDAY;
  }

  public void setBIRTHDAY(String BIRTHDAY) {
    this.BIRTHDAY =BIRTHDAY;
  }

  public String getMOBILE() {
    return this.MOBILE;
  }

  public void setMOBILE(String MOBILE) {
    this.MOBILE =MOBILE;
  }
}

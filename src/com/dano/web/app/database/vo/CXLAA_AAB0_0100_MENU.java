package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0100_MENU {
  private String ID_MENU;

  private String NAME;

  private String SLUG;

  private String IS_MENU;

  private String SUB_MENU;

  private String POSION_MENU;

  private String DES_C;

  private String ROLE_READ;

  private String WEB_USED;

  private String TEMPLATE_URL;

  private String CLASS_ICON;

  public String getID_MENU() {
    return this.ID_MENU;
  }

  public void setID_MENU(String ID_MENU) {
    this.ID_MENU =ID_MENU;
  }

  public String getNAME() {
    return this.NAME;
  }

  public void setNAME(String NAME) {
    this.NAME =NAME;
  }

  public String getSLUG() {
    return this.SLUG;
  }

  public void setSLUG(String SLUG) {
    this.SLUG =SLUG;
  }

  public String getIS_MENU() {
    return this.IS_MENU;
  }

  public void setIS_MENU(String IS_MENU) {
    this.IS_MENU =IS_MENU;
  }

  public String getSUB_MENU() {
    return this.SUB_MENU;
  }

  public void setSUB_MENU(String SUB_MENU) {
    this.SUB_MENU =SUB_MENU;
  }

  public String getPOSION_MENU() {
    return this.POSION_MENU;
  }

  public void setPOSION_MENU(String POSION_MENU) {
    this.POSION_MENU =POSION_MENU;
  }

  public String getDES_C() {
    return this.DES_C;
  }

  public void setDES_C(String DES_C) {
    this.DES_C =DES_C;
  }

  public String getROLE_READ() {
    return this.ROLE_READ;
  }

  public void setROLE_READ(String ROLE_READ) {
    this.ROLE_READ =ROLE_READ;
  }

  public String getWEB_USED() {
    return this.WEB_USED;
  }

  public void setWEB_USED(String WEB_USED) {
    this.WEB_USED =WEB_USED;
  }

  public String getTEMPLATE_URL() {
    return this.TEMPLATE_URL;
  }

  public void setTEMPLATE_URL(String TEMPLATE_URL) {
    this.TEMPLATE_URL =TEMPLATE_URL;
  }

  public String getCLASS_ICON() {
    return this.CLASS_ICON;
  }

  public void setCLASS_ICON(String CLASS_ICON) {
    this.CLASS_ICON =CLASS_ICON;
  }
}

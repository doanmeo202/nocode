package com.dano.web.app.database.vo;

import java.lang.String;

public class SYSTEM_BATCH_INFO {
  private String job_group;

  private String job_info;

  private String job_class;

  private String trigger_group;

  private String trigger_name;

  private String job_name;

  private String job_description;

  private String job_data;

  private String prev_fire_time;

  private String ip_action;

  private String id;

  private String trigger_cron_expression;

  private String next_fire_time;

  public String getjob_group() {
    return this.job_group;
  }

  public void setjob_group(String job_group) {
    this.job_group =job_group;
  }

  public String getjob_info() {
    return this.job_info;
  }

  public void setjob_info(String job_info) {
    this.job_info =job_info;
  }

  public String getjob_class() {
    return this.job_class;
  }

  public void setjob_class(String job_class) {
    this.job_class =job_class;
  }

  public String gettrigger_group() {
    return this.trigger_group;
  }

  public void settrigger_group(String trigger_group) {
    this.trigger_group =trigger_group;
  }

  public String gettrigger_name() {
    return this.trigger_name;
  }

  public void settrigger_name(String trigger_name) {
    this.trigger_name =trigger_name;
  }

  public String getjob_name() {
    return this.job_name;
  }

  public void setjob_name(String job_name) {
    this.job_name =job_name;
  }

  public String getjob_description() {
    return this.job_description;
  }

  public void setjob_description(String job_description) {
    this.job_description =job_description;
  }

  public String getjob_data() {
    return this.job_data;
  }

  public void setjob_data(String job_data) {
    this.job_data =job_data;
  }

  public String getprev_fire_time() {
    return this.prev_fire_time;
  }

  public void setprev_fire_time(String prev_fire_time) {
    this.prev_fire_time =prev_fire_time;
  }

  public String getip_action() {
    return this.ip_action;
  }

  public void setip_action(String ip_action) {
    this.ip_action =ip_action;
  }

  public String getid() {
    return this.id;
  }

  public void setid(String id) {
    this.id =id;
  }

  public String gettrigger_cron_expression() {
    return this.trigger_cron_expression;
  }

  public void settrigger_cron_expression(String trigger_cron_expression) {
    this.trigger_cron_expression =trigger_cron_expression;
  }

  public String getnext_fire_time() {
    return this.next_fire_time;
  }

  public void setnext_fire_time(String next_fire_time) {
    this.next_fire_time =next_fire_time;
  }
}

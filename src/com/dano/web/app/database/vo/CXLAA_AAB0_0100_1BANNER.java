package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0100_1BANNER {
  private String ID_BANNER;

  private String ID_WEB;

  private String INDEX_BANNER;

  private String TITLE;

  private String SUB_TITLE;

  private String BUTTON_TITLE;

  private String IMAGE_URL;

  public String getID_BANNER() {
    return this.ID_BANNER;
  }

  public void setID_BANNER(String ID_BANNER) {
    this.ID_BANNER =ID_BANNER;
  }

  public String getID_WEB() {
    return this.ID_WEB;
  }

  public void setID_WEB(String ID_WEB) {
    this.ID_WEB =ID_WEB;
  }

  public String getINDEX_BANNER() {
    return this.INDEX_BANNER;
  }

  public void setINDEX_BANNER(String INDEX_BANNER) {
    this.INDEX_BANNER =INDEX_BANNER;
  }

  public String getTITLE() {
    return this.TITLE;
  }

  public void setTITLE(String TITLE) {
    this.TITLE =TITLE;
  }

  public String getSUB_TITLE() {
    return this.SUB_TITLE;
  }

  public void setSUB_TITLE(String SUB_TITLE) {
    this.SUB_TITLE =SUB_TITLE;
  }

  public String getBUTTON_TITLE() {
    return this.BUTTON_TITLE;
  }

  public void setBUTTON_TITLE(String BUTTON_TITLE) {
    this.BUTTON_TITLE =BUTTON_TITLE;
  }

  public String getIMAGE_URL() {
    return this.IMAGE_URL;
  }

  public void setIMAGE_URL(String IMAGE_URL) {
    this.IMAGE_URL =IMAGE_URL;
  }
}

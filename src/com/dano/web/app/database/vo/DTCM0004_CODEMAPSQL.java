package com.dano.web.app.database.vo;

import java.lang.String;

public class DTCM0004_CODEMAPSQL {
  private String CODE_NAME;

  private String SQLSTRING;

  private String FLAG;

  public String getCODE_NAME() {
    return this.CODE_NAME;
  }

  public void setCODE_NAME(String CODE_NAME) {
    this.CODE_NAME =CODE_NAME;
  }

  public String getSQLSTRING() {
    return this.SQLSTRING;
  }

  public void setSQLSTRING(String SQLSTRING) {
    this.SQLSTRING =SQLSTRING;
  }

  public String getFLAG() {
    return this.FLAG;
  }

  public void setFLAG(String FLAG) {
    this.FLAG =FLAG;
  }
}

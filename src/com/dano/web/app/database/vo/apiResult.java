package com.dano.web.app.database.vo;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.dano.web.app.aa.b0.helper.VoTooL;
import com.fasterxml.jackson.databind.ObjectMapper;

public class apiResult<Object> {
	public interface ReturnCodeMap {
		/** 成功 */
		static final int SUCCESS = 0;
		/** 資料找不到 */
		static final int DATA_NOT_FOUND = -1;
		/** 資料檢核失敗 */
		static final int DATA_CHECK_FAIL = 1;
		/** 失敗 */
		static final int FAIL = 2;
		/** 錯誤訊息長度(db定義的長度) */
		static final int MESSAGE_LEN = 200;

	}

	private int returnCode;
	private int returnCodePage;
	private String returnMess;
	private String returnEx;
	private String status;
	private Object data;
	private HttpServletRequest request;
	private HttpServletResponse response;

	private HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	private HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public int getReturnCodePage() {
		return returnCodePage;
	}

	public void setReturnCodePage(int returnCodePage) {
		this.returnCodePage = returnCodePage;
	}

	public String getReturnMess() {
		return returnMess;
	}

	public void setReturnMess(String returnMess) {
		this.returnMess = returnMess;
	}

	public String getReturnEx() {
		return returnEx;
	}

	public void setReturnEx(String returnEx) {
		this.returnEx = returnEx;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public apiResult() {

	}

	public apiResult getOutputData(int returnCode, String returnMess, String returnEx, Object data,
			HttpServletRequest request, HttpServletResponse response) {

		if (request != null) {
			if (this.apiResult(request, response).getReturnCode() != 0) {
				return apiResult(request, response);
			}
			;
		} else {
			this.returnCode = returnCode;
			this.returnMess = returnMess;
			this.returnEx = returnEx;
			if (returnCode == ReturnCodeMap.SUCCESS) {
				this.status = "SUCCESS";
				this.returnCodePage = 200;
			} else if (returnCode == ReturnCodeMap.DATA_NOT_FOUND) {
				this.status = "DATA_NOT_FOUND";
				this.returnCodePage = 400;
			} else if (returnCode == ReturnCodeMap.DATA_CHECK_FAIL) {
				this.status = "DATA_CHECK_FAIL";
				this.returnCodePage = 400;
			} else if (returnCode == ReturnCodeMap.FAIL) {
				this.status = "FAIL";
				this.returnCodePage = 400;
			}
			this.data = data;
		}

		return this;
	}

	public apiResult apiResult(HttpServletRequest request, HttpServletResponse response) {
		String ipAddressClient = ObjectUtils.toString(request.getHeader("X-Real-IP"));
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		ObjectMapper mapper = new ObjectMapper();
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();

			if ("103.130.213.10".equals(ipAddress) && !"".equals(ipAddressClient)) {
				ipAddress = ipAddressClient;
			} else {
				if ("0:0:0:0:0:0:0:1".equals(ipAddress)) {
//					response.setStatus(HttpServletResponse.SC_ACCEPTED);
//					this.status = "SC_ACCEPTED";
//					this.returnCodePage = HttpServletResponse.SC_ACCEPTED;
//					this.returnCode=3;
//					this.returnMess="connect with localhost!!!";
//					return this;
					System.out.println("connect with localhost!!!");
				}
			}
		}
		if (StringUtils.isBlank(ipAddress)) {
			String responseToClient = "Từ chối kết nối, IP bạn đang truy cập bất hợp pháp đến hệ thống.";
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
			this.status = "SC_ACCEPTED";
			this.returnCodePage = HttpServletResponse.SC_ACCEPTED;
			this.returnCode = 3;
			this.returnMess = responseToClient;
			// response.getWriter().write(mapper.writeValueAsString(this));
			return this;
		}

		String url = request.getRequestURI();
		if (StringUtils.isBlank(url)) {
			String responseToClient = "Từ chối kết nối, Khởi tạo không có tham số. url : " + url;
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			this.status = "SC_ACCEPTED";
			this.returnCodePage = HttpServletResponse.SC_BAD_REQUEST;
			this.returnCode = 3;
			this.returnMess = responseToClient;
			// response.getWriter().write(mapper.writeValueAsString(this));
			return this;
		}

		int indexUrl = url.indexOf("/api/");
		if (indexUrl == -1) {
			String responseToClient = "Từ chối kết nối, Khởi tạo không có tham số.";
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			this.status = "SC_ACCEPTED";
			this.returnCodePage = HttpServletResponse.SC_BAD_REQUEST;
			this.returnCode = 3;
			this.returnMess = responseToClient;
			// response.getWriter().write(mapper.writeValueAsString(this));
			return this;
		}
		return this;
	}

	public static void main(String[] args) {

	}
}

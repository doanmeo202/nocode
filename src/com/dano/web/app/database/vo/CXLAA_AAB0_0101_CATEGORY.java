package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0101_CATEGORY {
  private String SLUG_ID;

  private String NAME;

  private String TYPE;

  private String DESCRIP;

  private String IMAGE;

  private String USER;

  public String getSLUG_ID() {
    return this.SLUG_ID;
  }

  public void setSLUG_ID(String SLUG_ID) {
    this.SLUG_ID =SLUG_ID;
  }

  public String getNAME() {
    return this.NAME;
  }

  public void setNAME(String NAME) {
    this.NAME =NAME;
  }

  public String getTYPE() {
    return this.TYPE;
  }

  public void setTYPE(String TYPE) {
    this.TYPE =TYPE;
  }

  public String getDESCRIP() {
    return this.DESCRIP;
  }

  public void setDESCRIP(String DESCRIP) {
    this.DESCRIP =DESCRIP;
  }

  public String getIMAGE() {
    return this.IMAGE;
  }

  public void setIMAGE(String IMAGE) {
    this.IMAGE =IMAGE;
  }

  public String getUSER() {
    return this.USER;
  }

  public void setUSER(String USER) {
    this.USER =USER;
  }
}

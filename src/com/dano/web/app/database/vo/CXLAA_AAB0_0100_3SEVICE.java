package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0100_3SEVICE {
  private String ID;

  private String WEB_ID;

  private String TITLE;

  private String SUB_TITLE;

  private String CONTENT;

  private String IMAGE_URL;

  private String TYPE;

  private String ICON_CLASS;

  public String getID() {
    return this.ID;
  }

  public void setID(String ID) {
    this.ID =ID;
  }

  public String getWEB_ID() {
    return this.WEB_ID;
  }

  public void setWEB_ID(String WEB_ID) {
    this.WEB_ID =WEB_ID;
  }

  public String getTITLE() {
    return this.TITLE;
  }

  public void setTITLE(String TITLE) {
    this.TITLE =TITLE;
  }

  public String getSUB_TITLE() {
    return this.SUB_TITLE;
  }

  public void setSUB_TITLE(String SUB_TITLE) {
    this.SUB_TITLE =SUB_TITLE;
  }

  public String getCONTENT() {
    return this.CONTENT;
  }

  public void setCONTENT(String CONTENT) {
    this.CONTENT =CONTENT;
  }

  public String getIMAGE_URL() {
    return this.IMAGE_URL;
  }

  public void setIMAGE_URL(String IMAGE_URL) {
    this.IMAGE_URL =IMAGE_URL;
  }

  public String getTYPE() {
    return this.TYPE;
  }

  public void setTYPE(String TYPE) {
    this.TYPE =TYPE;
  }

  public String getICON_CLASS() {
    return this.ICON_CLASS;
  }

  public void setICON_CLASS(String ICON_CLASS) {
    this.ICON_CLASS =ICON_CLASS;
  }
}

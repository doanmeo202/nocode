package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_RESGIN_TOKEN {
  private String TOKEN;

  private String ID;

  private String IS_ACTIVE;

  private String IS_DELETE;

  private String START_DATE;

  private String END_DATE;

  private String USERNAME;

  private String CREATE_DATE;

  private String STATUS;

  private String USER_CHATID;

  private String USER_INFOR;

  public String getTOKEN() {
    return this.TOKEN;
  }

  public void setTOKEN(String TOKEN) {
    this.TOKEN =TOKEN;
  }

  public String getID() {
    return this.ID;
  }

  public void setID(String ID) {
    this.ID =ID;
  }

  public String getIS_ACTIVE() {
    return this.IS_ACTIVE;
  }

  public void setIS_ACTIVE(String IS_ACTIVE) {
    this.IS_ACTIVE =IS_ACTIVE;
  }

  public String getIS_DELETE() {
    return this.IS_DELETE;
  }

  public void setIS_DELETE(String IS_DELETE) {
    this.IS_DELETE =IS_DELETE;
  }

  public String getSTART_DATE() {
    return this.START_DATE;
  }

  public void setSTART_DATE(String START_DATE) {
    this.START_DATE =START_DATE;
  }

  public String getEND_DATE() {
    return this.END_DATE;
  }

  public void setEND_DATE(String END_DATE) {
    this.END_DATE =END_DATE;
  }

  public String getUSERNAME() {
    return this.USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
    this.USERNAME =USERNAME;
  }

  public String getCREATE_DATE() {
    return this.CREATE_DATE;
  }

  public void setCREATE_DATE(String CREATE_DATE) {
    this.CREATE_DATE =CREATE_DATE;
  }

  public String getSTATUS() {
    return this.STATUS;
  }

  public void setSTATUS(String STATUS) {
    this.STATUS =STATUS;
  }

  public String getUSER_CHATID() {
    return this.USER_CHATID;
  }

  public void setUSER_CHATID(String USER_CHATID) {
    this.USER_CHATID =USER_CHATID;
  }

  public String getUSER_INFOR() {
    return this.USER_INFOR;
  }

  public void setUSER_INFOR(String USER_INFOR) {
    this.USER_INFOR =USER_INFOR;
  }
}

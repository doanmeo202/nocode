package com.dano.web.app.database.vo;

import java.lang.String;

public class DTCM0004_FIELD_OPTION_LIST {
  private String SYSID;

  private String FIELD_NAME;

  private String SETTING;

  private String NAME;

  private String FIELD_DESCRIPTION;

  private String SER_NO;

  public String getSYSID() {
    return this.SYSID;
  }

  public void setSYSID(String SYSID) {
    this.SYSID =SYSID;
  }

  public String getFIELD_NAME() {
    return this.FIELD_NAME;
  }

  public void setFIELD_NAME(String FIELD_NAME) {
    this.FIELD_NAME =FIELD_NAME;
  }

  public String getSETTING() {
    return this.SETTING;
  }

  public void setSETTING(String SETTING) {
    this.SETTING =SETTING;
  }

  public String getNAME() {
    return this.NAME;
  }

  public void setNAME(String NAME) {
    this.NAME =NAME;
  }

  public String getFIELD_DESCRIPTION() {
    return this.FIELD_DESCRIPTION;
  }

  public void setFIELD_DESCRIPTION(String FIELD_DESCRIPTION) {
    this.FIELD_DESCRIPTION =FIELD_DESCRIPTION;
  }

  public String getSER_NO() {
    return this.SER_NO;
  }

  public void setSER_NO(String SER_NO) {
    this.SER_NO =SER_NO;
  }
}

package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_GROUP {
  private String GROUP_ID;

  private String GROUP_TITLE;

  public String getGROUP_ID() {
    return this.GROUP_ID;
  }

  public void setGROUP_ID(String GROUP_ID) {
    this.GROUP_ID =GROUP_ID;
  }

  public String getGROUP_TITLE() {
    return this.GROUP_TITLE;
  }

  public void setGROUP_TITLE(String GROUP_TITLE) {
    this.GROUP_TITLE =GROUP_TITLE;
  }
}

package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB0_0100_2ABOUTUS {
  private String ID;

  private String WEB_ID;

  private String TYPE;

  private String TITLE;

  private String SUB_TITLE;

  private String CONTENT;

  private String ITEM1;

  private String ITEM2;

  private String ITEM3;

  private String IS_SHOW;

  public String getID() {
    return this.ID;
  }

  public void setID(String ID) {
    this.ID =ID;
  }

  public String getWEB_ID() {
    return this.WEB_ID;
  }

  public void setWEB_ID(String WEB_ID) {
    this.WEB_ID =WEB_ID;
  }

  public String getTYPE() {
    return this.TYPE;
  }

  public void setTYPE(String TYPE) {
    this.TYPE =TYPE;
  }

  public String getTITLE() {
    return this.TITLE;
  }

  public void setTITLE(String TITLE) {
    this.TITLE =TITLE;
  }

  public String getSUB_TITLE() {
    return this.SUB_TITLE;
  }

  public void setSUB_TITLE(String SUB_TITLE) {
    this.SUB_TITLE =SUB_TITLE;
  }

  public String getCONTENT() {
    return this.CONTENT;
  }

  public void setCONTENT(String CONTENT) {
    this.CONTENT =CONTENT;
  }

  public String getITEM1() {
    return this.ITEM1;
  }

  public void setITEM1(String ITEM1) {
    this.ITEM1 =ITEM1;
  }

  public String getITEM2() {
    return this.ITEM2;
  }

  public void setITEM2(String ITEM2) {
    this.ITEM2 =ITEM2;
  }

  public String getITEM3() {
    return this.ITEM3;
  }

  public void setITEM3(String ITEM3) {
    this.ITEM3 =ITEM3;
  }

  public String getIS_SHOW() {
    return this.IS_SHOW;
  }

  public void setIS_SHOW(String IS_SHOW) {
    this.IS_SHOW =IS_SHOW;
  }
}

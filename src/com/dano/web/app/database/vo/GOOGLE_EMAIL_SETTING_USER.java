package com.dano.web.app.database.vo;

import java.lang.String;

public class GOOGLE_EMAIL_SETTING_USER {
  private String USERNAME;

  private String PASSWORD;

  private String HOST;

  private String PROTOCOL;

  private String CREATE_DATE;

  private String ID;

  public String getUSERNAME() {
    return this.USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
    this.USERNAME =USERNAME;
  }

  public String getPASSWORD() {
    return this.PASSWORD;
  }

  public void setPASSWORD(String PASSWORD) {
    this.PASSWORD =PASSWORD;
  }

  public String getHOST() {
    return this.HOST;
  }

  public void setHOST(String HOST) {
    this.HOST =HOST;
  }

  public String getPROTOCOL() {
    return this.PROTOCOL;
  }

  public void setPROTOCOL(String PROTOCOL) {
    this.PROTOCOL =PROTOCOL;
  }

  public String getCREATE_DATE() {
    return this.CREATE_DATE;
  }

  public void setCREATE_DATE(String CREATE_DATE) {
    this.CREATE_DATE =CREATE_DATE;
  }

  public String getID() {
    return this.ID;
  }

  public void setID(String ID) {
    this.ID =ID;
  }
}

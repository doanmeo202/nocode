package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_USER {
  private String CHATID;

  private String USERNAME;

  private String PHONE;

  private String FROM_GROUP_BOT;

  private String IS_DISABLE;

  public String getCHATID() {
    return this.CHATID;
  }

  public void setCHATID(String CHATID) {
    this.CHATID =CHATID;
  }

  public String getUSERNAME() {
    return this.USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
    this.USERNAME =USERNAME;
  }

  public String getPHONE() {
    return this.PHONE;
  }

  public void setPHONE(String PHONE) {
    this.PHONE =PHONE;
  }

  public String getFROM_GROUP_BOT() {
    return this.FROM_GROUP_BOT;
  }

  public void setFROM_GROUP_BOT(String FROM_GROUP_BOT) {
    this.FROM_GROUP_BOT =FROM_GROUP_BOT;
  }

  public String getIS_DISABLE() {
    return this.IS_DISABLE;
  }

  public void setIS_DISABLE(String IS_DISABLE) {
    this.IS_DISABLE =IS_DISABLE;
  }
}

package com.dano.web.app.database.vo;

import java.lang.String;

public class SYSTEM_IP_ACCESS {
  private String IP;

  private String LAST_TIME_CONNECT;

  private String URL_IP_REQUEST;

  private String TOTAL_REQUET;

  private String DENY;

  public String getIP() {
    return this.IP;
  }

  public void setIP(String IP) {
    this.IP =IP;
  }

  public String getLAST_TIME_CONNECT() {
    return this.LAST_TIME_CONNECT;
  }

  public void setLAST_TIME_CONNECT(String LAST_TIME_CONNECT) {
    this.LAST_TIME_CONNECT =LAST_TIME_CONNECT;
  }

  public String getURL_IP_REQUEST() {
    return this.URL_IP_REQUEST;
  }

  public void setURL_IP_REQUEST(String URL_IP_REQUEST) {
    this.URL_IP_REQUEST =URL_IP_REQUEST;
  }

  public String getTOTAL_REQUET() {
    return this.TOTAL_REQUET;
  }

  public void setTOTAL_REQUET(String TOTAL_REQUET) {
    this.TOTAL_REQUET =TOTAL_REQUET;
  }

  public String getDENY() {
    return this.DENY;
  }

  public void setDENY(String DENY) {
    this.DENY =DENY;
  }
}

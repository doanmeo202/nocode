package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_LIST_FILE {
  private String ID;

  private String FILE_NAME;

  private String UP_TIME;

  private String USER_ID;

  private String FILE_FK;

  public String getID() {
    return this.ID;
  }

  public void setID(String ID) {
    this.ID =ID;
  }

  public String getFILE_NAME() {
    return this.FILE_NAME;
  }

  public void setFILE_NAME(String FILE_NAME) {
    this.FILE_NAME =FILE_NAME;
  }

  public String getUP_TIME() {
    return this.UP_TIME;
  }

  public void setUP_TIME(String UP_TIME) {
    this.UP_TIME =UP_TIME;
  }

  public String getUSER_ID() {
    return this.USER_ID;
  }

  public void setUSER_ID(String USER_ID) {
    this.USER_ID =USER_ID;
  }

  public String getFILE_FK() {
    return this.FILE_FK;
  }

  public void setFILE_FK(String FILE_FK) {
    this.FILE_FK =FILE_FK;
  }
}

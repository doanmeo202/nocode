package com.dano.web.app.database.vo;

import java.lang.String;

public class DTZSAP03 {
  private String API_KEY;

  private String CALL_MOD;

  private String CALL_METHOD;

  private String SER_NO;

  private String PARAM_NAME;

  private String PARAM_TYPE;

  private String IS_CONSEQUENCE;

  private String REMARK;

  private String LST_PROC_ID;

  private String LST_PROC_DATE;

  public String getAPI_KEY() {
    return this.API_KEY;
  }

  public void setAPI_KEY(String API_KEY) {
    this.API_KEY =API_KEY;
  }

  public String getCALL_MOD() {
    return this.CALL_MOD;
  }

  public void setCALL_MOD(String CALL_MOD) {
    this.CALL_MOD =CALL_MOD;
  }

  public String getCALL_METHOD() {
    return this.CALL_METHOD;
  }

  public void setCALL_METHOD(String CALL_METHOD) {
    this.CALL_METHOD =CALL_METHOD;
  }

  public String getSER_NO() {
    return this.SER_NO;
  }

  public void setSER_NO(String SER_NO) {
    this.SER_NO =SER_NO;
  }

  public String getPARAM_NAME() {
    return this.PARAM_NAME;
  }

  public void setPARAM_NAME(String PARAM_NAME) {
    this.PARAM_NAME =PARAM_NAME;
  }

  public String getPARAM_TYPE() {
    return this.PARAM_TYPE;
  }

  public void setPARAM_TYPE(String PARAM_TYPE) {
    this.PARAM_TYPE =PARAM_TYPE;
  }

  public String getIS_CONSEQUENCE() {
    return this.IS_CONSEQUENCE;
  }

  public void setIS_CONSEQUENCE(String IS_CONSEQUENCE) {
    this.IS_CONSEQUENCE =IS_CONSEQUENCE;
  }

  public String getREMARK() {
    return this.REMARK;
  }

  public void setREMARK(String REMARK) {
    this.REMARK =REMARK;
  }

  public String getLST_PROC_ID() {
    return this.LST_PROC_ID;
  }

  public void setLST_PROC_ID(String LST_PROC_ID) {
    this.LST_PROC_ID =LST_PROC_ID;
  }

  public String getLST_PROC_DATE() {
    return this.LST_PROC_DATE;
  }

  public void setLST_PROC_DATE(String LST_PROC_DATE) {
    this.LST_PROC_DATE =LST_PROC_DATE;
  }
}

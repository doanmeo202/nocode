package com.dano.web.app.database.vo;

import java.lang.String;

public class DTZSAP02 {
  private String API_KEY;

  private String START_UP;

  private String CALL_MOD;

  private String CALL_METHOD;

  private String RETURN_TYPE;

  private String REMARK;

  private String LST_PROC_ID;

  private String LST_PROC_DATE;

  public String getAPI_KEY() {
    return this.API_KEY;
  }

  public void setAPI_KEY(String API_KEY) {
    this.API_KEY =API_KEY;
  }

  public String getSTART_UP() {
    return this.START_UP;
  }

  public void setSTART_UP(String START_UP) {
    this.START_UP =START_UP;
  }

  public String getCALL_MOD() {
    return this.CALL_MOD;
  }

  public void setCALL_MOD(String CALL_MOD) {
    this.CALL_MOD =CALL_MOD;
  }

  public String getCALL_METHOD() {
    return this.CALL_METHOD;
  }

  public void setCALL_METHOD(String CALL_METHOD) {
    this.CALL_METHOD =CALL_METHOD;
  }

  public String getRETURN_TYPE() {
    return this.RETURN_TYPE;
  }

  public void setRETURN_TYPE(String RETURN_TYPE) {
    this.RETURN_TYPE =RETURN_TYPE;
  }

  public String getREMARK() {
    return this.REMARK;
  }

  public void setREMARK(String REMARK) {
    this.REMARK =REMARK;
  }

  public String getLST_PROC_ID() {
    return this.LST_PROC_ID;
  }

  public void setLST_PROC_ID(String LST_PROC_ID) {
    this.LST_PROC_ID =LST_PROC_ID;
  }

  public String getLST_PROC_DATE() {
    return this.LST_PROC_DATE;
  }

  public void setLST_PROC_DATE(String LST_PROC_DATE) {
    this.LST_PROC_DATE =LST_PROC_DATE;
  }
}

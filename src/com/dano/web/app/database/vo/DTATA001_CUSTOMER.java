package com.dano.web.app.database.vo;

import java.lang.String;

public class DTATA001_CUSTOMER {
  private String CUSTOMER_NUMBER;

  private String CERTIFICATE_TYPE;

  private String CERTIFICATE_NUMBER;

  private String CUSTOMER_NAME;

  private String GENDER;

  private String BIRTHDAY;

  private String BOSS_NAME;

  private String CONTACT_NAME;

  private String CREATE_DATE;

  private String CREATE_OPRID;

  private String BRANCH_OFF_TYPE;

  private String PROCESS_AUTHOR;

  private String EDUCATION;

  private String CMP_NOTE;

  private String CUST_NOTE;

  private String ENG_NAME;

  private String TW_IDNO;

  private String CTN_SNAME;

  private String CERTIFICATE_ISSUE_DATE;

  private String CERTIFICATE_ISSUE_PLACE;

  private String LICENSE_NUMBER;

  private String LICENSE_ISSUE_DATE;

  private String LICENSE_ISSUE_PLACE;

  private String COMPANY_OWNER_NAME;

  private String COMPANY_OWNER_TITLE;

  private String COMPANY_OWNER_BIRTHDAY;

  private String COMPANY_OWNER_GENDER;

  private String COMPANY_OWNER_ID;

  private String COMPANY_OWNER_ID_ISSUE_DATE;

  private String COMPANY_OWNER_ID_ISSUE_PLACE;

  private String COMPANY_OWNER_ADDRESS;

  private String AUTHOR_LETTER;

  private String AUTHOR_LETTER_DATE;

  private String BUSINESS_ACTIVITIES;

  public String getCUSTOMER_NUMBER() {
    return this.CUSTOMER_NUMBER;
  }

  public void setCUSTOMER_NUMBER(String CUSTOMER_NUMBER) {
    this.CUSTOMER_NUMBER =CUSTOMER_NUMBER;
  }

  public String getCERTIFICATE_TYPE() {
    return this.CERTIFICATE_TYPE;
  }

  public void setCERTIFICATE_TYPE(String CERTIFICATE_TYPE) {
    this.CERTIFICATE_TYPE =CERTIFICATE_TYPE;
  }

  public String getCERTIFICATE_NUMBER() {
    return this.CERTIFICATE_NUMBER;
  }

  public void setCERTIFICATE_NUMBER(String CERTIFICATE_NUMBER) {
    this.CERTIFICATE_NUMBER =CERTIFICATE_NUMBER;
  }

  public String getCUSTOMER_NAME() {
    return this.CUSTOMER_NAME;
  }

  public void setCUSTOMER_NAME(String CUSTOMER_NAME) {
    this.CUSTOMER_NAME =CUSTOMER_NAME;
  }

  public String getGENDER() {
    return this.GENDER;
  }

  public void setGENDER(String GENDER) {
    this.GENDER =GENDER;
  }

  public String getBIRTHDAY() {
    return this.BIRTHDAY;
  }

  public void setBIRTHDAY(String BIRTHDAY) {
    this.BIRTHDAY =BIRTHDAY;
  }

  public String getBOSS_NAME() {
    return this.BOSS_NAME;
  }

  public void setBOSS_NAME(String BOSS_NAME) {
    this.BOSS_NAME =BOSS_NAME;
  }

  public String getCONTACT_NAME() {
    return this.CONTACT_NAME;
  }

  public void setCONTACT_NAME(String CONTACT_NAME) {
    this.CONTACT_NAME =CONTACT_NAME;
  }

  public String getCREATE_DATE() {
    return this.CREATE_DATE;
  }

  public void setCREATE_DATE(String CREATE_DATE) {
    this.CREATE_DATE =CREATE_DATE;
  }

  public String getCREATE_OPRID() {
    return this.CREATE_OPRID;
  }

  public void setCREATE_OPRID(String CREATE_OPRID) {
    this.CREATE_OPRID =CREATE_OPRID;
  }

  public String getBRANCH_OFF_TYPE() {
    return this.BRANCH_OFF_TYPE;
  }

  public void setBRANCH_OFF_TYPE(String BRANCH_OFF_TYPE) {
    this.BRANCH_OFF_TYPE =BRANCH_OFF_TYPE;
  }

  public String getPROCESS_AUTHOR() {
    return this.PROCESS_AUTHOR;
  }

  public void setPROCESS_AUTHOR(String PROCESS_AUTHOR) {
    this.PROCESS_AUTHOR =PROCESS_AUTHOR;
  }

  public String getEDUCATION() {
    return this.EDUCATION;
  }

  public void setEDUCATION(String EDUCATION) {
    this.EDUCATION =EDUCATION;
  }

  public String getCMP_NOTE() {
    return this.CMP_NOTE;
  }

  public void setCMP_NOTE(String CMP_NOTE) {
    this.CMP_NOTE =CMP_NOTE;
  }

  public String getCUST_NOTE() {
    return this.CUST_NOTE;
  }

  public void setCUST_NOTE(String CUST_NOTE) {
    this.CUST_NOTE =CUST_NOTE;
  }

  public String getENG_NAME() {
    return this.ENG_NAME;
  }

  public void setENG_NAME(String ENG_NAME) {
    this.ENG_NAME =ENG_NAME;
  }

  public String getTW_IDNO() {
    return this.TW_IDNO;
  }

  public void setTW_IDNO(String TW_IDNO) {
    this.TW_IDNO =TW_IDNO;
  }

  public String getCTN_SNAME() {
    return this.CTN_SNAME;
  }

  public void setCTN_SNAME(String CTN_SNAME) {
    this.CTN_SNAME =CTN_SNAME;
  }

  public String getCERTIFICATE_ISSUE_DATE() {
    return this.CERTIFICATE_ISSUE_DATE;
  }

  public void setCERTIFICATE_ISSUE_DATE(String CERTIFICATE_ISSUE_DATE) {
    this.CERTIFICATE_ISSUE_DATE =CERTIFICATE_ISSUE_DATE;
  }

  public String getCERTIFICATE_ISSUE_PLACE() {
    return this.CERTIFICATE_ISSUE_PLACE;
  }

  public void setCERTIFICATE_ISSUE_PLACE(String CERTIFICATE_ISSUE_PLACE) {
    this.CERTIFICATE_ISSUE_PLACE =CERTIFICATE_ISSUE_PLACE;
  }

  public String getLICENSE_NUMBER() {
    return this.LICENSE_NUMBER;
  }

  public void setLICENSE_NUMBER(String LICENSE_NUMBER) {
    this.LICENSE_NUMBER =LICENSE_NUMBER;
  }

  public String getLICENSE_ISSUE_DATE() {
    return this.LICENSE_ISSUE_DATE;
  }

  public void setLICENSE_ISSUE_DATE(String LICENSE_ISSUE_DATE) {
    this.LICENSE_ISSUE_DATE =LICENSE_ISSUE_DATE;
  }

  public String getLICENSE_ISSUE_PLACE() {
    return this.LICENSE_ISSUE_PLACE;
  }

  public void setLICENSE_ISSUE_PLACE(String LICENSE_ISSUE_PLACE) {
    this.LICENSE_ISSUE_PLACE =LICENSE_ISSUE_PLACE;
  }

  public String getCOMPANY_OWNER_NAME() {
    return this.COMPANY_OWNER_NAME;
  }

  public void setCOMPANY_OWNER_NAME(String COMPANY_OWNER_NAME) {
    this.COMPANY_OWNER_NAME =COMPANY_OWNER_NAME;
  }

  public String getCOMPANY_OWNER_TITLE() {
    return this.COMPANY_OWNER_TITLE;
  }

  public void setCOMPANY_OWNER_TITLE(String COMPANY_OWNER_TITLE) {
    this.COMPANY_OWNER_TITLE =COMPANY_OWNER_TITLE;
  }

  public String getCOMPANY_OWNER_BIRTHDAY() {
    return this.COMPANY_OWNER_BIRTHDAY;
  }

  public void setCOMPANY_OWNER_BIRTHDAY(String COMPANY_OWNER_BIRTHDAY) {
    this.COMPANY_OWNER_BIRTHDAY =COMPANY_OWNER_BIRTHDAY;
  }

  public String getCOMPANY_OWNER_GENDER() {
    return this.COMPANY_OWNER_GENDER;
  }

  public void setCOMPANY_OWNER_GENDER(String COMPANY_OWNER_GENDER) {
    this.COMPANY_OWNER_GENDER =COMPANY_OWNER_GENDER;
  }

  public String getCOMPANY_OWNER_ID() {
    return this.COMPANY_OWNER_ID;
  }

  public void setCOMPANY_OWNER_ID(String COMPANY_OWNER_ID) {
    this.COMPANY_OWNER_ID =COMPANY_OWNER_ID;
  }

  public String getCOMPANY_OWNER_ID_ISSUE_DATE() {
    return this.COMPANY_OWNER_ID_ISSUE_DATE;
  }

  public void setCOMPANY_OWNER_ID_ISSUE_DATE(String COMPANY_OWNER_ID_ISSUE_DATE) {
    this.COMPANY_OWNER_ID_ISSUE_DATE =COMPANY_OWNER_ID_ISSUE_DATE;
  }

  public String getCOMPANY_OWNER_ID_ISSUE_PLACE() {
    return this.COMPANY_OWNER_ID_ISSUE_PLACE;
  }

  public void setCOMPANY_OWNER_ID_ISSUE_PLACE(String COMPANY_OWNER_ID_ISSUE_PLACE) {
    this.COMPANY_OWNER_ID_ISSUE_PLACE =COMPANY_OWNER_ID_ISSUE_PLACE;
  }

  public String getCOMPANY_OWNER_ADDRESS() {
    return this.COMPANY_OWNER_ADDRESS;
  }

  public void setCOMPANY_OWNER_ADDRESS(String COMPANY_OWNER_ADDRESS) {
    this.COMPANY_OWNER_ADDRESS =COMPANY_OWNER_ADDRESS;
  }

  public String getAUTHOR_LETTER() {
    return this.AUTHOR_LETTER;
  }

  public void setAUTHOR_LETTER(String AUTHOR_LETTER) {
    this.AUTHOR_LETTER =AUTHOR_LETTER;
  }

  public String getAUTHOR_LETTER_DATE() {
    return this.AUTHOR_LETTER_DATE;
  }

  public void setAUTHOR_LETTER_DATE(String AUTHOR_LETTER_DATE) {
    this.AUTHOR_LETTER_DATE =AUTHOR_LETTER_DATE;
  }

  public String getBUSINESS_ACTIVITIES() {
    return this.BUSINESS_ACTIVITIES;
  }

  public void setBUSINESS_ACTIVITIES(String BUSINESS_ACTIVITIES) {
    this.BUSINESS_ACTIVITIES =BUSINESS_ACTIVITIES;
  }
}

package com.dano.web.app.database.vo;

import java.io.IOException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.springframework.web.socket.WebSocketSession;
import org.sql2o.Connection;

import com.dano.web.app.aa.b0.helper.Enviroment;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.VoTooL;
import com.dano.web.app.aa.b0.helper.batchHelper;
import com.dano.web.app.aa.b0.helper.batchRealTimeDB;
import com.dano.web.app.aa.b0.websocket.AAB1_0100_sAPI;

public class SYSTEM_BATCH_INFO1 {
	public SYSTEM_BATCH_INFO1() {

	}

	private WebSocketSession session;

	public SYSTEM_BATCH_INFO1(WebSocketSession session) {
		this.session = session;
	}

	public SYSTEM_BATCH_INFO1 getSysBatchInfo(String job_id) {
		JdbcHelper ds = new JdbcHelper();
		Connection con = null;

		SYSTEM_BATCH_INFO1 info = new SYSTEM_BATCH_INFO1();
		try {

			con = ds.getConnection();
			List<Map<String, Object>> map1 = new ArrayList();
			map1 = con.createQuery("SELECT A.* from SYSTEM_BATCH_INFO A WHERE id='" + job_id + "'")
					.executeAndFetchTable().asList();

			for (Map<String, Object> map : map1) {
				info = (SYSTEM_BATCH_INFO1) new VoTooL().mapToObj(map, info);
//				info.setid(ObjectUtils.toString(map.get("id")));
//				info.setjob_name(ObjectUtils.toString(map.get("job_name")));
//				info.setjob_group(ObjectUtils.toString(map.get("job_group")));
//				info.setjob_description(ObjectUtils.toString(map.get("job_description")));
//				info.setjob_class(ObjectUtils.toString(map.get("job_class"))) ;
//				info.settrigger_name(ObjectUtils.toString(map.get("trigger_name")));
//				info.settrigger_group(ObjectUtils.toString(map.get("trigger_group")));
//				info.settrigger_cron_expression (ObjectUtils.toString(map.get("trigger_cron_expression"))) ;
//				info.setnext_fire_time(ObjectUtils.toString(map.get("next_fire_time")));
//				info.setprev_fire_time(ObjectUtils.toString(map.get("prev_fire_time")));
//				info.setjob_data(ObjectUtils.toString(map.get("job_data"))) ;
//				info.setjob_info(ObjectUtils.toString(map.get("job_info")));
			}

		} catch (ClassNotFoundException e) {
			con.rollback();
			e.printStackTrace();
		} finally {
			con.close();
		}

		return info;

	}

	private final boolean isDebug = log.isDebugEnabled();
	private static Logger log = Logger.getLogger(SYSTEM_BATCH_INFO.class.getName());
	private String id;

	private String job_name;

	private String job_group;

	private String job_description;

	private String job_class;

	private String trigger_name;

	private String trigger_group;

	private String trigger_cron_expression;

	private String next_fire_time;

	private String prev_fire_time;

	private String job_data;
	private String ip_action;

	public String getIp_action() {
		return ip_action;
	}

	public void setIp_action(String ip_action) {
		this.ip_action = ip_action;
	}

	private String job_info;
	private int returnCode;
	private String returnMesd;

	public String getid() {
		return this.id;
	}

	public void setid(String id) {
		this.id = id;
	}

	public String getjob_name() {
		return this.job_name;
	}

	public void setjob_name(String job_name) {
		this.job_name = job_name;
	}

	public String getjob_group() {
		return this.job_group;
	}

	public void setjob_group(String job_group) {
		this.job_group = job_group;
	}

	public String getjob_description() {
		return this.job_description;
	}

	public void setjob_description(String job_description) {
		this.job_description = job_description;
	}

	public String getjob_class() {
		return this.job_class;
	}

	public void setjob_class(String job_class) {
		this.job_class = job_class;
	}

	public String gettrigger_name() {
		return this.trigger_name;
	}

	public void settrigger_name(String trigger_name) {
		this.trigger_name = trigger_name;
	}

	public String gettrigger_group() {
		return this.trigger_group;
	}

	public void settrigger_group(String trigger_group) {
		this.trigger_group = trigger_group;
	}

	public String gettrigger_cron_expression() {
		return this.trigger_cron_expression;
	}

	public void settrigger_cron_expression(String trigger_cron_expression) {
		this.trigger_cron_expression = trigger_cron_expression;
	}

	public String getnext_fire_time() {
		return this.next_fire_time;
	}

	public void setnext_fire_time(String next_fire_time) {
		this.next_fire_time = next_fire_time;
	}

	public String getprev_fire_time() {
		return this.prev_fire_time;
	}

	public void setprev_fire_time(String prev_fire_time) {
		this.prev_fire_time = prev_fire_time;
	}

	public String getjob_data() {
		return this.job_data;
	}

	public void setjob_data(String job_data) {
		this.job_data = job_data;
	}

	public String getjob_info() {
		return this.job_info;
	}

	public void setjob_info(String job_info) {
		this.job_info = job_info;
	}

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMesd() {
		return returnMesd;
	}

	public void setReturnMesd(String returnMesd) {
		this.returnMesd = returnMesd;
	}

	public void SYSTEM_BATCH_INFO() {
	}

	public Map getMap() {
		Map<String, String> map = new HashMap<>();
		// map.putAll(new VoTooL().objToMap(getSysBatchInfo(this.id)));;
		map.put("id", this.id);
		map.put("job_name", this.job_name);
		map.put("job_group", this.job_group);
		map.put("job_description", this.job_description);
		map.put("job_class", this.job_class);
		map.put("trigger_name", this.trigger_name);
		map.put("trigger_group", this.trigger_group);
		map.put("trigger_cron_expression", this.trigger_cron_expression);
		map.put("next_fire_time", this.next_fire_time);
		map.put("prev_fire_time", this.prev_fire_time);
		map.put("job_data", this.job_data);
		map.put("job_info", this.job_info);
		return map;

	}

	private String getUpdateSql() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.id);
		map.put("job_name", this.job_name);
		map.put("job_group", this.job_group);
		map.put("job_description", this.job_description);
		map.put("job_class", this.job_class);
		map.put("trigger_name", this.trigger_name);
		map.put("trigger_group", this.trigger_group);
		map.put("trigger_cron_expression", this.trigger_cron_expression);
		map.put("next_fire_time", this.next_fire_time);
		map.put("prev_fire_time", this.prev_fire_time);
		map.put("job_data", this.job_data);
		map.put("job_info", this.job_info);
		StringBuilder sqlUpdate = new StringBuilder("UPDATE SYSTEM_BATCH_INFO SET ");
		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (entry.getValue() != null && !entry.getValue().isEmpty()) {
				sqlUpdate.append(entry.getKey()).append(" = '").append(entry.getValue()).append("', ");
			}
		}
		sqlUpdate.setLength(sqlUpdate.length() - 2);
		sqlUpdate.append(" WHERE id = '").append(this.id).append("'");
		return sqlUpdate.toString();
	}

	private String getInsertSql() {
		Map<String, String> map = new HashMap<>();
		map.put("id", this.id);
		map.put("job_name", this.job_name);
		map.put("job_group", this.job_group);
		map.put("job_description", this.job_description);
		map.put("job_class", this.job_class);
		map.put("trigger_name", this.trigger_name);
		map.put("trigger_group", this.trigger_group);
		map.put("trigger_cron_expression", this.trigger_cron_expression);
		map.put("next_fire_time", this.next_fire_time);
		map.put("prev_fire_time", this.prev_fire_time);
		map.put("job_data", this.job_data);
		map.put("job_info", this.job_info);
		StringBuilder columns = new StringBuilder();
		StringBuilder values = new StringBuilder();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (entry.getValue() != null && !entry.getValue().isEmpty()) {
				columns.append(entry.getKey()).append(", ");
				values.append("'").append(entry.getValue()).append("', ");
			}
		}
		columns.setLength(columns.length() - 2); // Xóa dấu phẩy cuối cùng
		values.setLength(values.length() - 2); // Xóa dấu phẩy cuối cùng

		String sqlInsert = "INSERT INTO SYSTEM_BATCH_INFO (" + columns.toString() + ") VALUES (" + values.toString()
				+ ")";
		return sqlInsert.toString();
	}

	public void UpdateDB() {
		JdbcHelper ds = new JdbcHelper();
		Connection con = null;
		Connection con1 = null;
		try {
			con = ds.getConnection();
			con1 = ds.getConnection();
			try {
				String SqlInsert = getInsertSql();
				String SqlUpdate = getUpdateSql();
				log.debug(SqlInsert);
				log.debug(SqlUpdate);
				List<Map<String, Object>> map1 = new ArrayList();
				map1 = con.createQuery("SELECT A.* from SYSTEM_BATCH_INFO A WHERE id='" + this.id + "'")
						.executeAndFetchTable().asList();

				if (map1.size() > 0) {
					try {
						con1.createQuery(SqlUpdate).executeUpdate();
						con1.commit();
					} catch (Exception e) {
						con1.rollback();
					}
				} else {
					try {
						con1.createQuery(SqlInsert).executeUpdate();
						con1.commit();
					} catch (Exception e) {
						con1.rollback();

					}
				}
			} finally {

				con.close();
				con1.close();
			}
			try {
				new Enviroment().getFireBaseConfig().saveBatchStatusRealTime(this.id, getMap());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			/*
			 * try { batchHelper b = new batchHelper(); Map map = new HashMap();
			 * map.put("batchInfo", getMap()); AAB1_0100_sAPI socket=new AAB1_0100_sAPI();
			 * socket.sendToAllConnections(map); } catch (IOException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 */
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

}

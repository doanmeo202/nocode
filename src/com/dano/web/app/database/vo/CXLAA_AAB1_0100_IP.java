package com.dano.web.app.database.vo;

import java.lang.String;

public class CXLAA_AAB1_0100_IP {
  private String IP;

  private String PATH;

  private String IS_LOCK;

  private String LOGS;

  private String TIME_LOGS;

  private String IP_INPUT;

  private String NGINX_FILE;

  public String getIP() {
    return this.IP;
  }

  public void setIP(String IP) {
    this.IP =IP;
  }

  public String getPATH() {
    return this.PATH;
  }

  public void setPATH(String PATH) {
    this.PATH =PATH;
  }

  public String getIS_LOCK() {
    return this.IS_LOCK;
  }

  public void setIS_LOCK(String IS_LOCK) {
    this.IS_LOCK =IS_LOCK;
  }

  public String getLOGS() {
    return this.LOGS;
  }

  public void setLOGS(String LOGS) {
    this.LOGS =LOGS;
  }

  public String getTIME_LOGS() {
    return this.TIME_LOGS;
  }

  public void setTIME_LOGS(String TIME_LOGS) {
    this.TIME_LOGS =TIME_LOGS;
  }

  public String getIP_INPUT() {
    return this.IP_INPUT;
  }

  public void setIP_INPUT(String IP_INPUT) {
    this.IP_INPUT =IP_INPUT;
  }

  public String getNGINX_FILE() {
    return this.NGINX_FILE;
  }

  public void setNGINX_FILE(String NGINX_FILE) {
    this.NGINX_FILE =NGINX_FILE;
  }
}

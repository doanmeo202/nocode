package com.dano.web.app.database.vo;

import java.lang.String;

public class TELEGRAM_TEMPLATE_FILES {
  private String ID_AUTO;

  private String FILE_ID;

  private String FILE_NAME;

  private String FILE_TYPE;

  private String IN_GROUP;

  private String CHAT_ID;

  private String URL_DOWNLOAD;

  private String FILE_UNIQUE_ID;

  private String FILE_SIZE;

  private String FILE_PATH;

  private String FILENAME_OLD;

  private String FILE_FK;

  public String getID_AUTO() {
    return this.ID_AUTO;
  }

  public void setID_AUTO(String ID_AUTO) {
    this.ID_AUTO =ID_AUTO;
  }

  public String getFILE_ID() {
    return this.FILE_ID;
  }

  public void setFILE_ID(String FILE_ID) {
    this.FILE_ID =FILE_ID;
  }

  public String getFILE_NAME() {
    return this.FILE_NAME;
  }

  public void setFILE_NAME(String FILE_NAME) {
    this.FILE_NAME =FILE_NAME;
  }

  public String getFILE_TYPE() {
    return this.FILE_TYPE;
  }

  public void setFILE_TYPE(String FILE_TYPE) {
    this.FILE_TYPE =FILE_TYPE;
  }

  public String getIN_GROUP() {
    return this.IN_GROUP;
  }

  public void setIN_GROUP(String IN_GROUP) {
    this.IN_GROUP =IN_GROUP;
  }

  public String getCHAT_ID() {
    return this.CHAT_ID;
  }

  public void setCHAT_ID(String CHAT_ID) {
    this.CHAT_ID =CHAT_ID;
  }

  public String getURL_DOWNLOAD() {
    return this.URL_DOWNLOAD;
  }

  public void setURL_DOWNLOAD(String URL_DOWNLOAD) {
    this.URL_DOWNLOAD =URL_DOWNLOAD;
  }

  public String getFILE_UNIQUE_ID() {
    return this.FILE_UNIQUE_ID;
  }

  public void setFILE_UNIQUE_ID(String FILE_UNIQUE_ID) {
    this.FILE_UNIQUE_ID =FILE_UNIQUE_ID;
  }

  public String getFILE_SIZE() {
    return this.FILE_SIZE;
  }

  public void setFILE_SIZE(String FILE_SIZE) {
    this.FILE_SIZE =FILE_SIZE;
  }

  public String getFILE_PATH() {
    return this.FILE_PATH;
  }

  public void setFILE_PATH(String FILE_PATH) {
    this.FILE_PATH =FILE_PATH;
  }

  public String getFILENAME_OLD() {
    return this.FILENAME_OLD;
  }

  public void setFILENAME_OLD(String FILENAME_OLD) {
    this.FILENAME_OLD =FILENAME_OLD;
  }

  public String getFILE_FK() {
    return this.FILE_FK;
  }

  public void setFILE_FK(String FILE_FK) {
    this.FILE_FK =FILE_FK;
  }
}

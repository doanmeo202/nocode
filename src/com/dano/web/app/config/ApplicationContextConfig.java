package com.dano.web.app.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.SchedulerProvider;
import com.dano.web.app.aa.b0.helper.batchHelper;
import com.dano.web.app.aa.b0.helper.fileHelper;
import com.dano.web.app.aa.b0.modun.AAB0_0100_mod;

@Configuration
@ComponentScan("com.dano.web.app.*")
public class ApplicationContextConfig {

	@Bean(name = "viewResolver")
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/public/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver commonsMultipartResolver() {
		final CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(-1);
		return commonsMultipartResolver;
	}

//	@Bean(name = "configLog4j")
//	public PropertyConfigurator configLog4j() {
//		PropertyConfigurator configurer = new PropertyConfigurator();
//
//		Properties properties = new Properties();
//		properties.setProperty("log4j.rootCategory", "DEBUG, all");
//		properties.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
//		properties.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
//		properties.setProperty("log4j.appender.all.layout.ConversionPattern",
//				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
//		AAB0_0100_mod mod = new AAB0_0100_mod();
//		int i = 1;
//		List listPackage = new ArrayList();
//		while (true) {
//			List<Map<String, Object>> li = mod.getFieldOptionList("SYS", "PACKAGE", "" + i);
//			if (li.size() < 1) {
//				break;
//			} else {
//				for (Map<String, Object> map : li) {
//					String name = map.get("name").toString();
//					String path = name + ".txt";
//
//					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//					String formatted = df.format(new Date());
//					String file_name = fileHelper.getPathTomcat() + "/log/" + formatted + "_" + path;
//					properties.setProperty("log4j.category." + name, "DEBUG," + name);
//					properties.setProperty("log4j.appender." + name + ".layout", "org.apache.log4j.PatternLayout");
//					properties.setProperty("log4j.appender." + name + ".layout.ConversionPattern",
//							"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
//					properties.setProperty("log4j.appender." + name, "org.apache.log4j.RollingFileAppender");
//					properties.setProperty("log4j.appender." + name + ".File", file_name);
//					properties.setProperty("log4j.appender." + name + ".MaxFileSize", "500KB");
//
//				}
//
//			}
//			i++;
//
//		}
//		properties.setProperty("log4j.logger.org.quartz", "warn");
//		BasicConfigurator.configure();
//		PropertyConfigurator.configure(properties);
//		return configurer;
//	}

//	@Bean(name = "configScheduler")
//	public Scheduler configScheduler() {
//		Scheduler scheduler = SchedulerProvider.getScheduler();
//		try {
//			Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.anyJobGroup());
//			if (jobKeys.size() < 1) {
//				batchHelper hp = new batchHelper();
//				hp.activateJobsOnStartup();
//			}
//		} catch (SchedulerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return scheduler;
//	}

}
package com.dano.web.app.aa.b0.batch;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dano.web.app.aa.b0.helper.DataSet;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.backup_helper;
import com.telegram.core.TelegramBotSetup;
import com.telegram.core.main;

public class AAB0_0103 implements Job {
	private Logger log = Logger.getLogger(this.getClass());

	public void execute(JobExecutionContext context) throws JobExecutionException {

		log.debug("====================================================================");
		log.debug("-----------------------------");
		log.debug(this.getClass().getSimpleName());
		log.debug("-----------------------------");
		TelegramBotSetup bot = new TelegramBotSetup();
		List<Map<String, Object>> li = bot.getList_File();
		for (Map map : li) {
			String Url = bot.getURL_File(ObjectUtils.toString(map.get("bot_username")),
					ObjectUtils.toString(map.get("file_id")));
			DataSet hp = new DataSet();
			hp.setField("URL", Url);
			hp.setField("BATCH", this.getClass().getSimpleName());
			hp.setField("FILE_ID", ObjectUtils.toString(map.get("file_id")));
			if (hp.searchAndRetrieve("com.telegram.template.getfileIdfromdb").size() > 0) {
				try {
					hp.update("com.telegram.template.updatefileIdfromdb");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					hp.update("com.telegram.template.insertfileIdfromdb");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		log.debug("====================================================================");
	}

	/*
	 * public static void main(String[] args) {
	 * 
	 * TelegramBotSetup bot = new TelegramBotSetup(); List<Map<String, Object>> li =
	 * bot.getList_File1(); for (Map map : li) { String Url =
	 * bot.getURL_File(ObjectUtils.toString(map.get("bot_username")),
	 * ObjectUtils.toString(map.get("file_id"))); JdbcHelper hp = new JdbcHelper();
	 * hp.setField("URL", Url); hp.setField("BATCH", "Main_Test");
	 * hp.setField("FILE_ID", ObjectUtils.toString(map.get("file_id"))); if
	 * (hp.searchAndRetrieve("com.telegram.template.getfileIdfromdb").size() > 0) {
	 * hp.update("com.telegram.template.updatefileIdfromdb"); } else {
	 * hp.update("com.telegram.template.insertfileIdfromdb"); } }
	 * 
	 * }
	 */

}

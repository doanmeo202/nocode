package com.dano.web.app.aa.b0.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dano.web.app.aa.b0.helper.batchUpdateDataSet;

public class AAB0_0104 implements Job {
	private Logger log = Logger.getLogger(this.getClass());

	public void execute(JobExecutionContext context) throws JobExecutionException {
		batchUpdateDataSet buds = new batchUpdateDataSet();

	
		try {
			String sql = buds.getSqlFromKey("com.dano.web.app.aa.b1.modun.AAB1_0100_PUT_IP");
			// "UPDATE cxlaa_aab1_0100_ip SET PATH='AAAAAAAAA' WHERE IP=:IP";
			// "DELETE FROM cxlaa_aab1_0100_ip WHERE IP=:IP";
			buds.setUpBatchInfo(AAB0_0104.class);
			buds.beginTransaction();
			buds.preparedBatch(sql);
			buds.setAutoCommit(false);
			 buds.setNotify(false);
			List<Map> li = new ArrayList();
			for (int i = 0; i < 11; i++) {
				Map<String, Object> map = new HashMap<>();
				map.put("IP", i);
				buds.setQuery(map);
				buds.addBatch();
			}
			int i = buds.executeBatch();

			// b.handleBatchErrors(batchResult);
		} finally {
			buds.endTransaction();
		}
	}

}

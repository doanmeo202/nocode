package com.dano.web.app.aa.b0.batch;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dano.web.app.aa.b0.helper.backup_helper;

public class AAB0_0101 implements Job {
	private Logger log = Logger.getLogger(AAB0_0101.class.getName());
	public void execute(JobExecutionContext context) throws JobExecutionException {
    log.debug("====================================================================");
    log.debug("BACKUP MYSQL DATA");
    backup_helper backup= new backup_helper();
    try {
		backup.backup();
	} catch (Exception e) {
		throw new JobExecutionException(e);
	}
    log.debug("====================================================================");
	}
}

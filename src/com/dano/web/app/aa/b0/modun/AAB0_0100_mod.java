package com.dano.web.app.aa.b0.modun;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.dano.web.app.aa.b0.helper.DataSet;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.batchUpdateDataSet;
import com.dano.web.app.aa.b0.helper.excelExcute;

public class AAB0_0100_mod {
	private String Select_Menu = "com.telegram.template.getmenuwebsite";
	private String Select_Banner = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_BANNER";
	private String Select_Category = "com.dano.web.app.aa.b0.modun.AAB0_0101_GET_CATEGORY";
	private String Select_SubMenu = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SUB_MENU";
	private String GET_FIELD_OPTION_LIST = "com.dano.web.app.aa.b0.modun.AAB0_0101_GET_FIELD_OPTION_LIST";
	private String Select_POST = "com.dano.web.app.aa.b0.modun.AAB0_0101_GET_POST";
	private String Backup_CodeMap = "com.dano.web.app.aa.b0.modun.AAB0_0101_BACKUP_CODEMAP";
	private String Select_About1 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_ABOUTUS1";
	private String Select_About2 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_ABOUTUS2";
	private String Select_Sevice = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE";
	private String Select_Sevice1 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE1";
	private String Select_Sevice2 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE2";
	// com.dano.web.app.aa.b0.modun.AAB0_0100_GET_BANNER
	private Map map;

	public List<Map<String, Object>> getSevice() {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		return ds.getDataFromKey(Select_Sevice, map);
	}

	public List<Map<String, Object>> getSevice(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_Sevice, map);
	}

	public List<Map<String, Object>> getSevice1(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_Sevice1, map);
	}

	public List<Map<String, Object>> getSevice2(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_Sevice2, map);
	}

	public List<Map<String, Object>> getAboutus1(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_About1, map);
	}

	public List<Map<String, Object>> getAboutus2(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_About2, map);
	}

	public List<Map<String, Object>> getBanner(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_Banner, map);
	}

	public List<Map<String, Object>> getMenu(String webUsed) {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		map.put("WEB_USED", webUsed);
		return ds.getDataFromKey(Select_Menu, map);
	}

	public List<Map<String, Object>> getCategory() {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		return ds.getDataFromKey(Select_Category, map);
	}

	public void backUpCodeMap(Map newMap) {
		JdbcHelper ds = new JdbcHelper();
		ds.setDataFromKey(Backup_CodeMap, newMap);
	}

	public List<Map<String, Object>> getPostID(String ID) {
		JdbcHelper ds = new JdbcHelper();

		map = new HashMap();
		map.put("ID", ID);
		return ds.getDataFromKey(Select_POST, map);
	}

	public List<Map<String, Object>> getFieldOptionList(String SYSID, String FIELD_NAME, String SETTING) {
		DataSet ds = DataSet.getDataSet();
		map = new HashMap();
		ds.setField("SYSID", SYSID);
		ds.setField("FIELD_NAME", FIELD_NAME);
		ds.setField("SETTING", SETTING);
		return ds.searchAndRetrieve(GET_FIELD_OPTION_LIST);
	}

	public List<Map<String, Object>> getHostDB() {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
		return ds.getDataFromKey("com.system.selecthost", map);
	}

	public AAB0_0100_mod() {

	}

	public Map insertData(File file) {
		UUID uuid = UUID.randomUUID();
		String generatedCode = uuid.toString().replace("-", "");
		excelExcute lib = new excelExcute();
		batchUpdateDataSet buds = new batchUpdateDataSet();
		String sql = "INSERT INTO EXCEL_RECODE_DATA (ID_BATCH, COL1, COL2, COL3, COL4, COL5) VALUES (:ID_BATCH, :COL1, :COL2, :COL3, :COL4, :COL5);";
		buds.setUpBatchInfo(this.getClass());
		buds.beginTransaction();
		buds.preparedBatch(sql);
		buds.setAutoCommit(true);
		try {
			// String sql = buds.getSqlFromKey("UPDATE cxlaa_aab1_0100_ip SET
			// PATH='AAAAAAAAA' WHERE IP=:IP");

			// "UPDATE cxlaa_aab1_0100_ip SET PATH='AAAAAAAAA' WHERE IP=:IP";
			// "DELETE FROM cxlaa_aab1_0100_ip WHERE IP=:IP";

			// buds.setNotify(true);
			lib.setFileInput(file, 0);
			List<Map<Integer, Object>> li = lib.SnapShortData();
			for (Map<Integer, Object> map : li) {
				Map map2 = new HashMap();
				map2.put("ID_BATCH", generatedCode);
				map2.put("COL1", map.get(0));
				map2.put("COL2", map.get(1));
				map2.put("COL3", map.get(2));
				map2.put("COL4", map.get(3));
				map2.put("COL5", map.get(4));
				buds.setQuery(map2);
				buds.addBatch();
			}

			buds.executeBatch();
		} finally {
			buds.endTransaction();
		}
		Map map = new HashMap();
		map.put("UID", generatedCode);
		return map;
	}

	public static void main(String[] args) {
		AAB0_0100_mod mod = new AAB0_0100_mod();
		File file = new File("D:\\Test1.xlsx");
		mod.insertData(file);
		// System.out.println(mod.getMenu());
	}
}

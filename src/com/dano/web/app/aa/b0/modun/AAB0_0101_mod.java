package com.dano.web.app.aa.b0.modun;

import java.util.Arrays;
import java.util.List;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

public class AAB0_0101_mod {
    private static final String APPLICATION_NAME = "Drive API Java Quickstart";
    private static final JacksonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE_FILE);

    private Drive getService() throws IOException, GeneralSecurityException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        Credential credential = GoogleCredential.getApplicationDefault().createScoped(SCOPES);
        return new Drive.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public String downloadFileGoogleDrive(String fileId) throws IOException, GeneralSecurityException {
        Drive service = getService();
        File file = service.files().get(fileId).execute();
        String downloadUrl = file.getWebContentLink();
        return downloadUrl;
    }

    public static void main(String[] args) {
        String fileId = "1w4_UT0_G_WboKV4dl2AZy_YYwCHtwTaB";
        try {
        	AAB0_0101_mod mod= new AAB0_0101_mod();
        	mod.downloadFileGoogleDrive(fileId);
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }
}

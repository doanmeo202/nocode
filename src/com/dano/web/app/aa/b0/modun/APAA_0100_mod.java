package com.dano.web.app.aa.b0.modun;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.BasicConfigurator;

import com.dano.web.app.aa.b0.helper.DataSet;
import com.dano.web.app.aa.b0.helper.batchUpdateDataSet;
import com.dano.web.app.aa.b0.helper.excelExcute;


public class APAA_0100_mod {
	public String generatedCode() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
	}

	public List<Map<String, Object>> getDataUploadDetail(String ID) {
		String sql = "SELECT * FROM EXCEL_RECODE_DATA WHERE ID_BATCH =:ID_BATCH";
		DataSet ds = DataSet.getDataSet();
		ds.setField("ID_BATCH", ID);
		return ds.searchAndRetrieve(sql);
	}

	public List<Map<String, Object>> getDataAnsDetail(String ID) {
		String sql = "SELECT * FROM EXCEL_RECODE_QA WHERE ID_BATCH =:ID_BATCH";
		DataSet ds = DataSet.getDataSet();
		ds.setField("ID_BATCH", ID);
		return ds.searchAndRetrieve(sql);
	}

	public List<Map<String, Object>> getDataComplete(String ID) {
		String sql = "SELECT A.ID_BATCH ,A.COMPLETE ,B.COL1 ,B.COL2,B.COL4,B.COL5  FROM EXCEL_RECODE_COMPLETE A JOIN EXCEL_RECODE_DATA B ON A.ID_BATCH =B.ID_BATCH  AND A.ID =B.ID WHERE A.ID_BATCH =:ID_BATCH";
		DataSet ds = DataSet.getDataSet();
		ds.setField("ID_BATCH", ID);
		return ds.searchAndRetrieve(sql);
	}

	public List<Map<String, Object>> getListID_BATCH() {
		String sql = "SELECT ID_BATCH, COUNT(ID_BATCH) AS COUNT_DATA, CONCAT(CONCAT('<a href=\\\"promptView?ID_BATCH=', ID_BATCH), '\\\">xem dữ liệu</a>\') as ACT, DATE(MAX(TIME_UPDATE)) AS TIME_UPDATE FROM EXCEL_RECODE_DATA GROUP BY ID_BATCH ORDER BY TIME_UPDATE DESC";
		DataSet ds = DataSet.getDataSet();
		return ds.searchAndRetrieve(sql);
	}

	public String insertDataUser(File file) throws SQLException {
		excelExcute lib = new excelExcute();
		batchUpdateDataSet buds = new batchUpdateDataSet();
		String sql = "INSERT INTO EXCEL_RECODE_DATA (ID_BATCH, COL1, COL2, COL3, COL4, COL5) VALUES (:ID_BATCH, :COL1, :COL2, :COL3, :COL4, :COL5);";
		buds.setUpBatchInfo(this.getClass());
		buds.beginTransaction();
		buds.preparedBatch(sql);
		String generatedCode = generatedCode();
		try {
			lib.setFileInput(file, 0);
			List<Map<Integer, Object>> li = lib.SnapShortData();
			for (Map<Integer, Object> map : li) {
				Map map2 = new HashMap();
				map2.put("ID_BATCH", generatedCode);
				map2.put("COL1", map.get(0));
				map2.put("COL2", map.get(1));
				map2.put("COL3", map.get(2));
				map2.put("COL4", map.get(3));
				map2.put("COL5", map.get(4));
				buds.setQuery(map2);
				buds.addBatch();
			}

			buds.executeBatch(true);
		} finally {
			buds.endTransaction();
		}
		batchUpdateDataSet buds2 = new batchUpdateDataSet();
		buds2.setUpBatchInfo(this.getClass());
		buds2.beginTransaction();
		String sql2 = "INSERT INTO EXCEL_RECODE_QA" + "( ID_BATCH, `INDEX`, TRUE_ANS)"
				+ "VALUES( :ID_BATCH, :INDEX, :TRUE_ANS);";
		buds2.preparedBatch(sql2);

		try {
			lib.setFileInput(file, 1);
			List<Map<Integer, Object>> li = lib.SnapShortData();
			int i = 0;
			for (Map<Integer, Object> map : li) {
				Map map2 = new HashMap();
				map2.put("ID_BATCH", generatedCode);
				map2.put("INDEX", i);
				map2.put("TRUE_ANS", map.get(0).toString());
				i++;
				buds2.setQuery(map2);
				buds2.addBatch();
			}
			buds2.executeBatch(true);
		} finally {
			buds.endTransaction();
		}

		return generatedCode;
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		APAA_0100_mod mod = new APAA_0100_mod();
		File file = new File("D:\\Test.xlsx");
		try {
			mod.insertDataUser(file);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

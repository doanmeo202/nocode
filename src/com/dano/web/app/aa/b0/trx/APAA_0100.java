package com.dano.web.app.aa.b0.trx;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dano.web.app.aa.b0.helper.returnObject;
import com.dano.web.app.aa.b0.modun.APAA_0100_mod;

@Controller
@RequestMapping("/APAA_0100")
public class APAA_0100 {
	private returnObject msg;

	@RequestMapping("/promptUpload")
	public String indexPage(@RequestParam Map<String, Object> params, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		return "/APAA_0100/APAA_0100_UPLOAD";
	}

	@RequestMapping(value = "/query", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String query(@RequestParam Map<String, Object> params, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		APAA_0100_mod mod = new APAA_0100_mod();
		msg.setReturnData(mod.getListID_BATCH());
		return msg.returnClient();
	}

	@RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String upload(@RequestParam("file") MultipartFile file, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		File tempFile = null;
		APAA_0100_mod mod = new APAA_0100_mod();
		Map outPutMap = new HashMap();
		try {
			tempFile = File.createTempFile("temp", null);
			file.transferTo(tempFile);
			msg.setReturnData(outPutMap);
			String ID_BATCH = mod.insertDataUser(tempFile);
			msg.setReturnData(ID_BATCH);
			msg.setMsgDescs("Hoàn tất tải lên, mã lô: " + ID_BATCH);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg.setException(e);
		} finally {
			tempFile.delete();
		}
		return msg.returnClient();

	}

	@RequestMapping(value = "/promptView")
	public String view(@RequestParam Map<String, Object> params, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		APAA_0100_mod mod = new APAA_0100_mod();
		String ID = params.get("ID_BATCH").toString();
		Map map = new HashMap();
		model.addAttribute("getDataUploadDetail", mod.getDataUploadDetail(ID));

		model.addAttribute("getDataAnsDetail", mod.getDataAnsDetail(ID));
		return "/APAA_0100/APAA_0100_VIEW";

	}

	@RequestMapping(value = "/queryHistory", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String queryHistory(@RequestParam Map<String, Object> params, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		String ID = ObjectUtils.toString(params.get("ID_BATCH"));
		APAA_0100_mod mod = new APAA_0100_mod();
		msg.setReturnData(mod.getDataComplete(ID));
		return msg.returnClient();
	}

	@RequestMapping(value = "/promptHistory")
	public String history(@RequestParam Map<String, Object> params, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		msg = new returnObject();
		msg.setRequest(request);
		msg.setResponse(response);
		return "/APAA_0100/APAA_0100_HISTORY";
	}

}
package com.dano.web.app.aa.b0.trx;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dano.web.app.aa.b0.helper.excelExcute;
import com.dano.web.app.aa.b0.helper.fOL;
import com.dano.web.app.aa.b0.helper.returnObject;

@Controller
public class AAB0_0100 {
	private String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	private String javaVersion = ObjectUtils.toString(System.getProperties().get("java.version"));

	@RequestMapping("/")
	public String indexPage(@RequestParam Map<String, Object> params, Model model) {
		return "index";

	}

	@RequestMapping("/uploadExcel")
	public String uploadExcel(@RequestParam Map<String, Object> params, Model model) {
		return "uploadExcel";

	}

	@RequestMapping("/history")
	public String history(@RequestParam Map<String, Object> params, Model model) {
		return "uploadExcel";

	}

	@RequestMapping("/login")
	public String login(@RequestParam Map<String, Object> params, Model model) {
		return "login";

	}

	private returnObject msg;

	@RequestMapping(value = "/api-excel/getData", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String excuteExcel(@RequestParam("file") MultipartFile file, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		msg = new returnObject();

		msg.setRequest(request);
		msg.setResponse(response);
		File tempFile = null;
		excelExcute lib = new excelExcute();
		Map outPutMap = new HashMap();
		try {
			tempFile = File.createTempFile("temp", null);
			file.transferTo(tempFile);
			lib.setFileInput(tempFile, 0);
			outPutMap.put("SHEET1", lib.SnapShortData());
			lib.setFileInput(tempFile, 1);
			outPutMap.put("SHEET2", lib.SnapShortData());
			msg.setReturnData(outPutMap);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg.setException(e);
		} finally {
			tempFile.delete();
		}
		return msg.returnClient();
	}

	@RequestMapping("/admin")
	public String adminPage(@RequestParam Map<String, Object> params, Model model, HttpServletRequest request) {
		String ipAddress = ObjectUtils.toString(request.getHeader("X-FORWARDED-FOR"));
		List<fOL> ip = new fOL().getFol("SYS", "PROXY", ipAddress);
		List<fOL> proxy = new fOL().getFol("SYS", "PROXYSOCKET", ipAddress);

		for (fOL fOL : ip) {
			model.addAttribute("proxy", fOL.getnAME());
		}
		for (fOL fOL : proxy) {
			model.addAttribute("proxySoket", fOL.getnAME());
		}
		if (ip.size() < 1) {
			model.addAttribute("proxy", "http://webdano.ddns.net:8080/api/");
			model.addAttribute("proxySoket", "webdano.ddns.net:8080/");
		}

		// Lấy thông tin về URL mà người dùng đang truy cập
		String domain = request.getRequestURL().toString();
		String contextPath = request.getContextPath();
		domain = domain.replace(contextPath, "");
		model.addAttribute("domain", domain);

		return "admin";
	}

	public static void main(String[] args) throws SchedulerException {
		String dir = System.getProperty("user.dir");
		System.out.println();
		System.out.println("");
	}
}

package com.dano.web.app.aa.b0.websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.dano.web.app.aa.b0.helper.DataSenderSocket;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class AAB1_0100_sAPI extends TextWebSocketHandler {
    private static WebSocketSession session;

    public static void setSession(WebSocketSession session) {
    	AAB1_0100_sAPI.session = session;
    }
    
	@Autowired
	private HttpServletRequest request;
	private Logger log = Logger.getLogger(AAB1_0100_sAPI.class.getName());

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) {
		// Handle the message from the client here
		System.out.println("Received message from client: " + message.getPayload());
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) {
		this.session = session;
	}

	public void sendData(Object data) {
		try {
			ObjectMapper obj = new ObjectMapper();
			String json = obj.writeValueAsString(data);
			session.sendMessage(new TextMessage(json));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void sendToAllConnections(Object data) throws IOException {
		 ObjectMapper obj = new ObjectMapper();
         try {
        	 String json = obj.writeValueAsString(data);
        	 if(session!=null) {
            	  session.sendMessage(new TextMessage(json));
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}

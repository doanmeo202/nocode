package com.dano.web.app.aa.b0.helper;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.jetty.util.log.Log;
import org.quartz.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.quartz.Trigger.TriggerState;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.web.socket.WebSocketSession;
import com.dano.web.app.database.vo.SYSTEM_BATCH_INFO;
import com.dano.web.app.database.vo.SYSTEM_BATCH_INFO1;

public class batchHelper {
	private int key;
	private String value;
	private String jobName;
	private String statusJob;
	private String cronExpression;
	private batchHelper batch;
	private SYSTEM_BATCH_INFO1 info = new SYSTEM_BATCH_INFO1();

	public batchHelper() {

	}

	public void activateJobsOnStartup() {
		Scheduler sched = SchedulerProvider.getScheduler();
		JdbcHelper jd = new JdbcHelper();
		Map map = new HashMap();
		jd.setDataFromKey("com.system.update.status.batch", map);
		List<Map<String, Object>> li = jd.getDataFromKey("com.system.query.status.batch", map);
		for (Map<String, Object> item : li) {
			Class cl;
			try {
				cl = Class.forName(ObjectUtils.toString(item.get("job_class")));
				String group = getPackage(cl.getName(), cl.getSimpleName());
				scheduleJobOnStartup(sched, ObjectUtils.toString(item.get("job_name")), cl,
						ObjectUtils.toString(item.get("trigger_cron_expression")),
						ObjectUtils.toString(item.get("next_fire_time")));
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public SYSTEM_BATCH_INFO1 scheduleJobOnStartup(Scheduler scheduler, String jobName, Class<? extends Job> jobClass,
			String cronExpression, String getNextFireTime) throws SchedulerException {
		info = new SYSTEM_BATCH_INFO1().getSysBatchInfo(jobName);
		batch = new batchHelper();
		info.setjob_class(jobClass.getName());
		info.setjob_name(jobName);
		info.setid(jobName);
		info.settrigger_cron_expression(cronExpression);

		if (!checkSchedule(scheduler, jobName)) {
			JobKey jobKey = new JobKey(jobName);
			SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
			Date date = null;
			try {
				date = format.parse(getNextFireTime);
			} catch (ParseException e) {

			}
			if (date != null) {
				if (!scheduler.checkExists(jobKey)) {
					JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey).build();
					Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName)
							.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).startAt(date).build();
					scheduler.scheduleJob(jobDetail, trigger);
				}
			}

		} else {
			batch.erorr("bath đã hoặt động!!!!!");
		}
		info.setjob_info("Order");
		info.UpdateDB();
		return info;

	}

	private WebSocketSession session;

	public batchHelper(WebSocketSession session) {
		this.session = session;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getStatusJob() {
		return statusJob;
	}

	public void setStatusJob(String statusJob) {
		this.statusJob = statusJob;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public void erorr(String value) {
		this.key = -1;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void batchHelper() {
		this.value = "default";
		this.key = 0;
	}

	public void stopJob(Scheduler scheduler, String jobName) throws SchedulerException {
		TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
		scheduler.unscheduleJob(triggerKey);
	}

	public boolean isJobRunning(Scheduler scheduler, String jobName) throws SchedulerException {
		List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
		for (JobExecutionContext executingJob : executingJobs) {
			if (executingJob.getJobDetail().getKey().getName().equals(jobName)) {
				return true;
			}
		}
		return false;
	}

	public void pauseJob(Scheduler scheduler, String jobName) throws SchedulerException {
		JobKey jobKey = JobKey.jobKey(jobName);
		scheduler.pauseJob(jobKey);

	}

	public void resumeJob(Scheduler scheduler, String jobName) throws SchedulerException {
		JobKey jobKey = JobKey.jobKey(jobName);
		scheduler.resumeJob(jobKey);
	}

	public boolean checkSchedule(Scheduler scheduler, String jobName) throws SchedulerException {
		TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
		TriggerState triggerState = scheduler.getTriggerState(triggerKey);
		if (triggerState == TriggerState.NORMAL) {
			return true;
		} else {
			return false;
		}
	}

	public SYSTEM_BATCH_INFO1 scheduleJob(Scheduler scheduler, String jobName, Class<? extends Job> jobClass,
			String cronExpression, String Dess) throws SchedulerException {
		info = new SYSTEM_BATCH_INFO1().getSysBatchInfo(jobName);
		batch = new batchHelper();
		info.setjob_class(jobClass.getName());
		info.setjob_name(jobName);
		info.setid(jobName);
		info.setjob_description(Dess);
		info.settrigger_cron_expression(cronExpression);

		if (!checkSchedule(scheduler, jobName)) {
			JobKey jobKey = new JobKey(jobName);

			if (!scheduler.checkExists(jobKey)) {
				JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey).build();
				/*
				 * JobDataMap dataMap = new JobDataMap(); dataMap.put("",""); JobDetail
				 * jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey)
				 * .usingJobData("param1", "null") // Truyền giá trị param1 vào JobDataMap
				 * .usingJobData("param2", "") // Truyền giá trị param2 vào JobDataMap .build();
				 */

				Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName)
						.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();

				scheduler.scheduleJob(jobDetail, trigger);
			}
		} else {
			batch.erorr("bath đã hoặt động!!!!!");
		}
		info.setjob_info("Order");
		info.UpdateDB();
		return info;

	}

	public String getJobStatus(Scheduler scheduler, String jobName) throws SchedulerException {
		JobKey jobKey = new JobKey(jobName);
		List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
		if (triggers.isEmpty()) {
			return "Job not found";
		}

		for (Trigger trigger : triggers) {
			TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
			if (TriggerState.PAUSED.equals(triggerState)) {
				return "Paused";
			} else if (TriggerState.BLOCKED.equals(triggerState)) {
				return "Blocked";
			} else if (TriggerState.ERROR.equals(triggerState)) {
				return "Error";
			} else if (TriggerState.COMPLETE.equals(triggerState)) {
				return "Complete";
			}
		}
		return "Running";
	}

	public int getJobStatusInt(Scheduler scheduler, String jobName) throws SchedulerException {
		JobKey jobKey = new JobKey(jobName);
		List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
		if (triggers.isEmpty()) {
			return -1;
		}

		for (Trigger trigger : triggers) {
			TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
			if (TriggerState.PAUSED.equals(triggerState)) {
				return -2;
			} else if (TriggerState.BLOCKED.equals(triggerState)) {
				return -3;
			} else if (TriggerState.ERROR.equals(triggerState)) {
				return -4;
			} else if (TriggerState.COMPLETE.equals(triggerState)) {
				return 1;
			}
		}
		return 0;
	}

	public void getJobStartAndEndTimes(Scheduler scheduler, String jobName) throws SchedulerException {
		JobKey jobKey = new JobKey(jobName);
		List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
		for (Trigger trigger : triggers) {
			Date startTime = trigger.getStartTime();
			Date endTime = trigger.getEndTime();
		}
	}

	public SYSTEM_BATCH_INFO1 excuteBatch(String packageClass) throws Exception {

		Class cl = Class.forName(packageClass);
		String group = getPackage(cl.getName(), cl.getSimpleName());
		Scheduler scheduler = SchedulerProvider.getScheduler();
		info = new SYSTEM_BATCH_INFO1();
		info = scheduleJob(scheduler, cl.getSimpleName(), cl, "0/30 * * * * ?", "");
		return info;
	}

	public List<Map> stopJobApi(String jobName) throws SchedulerException {
		List<Map> li = new ArrayList();
		Map map = new HashMap();
		if (jobName != null) {
			TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
			info = new SYSTEM_BATCH_INFO1();
			info = new SYSTEM_BATCH_INFO1().getSysBatchInfo(jobName);
			info.setjob_info("Kill");

			info.UpdateDB();
			map.put("batchInfo", info);
			li.add(map);
			Scheduler scheduler = SchedulerProvider.getScheduler();
			scheduler.unscheduleJob(triggerKey);
		} else {
			info = new SYSTEM_BATCH_INFO1();
			info.setReturnCode(-1);
			info.setReturnMesd("LỖI PARAM jobName: " + "`" + jobName + "`");
			map.put("batchInfo", info);
			li.add(map);
		}

		return li;
	}

	public List<Map> pauseJobApi(String jobName) throws SchedulerException {
		List<Map> li = new ArrayList();
		info = new SYSTEM_BATCH_INFO1();
		info = new SYSTEM_BATCH_INFO1().getSysBatchInfo(jobName);

		JobKey jobKey = JobKey.jobKey(jobName);
		Scheduler scheduler = SchedulerProvider.getScheduler();
		if (checkSchedule(scheduler, jobName)) {
			info.setjob_info("Hold");
			scheduler.pauseJob(jobKey);
			info.UpdateDB();

		}

		Map map = new HashMap();
		map.put("batchInfo", info);
		li.add(map);
		return li;
	}

	public List<Map> resumeJobApi(String jobName) throws SchedulerException {
		List<Map> li = new ArrayList();
		info = new SYSTEM_BATCH_INFO1();
		info = new SYSTEM_BATCH_INFO1().getSysBatchInfo(jobName);

		JobKey jobKey = JobKey.jobKey(jobName);
		Scheduler scheduler = SchedulerProvider.getScheduler();

		Map map = new HashMap();
		if (checkSchedule(scheduler, jobName)) {
			info.setjob_info("Free");
			scheduler.resumeJob(jobKey);
			info.UpdateDB();

		}
		map.put("batchInfo", info);
		li.add(map);
		return li;
	}

	public String getPackage(String name, String simpleName) {
		int indexOf = name.indexOf("." + simpleName);
		return name.substring(0, indexOf);
	}

	public List<Map> excuteBatchApi(String packageClass, String cron_expression, String dess) throws Exception {

		List<Map> li = new ArrayList();
		Map map = new HashMap();
		try {
			Class cl = Class.forName(packageClass);
			String group = getPackage(cl.getName(), cl.getSimpleName());
			Scheduler scheduler = SchedulerProvider.getScheduler();
			info = new SYSTEM_BATCH_INFO1();
			info = scheduleJob(scheduler, cl.getSimpleName(), cl, cron_expression, dess);
			info.setReturnCode(0);
			info.setReturnMesd("Kích hoạt thành công!");
			map.put("batchInfo", info);
		} catch (Exception e) {
			info.setReturnCode(1);
			info.setReturnMesd("batch Đã hoạt động!");
			if (e.getCause() == null) {
				info.setReturnCode(-1);
				info.setReturnMesd("Công việc này không tồn tại!");
				e.printStackTrace();
				throw new Exception("Công việc này không tồn tại!");
			}

			e.printStackTrace();
		}
		li.add(map);
		return li;
	}

	public static String dataToCron(String dateStr) {
		Date date = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(date);
		int minute = calendar.get(java.util.Calendar.MINUTE);
		int hour = calendar.get(java.util.Calendar.HOUR_OF_DAY);
		int dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);
		int month = calendar.get(java.util.Calendar.MONTH) + 1;
		int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
		String cronExpression = String.format("%d %d %d %d %d", minute, hour, dayOfMonth, month, dayOfWeek);
		return cronExpression;
	}

	public List<Map> getListBatchAPI(String PARAM) {
		List<Map> li = new ArrayList();
		try {
			Scheduler scheduler = SchedulerProvider.getScheduler();
			Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.anyJobGroup());
			for (JobKey jobKey : jobKeys) {
				Map map = new HashMap();
				Log.debug(jobKey.getName());
				info = new SYSTEM_BATCH_INFO1();
				SYSTEM_BATCH_INFO1 ifo = new SYSTEM_BATCH_INFO1();
				info = ifo.getSysBatchInfo(jobKey.getName());
				map.put("batchInfo", info);
				li.add(map);
			}
		} catch (SchedulerException e) {
			Map map = new HashMap();
			map.put("data", "err");
			li.add(map);
			e.printStackTrace();
		}

		return li;
	}

	public List<Map> getListBatchAllDB(String PARAM) {
		List<Map> li = new ArrayList();
		Map map = new HashMap();
		map.put("batchInfo", getListBatchAllDb());
		li.add(map);
		return li;
	}

	public List<SYSTEM_BATCH_INFO> getListBatchAllDb() {
		JdbcHelper hp = new JdbcHelper();
		List<Map<String, Object>> li = hp.searchAndRetrieve("com.system.select.status.batchAll");
		List<SYSTEM_BATCH_INFO> list = new ArrayList();
		for (Map<String, Object> map : li) {
			list.add((SYSTEM_BATCH_INFO) new VoTooL().mapToObj(map, new SYSTEM_BATCH_INFO()));
		}

		return list;

	}

//	public List<Map> getListBatchAPI(String PARAM) {
//		List<Map> li = new ArrayList();
//		try {
//			Scheduler scheduler = SchedulerProvider.getScheduler();
//			List<JobExecutionContext> currentlyExecutingJobs = scheduler.getCurrentlyExecutingJobs();
//			for (JobExecutionContext jobExecutionContext : currentlyExecutingJobs) {
//				JobDetail jobDetail = jobExecutionContext.getJobDetail();
//				JobKey jobKey = jobDetail.getKey();
//				Map quartJob = new HashMap();
//				quartJob.put("Group",jobKey.getGroup());
//				quartJob.put("Name",jobKey.getName());
//				quartJob.put("JobStatus",getJobStatus(scheduler, jobKey.getName()));
//				li.add(quartJob);
//			}
//		} catch (SchedulerException e) {
//			e.printStackTrace();
//		}
//		return li;
//	}
	public static String toCronExpression(int type, Date date, Date end_date, String time) {
		SimpleDateFormat dayOfWeekFormat = new SimpleDateFormat("u");
		SimpleDateFormat dayOfMonthFormat = new SimpleDateFormat("d");
		SimpleDateFormat monthFormat = new SimpleDateFormat("M");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		String dayOfWeek = dayOfWeekFormat.format(date);
		String dayOfMonth = dayOfMonthFormat.format(date);
		String month = monthFormat.format(date);
		String year = yearFormat.format(date);
		String[] timeParts = time.split(":");
		String hour = timeParts[0];
		String minute = timeParts[1];
		String cronExpression = "";
		switch (type) {
		case 0:
			cronExpression = "0 " + minute + " " + hour + " * * ?";
			break;
		case 1:
			cronExpression = "0 " + minute + " " + hour + " ? * " + dayOfWeek;
			break;
		case 2:
			cronExpression = "0 " + minute + " " + hour + " " + dayOfMonth + " * ?";
			break;
		case 3:
			cronExpression = "0 " + minute + " " + hour + " " + dayOfMonth + " " + month + " ?";
			break;
		default:
			break;
		}
		if (end_date != null) {
			String endYear = yearFormat.format(end_date);
			cronExpression += " " + year + "-" + endYear;
		}
		return cronExpression;
	}

	public static Object[] fromCronExpression(String cronExpression) throws ParseException {
		String[] parts = cronExpression.split(" ");
		String minute = parts[1];
		String hour = parts[2];
		String dayOfMonth = parts[3];
		String month = parts[4];
		String dayOfWeek = parts[5];
		String yearRange = null;
		if (parts.length > 6) {
			yearRange = parts[6];
		}
		int type = -1;
		Date date = null;
		Date end_date = null;
		String time = hour + ":" + minute;
		if (dayOfMonth.equals("*") && month.equals("*") && dayOfWeek.equals("?")) {
			type = 0;
			date = new Date();
		} else if (dayOfMonth.equals("?") && month.equals("*") && !dayOfWeek.equals("?")) {
			type = 1;
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.set(java.util.Calendar.DAY_OF_WEEK, Integer.parseInt(dayOfWeek));
			date = calendar.getTime();
		} else if (!dayOfMonth.equals("?") && month.equals("*") && dayOfWeek.equals("?")) {
			type = 2;
			SimpleDateFormat dateFormat = new SimpleDateFormat("d-M-yyyy");
			date = dateFormat.parse(dayOfMonth + "-1-2000");
		} else if (!dayOfMonth.equals("?") && !month.equals("*") && dayOfWeek.equals("?")) {
			type = 3;
			SimpleDateFormat dateFormat = new SimpleDateFormat("d-M-yyyy");
			date = dateFormat.parse(dayOfMonth + "-" + month + "-2000");
		}
		if (yearRange != null) {
			String[] years = yearRange.split("-");
			String startYear = years[0];
			String endYear = years[1];
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(java.util.Calendar.YEAR, Integer.parseInt(startYear));
			date = calendar.getTime();
			calendar.set(java.util.Calendar.YEAR, Integer.parseInt(endYear));
			end_date = calendar.getTime();
		}
		return new Object[] { type, date, end_date, time };
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		batchHelper rg = new batchHelper();
		try {
			// rg.excuteBatch("com.dano.web.app.aa.b0.batch.AAB0_0103");
			rg.excuteBatchApi("com.dano.web.app.aa.b0.batch.AAB0_0104", "0/30 * * * * ?", "insert hàng loạt");
			rg.excuteBatchApi("com.dano.web.app.aa.b0.batch.AAB0_0103", "0/30 * * * * ?", "Cập nhập link video");
			// rg.excuteBatchApi("com.dano.web.app.aa.b0.batch.AAB0_0101", "0/30 * * * * ?",
			// "Cập nhập link video");
			// rg.excuteBatchApi("com.dano.web.app.aa.b0.batch.AAB0_0104","0 0 */4 * *
			// ?","Cập nhập link video");
			// 0 0 */4 * * ?

//			int type = 1;
//			Date date = new Date();
//			Date end_date = new Date();
//			String time = "12:00";
//			String cronExpression = rg.toCronExpression(type, date, end_date, time);
//			System.out.println(cronExpression);
//			Object[] result = rg.fromCronExpression(cronExpression);
//			System.out.println(result[0]);
//			System.out.println(result[1]);
//			System.out.println(result[2]);
//			System.out.println(result[3]);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(dataToCron(""));
		// System.out.println("1212");
	}

}
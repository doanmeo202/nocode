package com.dano.web.app.aa.b0.helper;

import java.util.Map;
import java.util.Properties;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;
import org.kurento.client.PlayerEndpoint;
import org.kurento.client.WebRtcEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.dano.web.app.database.vo.apiResult;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class APIControler {
	private final boolean isDebug = log.isDebugEnabled();

	private static Logger log = Logger.getLogger(APIControler.class.getName());
	private static final String encode = "UTF-8";
	private final Class[] StringClass = { String.class };
	private String getURI_PATH = "com.system.apisevice.getURI_PATH";
	private String query_Parner = "com.system.query.API_PARTNER";
	private String resignParner = "com.system.insert.API_PARTNER";
	private String updateParner = "com.system.update.API_PARTNER";

	public boolean check_Key(String PPK, String Parner) {
		JdbcHelper hp = new JdbcHelper();
		Map map = new HashMap();
		map.put("PARNER_ID", Parner);
		List<Map<String, Object>> li = hp.getDataFromKey(query_Parner, map);
		for (Map<String, Object> map2 : li) {
			String PPKDB = ObjectUtils.toString(map2.get("ppk"));
			try {
				String PPK256 = getSHA256(PPKDB);
				System.out.println("PPK: " + PPK256);
				if (PPK256.equals(PPK)) {
					hp.setDataFromKey(updateParner, map);
					return true;
				} else {
					return false;
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	public int resignAPIpartner(String Parner) {
		JdbcHelper hp = new JdbcHelper();
		Map map = new HashMap();
		String dateStr = new Date().toLocaleString();
		try {
			LocalDate today = LocalDate.now();
			LocalDate futureDate = today.plusDays(30);
			map.put("PRIVATE_KEY", getSHA256("PRIVATE_KEY" + Parner + dateStr));
			map.put("PUBLIC_KEY", getSHA256("PUBLIC_KEY" + Parner + dateStr));
			map.put("PARNER_ID", Parner);
			map.put("STOP_TIME", futureDate.toString());
			map.put("IS_CHECK", "Y");
			if (hp.getDataFromKey(query_Parner, map).size() > 0) {
				return 1;
			} else {
				hp.setDataFromKey(resignParner, map);
				return 0;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public String getSHA256(String data) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] encodedhash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
		return bytesToHex(encodedhash);
	}

	private static String bytesToHex(byte[] hash) {
		StringBuilder hexString = new StringBuilder(2 * hash.length);
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}

	/*
	 * @RequestMapping(value = "/api-get-video", produces = "video/mp4") public void
	 * getVideo(@RequestParam("FILE_ID") String fileId, HttpServletRequest request,
	 * HttpServletResponse response) throws IOException { String referer =
	 * ObjectUtils.toString(request.getHeader("Referer"), ""); if
	 * (!isValidReferer(referer)) { ObjectMapper mapper = new ObjectMapper();
	 * request.setCharacterEncoding(encode); response.setCharacterEncoding(encode);
	 * response.setContentType("application/json; charset=" + encode); Map api = new
	 * HashMap(); api.put("Opps",
	 * "Bạn không thể thực hiện thao tác này, IP bạn chưa được cấp phép truy cập!");
	 * response.getWriter().write(mapper.writeValueAsString(api)); } else { String
	 * fileUrl = getFileUrl(fileId); URL url = new URL(fileUrl); URLConnection conn
	 * = url.openConnection(); InputStream inputStream = conn.getInputStream(); //
	 * response.setContentType(conn.getContentType());
	 * response.setContentLength(conn.getContentLength());
	 * response.setHeader("Content-Disposition", "attachment; filename=\"" +
	 * FilenameUtils.getName(fileUrl) + "\""); IOUtils.copy(inputStream,
	 * response.getOutputStream()); inputStream.close(); }
	 * 
	 * }
	 * 
	 * private boolean isValidReferer(String referer) { // String allowedDomain =
	 * "http://127.0.0.1"; // Change this to your domain boolean check = false;
	 * JdbcHelper hp = new JdbcHelper(); List<Map<String, Object>> liAllowedDomain =
	 * hp.searchAndRetrieve("com.telegram.template.getDomain"); if
	 * (referer.equals("")) { check = false; return check; } try { URI refererUri =
	 * new URI(referer); for (Map<String, Object> map : liAllowedDomain) { String
	 * domain = ObjectUtils.toString(map.get("domain")); String is_accept =
	 * ObjectUtils.toString(map.get("is_accept"), "N"); if
	 * (domain.equals(refererUri.getScheme() + "://" + refererUri.getHost())) { if
	 * (!is_accept.equals("N")) { check = true; return check; } } } // return
	 * allowedDomain.equals(refererUri.getScheme() + "://" + //
	 * refererUri.getHost()); } catch (URISyntaxException e) { return check; }
	 * return check; }
	 */

	@RequestMapping(value = "/stream-video")
	public void streamVideo(@RequestParam("FILE_ID") String fileId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		byte[] videoData = VideoBufferManager.getVideoFromBuffer(fileId);

		if (videoData != null) {
			// Video exists in the buffer, write it to the output stream
			response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
			response.setHeader("Content-Disposition", "attachment; filename=" + fileId + ".mp4");
			response.getOutputStream().write(videoData);
			response.flushBuffer();
		} else {
			// Video doesn't exist in the buffer, stream it as before
			try {
				String videoUrl = getFileUrl(fileId);
				URL url = new URL(videoUrl);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.connect();

				if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = new BufferedInputStream(connection.getInputStream());
					response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
					response.setHeader("Content-Disposition", "attachment; filename=" + fileId + ".mp4");

					byte[] buffer = new byte[4096];
					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					int bytesRead;
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						if (isClientDisconnected(request)) {
							break;
						}
						baos.write(buffer, 0, bytesRead);
					}

					videoData = baos.toByteArray();
					VideoBufferManager.addVideoToBuffer(fileId, videoData);

					response.getOutputStream().write(videoData);
					response.flushBuffer();
				} else {
					System.out.println("Không thể kết nối đến URL video.");
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}
			} catch (IOException e) {
				e.printStackTrace();
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/stream-video-rtc", produces = "text/plain;charset=UTF-8") // method = RequestMethod.POST
	public void streamVideo(@RequestParam("FILE_ID") String fileId, HttpServletResponse response) {
		response.setCharacterEncoding(encode);
		String videoUrl = getFileUrl(fileId);
		String kurentoServerUrl = "ws://webdano.ddns.net:8888/kurento";
		KurentoClient kurento = KurentoClient.create(kurentoServerUrl);
		MediaPipeline pipeline = kurento.createMediaPipeline();
		WebRtcEndpoint webRtcEndpoint = new WebRtcEndpoint.Builder(pipeline).build();
		PlayerEndpoint playerEndpoint = new PlayerEndpoint.Builder(pipeline, videoUrl).build();
		playerEndpoint.connect(webRtcEndpoint);
		webRtcEndpoint.gatherCandidates();
		playerEndpoint.play();
		String sdpOffer = webRtcEndpoint.generateOffer();
		webRtcEndpoint.release();
		playerEndpoint.release();
		pipeline.release();
		try {
			response.getWriter().write(sdpOffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Trả về SDP Offer cho client
		// return sdpOffer;
	}

	private boolean isClientDisconnected(HttpServletRequest request) {
		return request.isAsyncStarted() && request.getAsyncContext().getResponse().isCommitted();
	}

	@RequestMapping(value = "/api-get-video")
	@ResponseBody
	public void getVideo(@RequestParam("FILE_ID") String fileId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String referer = ObjectUtils.toString(request.getHeader("Referer"), "");
//	    if (!isValidReferer(referer)) {
//	        ObjectMapper mapper = new ObjectMapper();
//	        request.setCharacterEncoding(encode);
//	        response.setCharacterEncoding(encode);
//	        response.setContentType("application/json; charset=" + encode);
//	        Map<String, Object> api = new HashMap<>();
//	        api.put("Opps", "Bạn không thể thực hiện thao tác này, IP bạn chưa được cấp phép truy cập!");
//	        response.getWriter().write(mapper.writeValueAsString(api));
//	    } else {
//	       
//	    }
		String fileUrl = getFileUrl(fileId);
		URL url = new URL(fileUrl);
		response.setContentType("video/mp4");
		response.setHeader("Content-Disposition", "inline; filename=\"" + fileId + ".mp4\"");
		try (InputStream inputStream = url.openStream()) {
			IOUtils.copy(inputStream, response.getOutputStream());
		}
	}

	@RequestMapping(value = "/api-get-video-stream", produces = "video/mp4")
	public void getVideoStream(@RequestParam("FILE_ID") String fileId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String referer = ObjectUtils.toString(request.getHeader("Referer"), "");
		if (!isValidReferer(referer)) {
			ObjectMapper mapper = new ObjectMapper();
			request.setCharacterEncoding(encode);
			response.setCharacterEncoding(encode);
			response.setContentType("application/json; charset=" + encode);
			Map<String, Object> api = new HashMap<>();
			api.put("Opps", "Bạn không thể thực hiện thao tác này, IP bạn chưa được cấp phép truy cập!");
			response.getWriter().write(mapper.writeValueAsString(api));
		} else {
			String fileUrl = getFileUrl(fileId);
			response.sendRedirect(fileUrl);
		}
	}

	private boolean isValidReferer(String referer) {
		boolean check = false;
		JdbcHelper hp = new JdbcHelper();
		List<Map<String, Object>> liAllowedDomain = hp.searchAndRetrieve("com.telegram.template.getDomain");
		if (referer.equals("")) {
			return check;
		}
		try {
			URI refererUri = new URI(referer);
			for (Map<String, Object> map : liAllowedDomain) {
				String domain = ObjectUtils.toString(map.get("domain"));
				String is_accept = ObjectUtils.toString(map.get("is_accept"), "N");
				if (domain.equals(refererUri.getScheme() + "://" + refererUri.getHost())) {
					if (!is_accept.equals("N")) {
						check = true;
						return check;
					}
				}
			}
		} catch (URISyntaxException e) {
			return check;
		}
		return check;
	}

	private String getFileUrl(String fileId) {
		JdbcHelper hp = new JdbcHelper();
		hp.setField("FILE_ID", fileId);
		List<Map<String, Object>> li = hp.searchAndRetrieve("com.telegram.template.getVideoFromFileID");
		String fileUrl = "";
		for (Map map : li) {
			fileUrl = ObjectUtils.toString(map.get("url"));
			log.debug(fileUrl);
		}
		return fileUrl;
	}

	@RequestMapping(value = "/api/**", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public void service(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) throws ServletException, IOException {
		apiResult<Object> api = new apiResult<Object>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String PARNER_ID = request.getParameter("PARNER_ID");
			String PPK = request.getParameter("PPK");

			// resignAPIpartner("webdano");
			request.setCharacterEncoding(encode);
			response.setCharacterEncoding(encode);
			response.setContentType("application/json; charset=" + encode);
			String ipAddressClient = ObjectUtils.toString(request.getHeader("X-Real-IP"));
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			String url = request.getRequestURI();
			int indexUrl = url.indexOf("/api/");

			api.getOutputData(0, "", "", "", request, response);
			if (api.getReturnCode() != 0) {
				response.getWriter().write(mapper.writeValueAsString(api));
				return;
			}
			JdbcHelper hp = new JdbcHelper();

			String tempURL = request.getRequestURI().substring(indexUrl + 5); // 去掉/api/
			hp.setField("URI_PATH", tempURL);
			List<Map<String, Object>> li = hp.searchAndRetrieve(getURI_PATH);

			if (li.size() < 1) {
				String responseToClient = "Từ chối kết nối, api chưa được triển khai.";
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				api.setStatus("SC_BAD_REQUEST");
				api.setReturnCodePage(HttpServletResponse.SC_BAD_REQUEST);
				api.setReturnCode(3);
				api.setReturnMess(responseToClient);
				response.getWriter().write(mapper.writeValueAsString(api));

				return;
			}
			String[] CALL_MOD_info = ObjectUtils.toString(li.get(0).get("call_mod")).split("/");// [0]class name [1]call

			if ("Y".equals(ObjectUtils.toString(li.get(0).get("need_check")).toUpperCase())) {
				if (!check_Key(PPK, PARNER_ID)) {
					String responseToClient = "Từ chối kết nối,Thông tin đối tác không chính xác hoặc chưa đăng ký";
					response.setStatus(HttpServletResponse.SC_ACCEPTED);
					// response.getWriter().write(responseToClient);
					api.setStatus("SC_ACCEPTED");
					api.setReturnCodePage(HttpServletResponse.SC_ACCEPTED);
					api.setReturnCode(3);
					api.setReturnMess(responseToClient);
					response.getWriter().write(mapper.writeValueAsString(api));
					return;
				}
			}
			// mod
			List<Map> reqArr = new ArrayList();

			Class[] Class_type = new Class[li.size()];
			Object[] PARAM_DATA = new Object[li.size()];
			int i = 0;
			for (Map item : li) {
				Map reqItem = new HashMap();
				// {"SER_NO":"1","PARAM_NAME":"CSIDNO","PARAM_DATA":"A000000168","CLASS_TYPE":"java.lang.String"}
				try {

					String param = ObjectUtils
							.toString(request.getParameter(ObjectUtils.toString(item.get("param_name"))));
					Class primitiveType = null;
					log.debug("Get request          : " + request.getParameterMap());
					Class_type[i] = Class.forName(ObjectUtils.toString(item.get("param_type")));
					PARAM_DATA[i] = param;
					i++;
					reqItem.put("SER_NO", item.get("ser_no"));
					reqItem.put("PARAM_DATA", param);
					reqItem.put("CLASS_TYPE", item.get("param_type"));
					if (!"".equals(ObjectUtils.toString(item.get("param_name")))) {
						if ("".equals(param)) {

							param = ObjectUtils.toString(params.get(ObjectUtils.toString(item.get("param_name"))));
							if ("".equals(param)) {
								response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
								// response.getWriter().write("Thông số yêu cầu không được để trống :`"
								// + ObjectUtils.toString(item.get("param_name")).replaceAll(" ", "") + "`");
								api.setStatus("SC_BAD_REQUEST");
								api.setReturnCodePage(HttpServletResponse.SC_BAD_REQUEST);
								api.setReturnCode(3);
								api.setReturnMess("Thông số yêu cầu không được để trống :`"
										+ ObjectUtils.toString(item.get("param_name")).replaceAll(" ", "") + "`");
								response.getWriter().write(mapper.writeValueAsString(api));
								return;
							}

						}
					}
					reqArr.add(reqItem);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			String reqJson = mapper.writeValueAsString(reqArr);
			if (isDebug) {
				log.debug("=============================================================================");
				log.debug("Get IP from request          : " + ipAddress);
				log.debug("Get IP from ipAddress Client : " + ipAddressClient);
				log.debug("Get reqJson from request     : " + reqJson);
				log.debug("Get tempURL from request     : " + tempURL);
				log.debug("Get CLASS_NAME from request  : " + CALL_MOD_info[2]);
				log.debug("Get CALL_MOD from request    : " + CALL_MOD_info[3]);
				log.debug("=============================================================================");
			}
			Class<?> clazz = Class.forName(CALL_MOD_info[2]);
			Method method = clazz.getMethod(CALL_MOD_info[3], Class_type);
			Object instance = clazz.getConstructor().newInstance();
			List<Map<String, Object>> result = (List<Map<String, Object>>) method.invoke(instance, PARAM_DATA);
			log.debug("result = " + result);
			response.setStatus(HttpServletResponse.SC_OK);
			api.setData(result);
			api.setReturnCodePage(HttpServletResponse.SC_OK);
			api.setReturnMess("");
			api.setStatus("SUCCESS");
			response.getWriter().write(mapper.writeValueAsString(api));
		} catch (Exception e) {
			log.fatal(e, e);

			String errMsg = e.getMessage();

			if (e instanceof InvocationTargetException) {
				Throwable t = ((InvocationTargetException) e).getTargetException();

				errMsg = t.getMessage();
			}
			if (e instanceof NoSuchMethodException) {
				errMsg = "Phương thức không tồn tại: NoSuchMethodException";
			}
			if (e instanceof ClassNotFoundException) {
				errMsg = "Không tìm thấy class được gọi đến: ClassNotFoundException";
			}
			if (StringUtils.isBlank(errMsg)) {
				errMsg = "Đã xảy ra lỗi trong mô-đun cuộc gọi";
			}

			// response.getWriter().write(errMsg);
			api.setStatus("SC_BAD_REQUEST");
			api.setReturnCodePage(HttpServletResponse.SC_BAD_REQUEST);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			api.setReturnCode(3);

			if (errMsg == null) {
				api.setReturnMess(e.getMessage());
				api.setReturnEx(e.getStackTrace().toString());
			} else {
				api.setReturnMess(errMsg);
				api.setReturnEx(e.getMessage());
			}
			response.getWriter().write(mapper.writeValueAsString(api));
		}
	}
}

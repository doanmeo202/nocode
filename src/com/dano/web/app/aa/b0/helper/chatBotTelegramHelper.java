package com.dano.web.app.aa.b0.helper;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
public class chatBotTelegramHelper extends TelegramLongPollingBot {
	private String botUsername;
	private String botToken;
	private String chatId;
	private TelegramBotsApi botsApi;
	private SendMessage sendMessage;
	static {
		BasicConfigurator.configure();
		Properties properties = new Properties();
		properties.setProperty("log4j.rootCategory", "DEBUG, all");
		properties.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		properties.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		properties.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		properties.setProperty("log4j.appender.NotConsole", "org.apache.log4j.RollingFileAppender");
		properties.setProperty("log4j.appender.NotConsole.fileName", "D:\\Log\\ChatBotConfig.log");
		properties.setProperty("log4j.appender.NotConsole.maxFileSize", "20MB");
		PropertyConfigurator.configure(properties);
	}

	public boolean registerBot() {
		boolean check = false;
		try {
			botsApi = new TelegramBotsApi(DefaultBotSession.class);
			chatBotTelegramHelper chatbot = new chatBotTelegramHelper();
			chatbot.setBotToken(botToken);
			chatbot.setBotUsername(botUsername);
			chatbot.setChatId(chatId);
			botsApi.registerBot(chatbot);
			check = true;
		} catch (Exception e) {
			System.err.println(e);
		}
		return check;
	}

	private void sendMess(String mess) throws TelegramApiException {
		if (chatId == null) {
			throw new TelegramApiException("chatID is null");
		}
		sendMessage = new SendMessage();
		sendMessage.setText(mess);
		sendMessage.setChatId(chatId);
		execute(sendMessage);
	}
	public synchronized void send_SYS_Messenger(String mess) {
		chatBotTelegramHelper baotapviet = new chatBotTelegramHelper();
		baotapviet.setBotToken(Enviroment.SYS_BOT_TOKEN);
		baotapviet.setBotUsername(Enviroment.SYS_BOT_USERNA);
		baotapviet.setChatId(Enviroment.SYS_BOT_CHATID);
		if (baotapviet.registerBot()) {
			try {
				baotapviet.sendMess(mess);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

	public String getBotToken() {
		return botToken;
	}

	public void setBotToken(String botToken) {
		this.botToken = botToken;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public void setBotUsername(String botUsername) {
		this.botUsername = botUsername;
	}

	@Override
	public void onUpdateReceived(Update update) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getBotUsername() {
		return botUsername;
	}

	public static void main(String[] args) {
		chatBotTelegramHelper chat = new chatBotTelegramHelper();
		chat.send_SYS_Messenger("Send From SYS");

	}

}

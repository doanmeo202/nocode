package com.dano.web.app.aa.b0.helper;

import com.squareup.javapoet.*;
import com.squareup.javapoet.TypeSpec.Builder;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

public class createObject_Helper {
	/** 基礎型別對應 */
	private static final Map TYPE_MAPPING = new HashMap();

	/** 基礎型別對應 */
	private static final Map PRIMITIVE_MAPPING = new HashMap();
	static {

		TYPE_MAPPING.put("byte", java.lang.Byte.class.getName());
		TYPE_MAPPING.put("char", java.lang.Character.class.getName());
		TYPE_MAPPING.put("short", java.lang.Short.class.getName());
		TYPE_MAPPING.put("int", java.lang.Integer.class.getName());
		TYPE_MAPPING.put("boolean", java.lang.Boolean.class.getName());
		TYPE_MAPPING.put("long", java.lang.Long.class.getName());
		TYPE_MAPPING.put("double", java.lang.Double.class.getName());
		TYPE_MAPPING.put("float", java.lang.Float.class.getName());
		TYPE_MAPPING.put("String", java.lang.String.class.getName());
		TYPE_MAPPING.put("java.lang.String", java.lang.String.class.getName());

		PRIMITIVE_MAPPING.put("byte", Byte.TYPE);
		PRIMITIVE_MAPPING.put("char", Character.TYPE);
		PRIMITIVE_MAPPING.put("short", Short.TYPE);
		PRIMITIVE_MAPPING.put("int", Integer.TYPE);
		PRIMITIVE_MAPPING.put("boolean", Boolean.TYPE);
		PRIMITIVE_MAPPING.put("long", Long.TYPE);
		PRIMITIVE_MAPPING.put("double", Double.TYPE);
		PRIMITIVE_MAPPING.put("float", Float.TYPE);
		PRIMITIVE_MAPPING.put("String", "".getClass());
		PRIMITIVE_MAPPING.put("java.lang.String", "".getClass());

	}

	public static void tempClass(String Table) throws SQLException {
		String SQL = "SELECT * FROM " + Table + "  limit 1";
		System.out.println(SQL);
		JdbcHelper helper = new JdbcHelper();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(helper.getuRL(), helper.getuSER(), helper.getpASSWORD());
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(SQL);

			List<String> columnNames = new ArrayList<>();
			ResultSetMetaData rsmd = rs.getMetaData();
			List<FieldSpec> liFieldSpec = new ArrayList<FieldSpec>();
			List<MethodSpec> liMethodSpec = new ArrayList<MethodSpec>();
			String pram = "";
			String pramValue = "";
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				Object value = rsmd.getColumnName(i);

				if (i == rsmd.getColumnCount()) {
					pram += rsmd.getColumnName(i);
					pramValue += "'\"+this." + rsmd.getColumnName(i) + "+\"'";

				} else {

					pram += rsmd.getColumnName(i) + ",";
					pramValue += "'\"+this." + rsmd.getColumnName(i) + "+\"',";
					// pramValue += ":" + rsmd.getColumnName(i)+ ",";
				}
				System.out.println(rsmd.getColumnName(i));

				if (value instanceof Integer) {
					FieldSpec nameField = FieldSpec.builder(TypeName.INT, rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PRIVATE).build();
					liFieldSpec.add(nameField);
					MethodSpec getNameMethod = MethodSpec.methodBuilder("get" + rsmd.getColumnName(i))
							.returns(int.class).addStatement("return this." + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					MethodSpec setNameMethod = MethodSpec.methodBuilder("set" + rsmd.getColumnName(i))
							// .addModifiers(Modifier.PUBLIC)
							.addParameter(int.class, rsmd.getColumnName(i))
							.addStatement("this." + rsmd.getColumnName(i) + " =" + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					liMethodSpec.add(getNameMethod);
					liMethodSpec.add(setNameMethod);
				} else if (value instanceof String) {
					FieldSpec nameField = FieldSpec.builder(String.class, rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PRIVATE).build();
					liFieldSpec.add(nameField);
					MethodSpec getNameMethod = MethodSpec.methodBuilder("get" + rsmd.getColumnName(i))
							.returns(String.class).addStatement("return this." + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					MethodSpec setNameMethod = MethodSpec.methodBuilder("set" + rsmd.getColumnName(i))
							// .addModifiers(Modifier.PUBLIC)
							.addParameter(String.class, rsmd.getColumnName(i))
							.addStatement("this." + rsmd.getColumnName(i) + " =" + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					liMethodSpec.add(getNameMethod);
					liMethodSpec.add(setNameMethod);

				} else if (value instanceof Date) {
					FieldSpec nameField = FieldSpec.builder(Date.class, rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PRIVATE).build();
					liFieldSpec.add(nameField);
					MethodSpec getNameMethod = MethodSpec.methodBuilder("get" + rsmd.getColumnName(i))
							.returns(Date.class).addStatement("return this." + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					MethodSpec setNameMethod = MethodSpec.methodBuilder("set" + rsmd.getColumnName(i))
							.addParameter(Date.class, rsmd.getColumnName(i))
							.addStatement("this." + rsmd.getColumnName(i) + " =" + rsmd.getColumnName(i))
							.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
					liMethodSpec.add(getNameMethod);
					liMethodSpec.add(setNameMethod);
				}

			}
//			String insertSQL = "INSERT INTO " + Table + " (" + pram + ") VALUES(" + pramValue + ")";
//			MethodSpec publicMethod = MethodSpec.methodBuilder(Table)
//					.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
//			MethodSpec publicMethodGetSQL = MethodSpec.methodBuilder("getSqlInsert").returns(String.class)
//					.addStatement("return " + "\"" + insertSQL + "\"")
//					.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
//
//			liMethodSpec.add(publicMethod);
//			liMethodSpec.add(publicMethodGetSQL);
			Builder personClass = TypeSpec.classBuilder(Table);
			for (FieldSpec fieldSpec : liFieldSpec) {
				personClass.addField(fieldSpec);
			}
			for (MethodSpec methodSpec : liMethodSpec) {
				personClass.addMethod(methodSpec);
			}
			TypeSpec personClass_ = personClass.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
			String voPath = "com.dano.web.app.database.vo";
			JavaFile javaFile = JavaFile.builder(voPath, personClass_).build();
			try {
				File file = new File("./src/");
				javaFile.writeTo(file);
				System.out.println(file.getAbsolutePath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(javaFile.toString());
		} catch (SQLException e) {
			System.out.println("Error querying the database");
			conn.rollback();
			e.printStackTrace();
		} finally {
			conn.close();
			System.out.println("done!!!");

		}
	}


	public static void main(String[] args) {
		try {
			JdbcHelper helper = new JdbcHelper();
			Map map = new HashMap();
			map.put("TABLE_SCHEMA", "DB_STAG");
			List<Map<String, Object>> li = helper.getDataFromKey("com.system.selecttablename", map);
			for (Map map2 : li) {
				System.out.println(ObjectUtils.toString(map2.get("table_name")).equals(""));
				if (!ObjectUtils.toString(map2.get("table_name")).equals("")) {
					tempClass(ObjectUtils.toString(map2.get("table_name")));
				}

			}
//			Object value = rs.getObject(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

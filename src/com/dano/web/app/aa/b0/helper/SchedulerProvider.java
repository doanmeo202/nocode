package com.dano.web.app.aa.b0.helper;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.socket.WebSocketSession;

public class SchedulerProvider {
	private static Scheduler scheduler;
	static {
		try {
			SchedulerFactory schedulerFactory = new StdSchedulerFactory();
			jobListener_Helper  jhb= new  jobListener_Helper();
			scheduler = schedulerFactory.getScheduler();
			if(!scheduler.isStarted()) {
				scheduler.start();
			}
			scheduler.getListenerManager().addJobListener(jhb);
		} catch (SchedulerException e) {
	        e.printStackTrace();
		}
	}

	public static Scheduler getScheduler() {
		return scheduler;
	}

}

package com.dano.web.app.aa.b0.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

public class batchQueryDataSet {
	private static Logger logger = Logger.getLogger(batchQueryDataSet.class);
	private static String tableCodeMap = Enviroment.CODEMAP_TABLE;
	private Sql2o batchSql2o;
	public static Query query;
	private static Connection connection;

	public batchQueryDataSet() {
		batchSql2o = new Sql2o(Enviroment.URL_CONNECT_DB, Enviroment.USERNAME_DB, Enviroment.PASSWORD_DB);
	}

	public batchQueryDataSet(String url, String username, String password) {
		batchQueryDataSet bqds = new batchQueryDataSet();
		this.batchSql2o = new Sql2o(url, username, password);
	}

	public Connection beginTransaction() {
		batchSql2o.open();
		connection = batchSql2o.beginTransaction();
		logger.fatal("beginTransaction");
		return connection;
	}

	public static Connection rollbackTransaction() {
		connection.rollback();
		logger.fatal("rollbackTransaction");
		return connection;
	}

	public static Connection endTransaction() {
		if (connection != null) {
			connection.close();
			logger.fatal("endTransaction");
		}
		return connection;
	}

	public static Query createQuery(String sql) {
		query = connection.createQuery(sql);
		return query;
	}

	public static Query setField(String name, Object value) {
		try {
			query.addParameter(name, value);
		} catch (Exception e) {
			logger.debug("setField Failed: [" + name + "] - [" + value + "]");
		}
		return query;
	}

	public Query setQuery(Map map) {
		ArrayList<String> myKeyList = new ArrayList<>(map.keySet());
		Map<String, List<Integer>> mapCheckItem = query.getParamNameToIdxMap();
		List<String> missingParams = new ArrayList<>();

		for (String paramName : mapCheckItem.keySet()) {
			if (!map.containsKey(paramName)) {
				missingParams.add(paramName);
			}
		}

		if (!missingParams.isEmpty()) {
			StringBuilder errorMessage = new StringBuilder("MISSING PARAM: [:");
			for (String missingParam : missingParams) {
				errorMessage.append(missingParam + "]").append(", ");
			}
			errorMessage.delete(errorMessage.length() - 2, errorMessage.length());
			logger.fatal(errorMessage.toString());

			throw new IllegalArgumentException(errorMessage.toString());
		}

		for (String key : myKeyList) {
			Object value = (Object) map.get(key);
			try {
				query.addParameter(key, value);
				// BATCH_COUNT++;
				// logger.fatal("setField [" + key + "=" + value + "]");
			} catch (Exception e) {
				if (e.getCause() != null && !e.getCause().equals("null")) {
					logger.fatal("SET PARAM ERR [" + key + "=" + value + "]");
					logger.fatal("=>>>>>>" + e.getCause());
					throw new Sql2oException(e);
				}
			}
		}

		return query;
	}

	public String getSqlFromKey(String keysql) {
		Connection con = beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() > 0) {
				for (Map<String, Object> map : map1) {
					sql = ObjectUtils.toString(map.get("sqlstring"), "");
				}
			}
		} finally {
			endTransaction();
		}
		return sql;
	}

	public List<Map<String, Object>> searchAndRetrieve(String sql) {
		List<List<Map>> li = new ArrayList<List<Map>>();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			String sql1 = getSqlFromKey(sql);
			if (sql1.equals("") || sql1 == null) {
				sql1 = sql;
			}
			beginTransaction();
			list = createQuery(sql1).executeAndFetchTable().asList();

		} catch (Exception e) {
			logger.fatal(sql);
			logger.error(e.getMessage());
			rollbackTransaction();
		} finally {
			endTransaction();
		}
		return list;

	}

	public static void main(String[] args) {
		batchQueryDataSet b = new batchQueryDataSet();
		Map map = new HashMap();
		map.put("SYSID", "AA");

		// b.setQuery(map);

		System.out.println(b.searchAndRetrieve("SELECT * FROM dtcm0004_field_option_list"));
	}
}

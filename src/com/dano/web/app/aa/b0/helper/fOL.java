package com.dano.web.app.aa.b0.helper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.sql2o.Connection;

public class fOL {
	private String sYSID;
	private String fIELD_NAME;
	private String sETTING;
	private String nAME;
	private String fIELD_DESCRIPTION;

	public String getfIELD_DESCRIPTION() {
		return fIELD_DESCRIPTION;
	}

	public void setfIELD_DESCRIPTION(String fIELD_DESCRIPTION) {
		this.fIELD_DESCRIPTION = fIELD_DESCRIPTION;
	}

	public String getsYSID() {
		return sYSID;
	}

	public void setsYSID(String sYSID) {
		this.sYSID = sYSID;
	}

	public String getfIELD_NAME() {
		return fIELD_NAME;
	}

	public void setfIELD_NAME(String fIELD_NAME) {
		this.fIELD_NAME = fIELD_NAME;
	}

	public String getsETTING() {
		return sETTING;
	}

	public void setsETTING(String sETTING) {
		this.sETTING = sETTING;
	}

	public String getnAME() {
		return nAME;
	}

	public void setnAME(String nAME) {
		this.nAME = nAME;
	}

	public void fOL() {

	}

	public void saveDB(Map map) throws Exception {

		ArrayList myKeyList = new ArrayList(map.keySet());
		String pram = "";
		String pramValue = "";
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			if (i == (myKeyList.size() - 1)) {

				pram += key;
				pramValue += "'".concat(value).concat("'");
			} else {
				pram += key + ",";
				pramValue += "'".concat(value).concat("'") + ",";
			}
		}
		String SQL = "INSERT INTO DTCM0004_FIELD_OPTION_LIST (" + pram + ") VALUES(" + pramValue + ")";
		System.out.println(SQL);
		JdbcHelper helper = new JdbcHelper();
		Connection con1 = null;
		try {
			con1 = helper.getConnection();
		} catch (ClassNotFoundException e) {
			con1.close();
			e.printStackTrace();
		}
		try {
			con1.createQuery(SQL).executeUpdate();
			con1.commit();
		} catch (Exception e) {
			con1.rollback();
			throw new Exception(e.getMessage());
		} finally {
			con1.close();
		}

	}

	public List<fOL> getfol(Map map) {
		List<fOL> fOLlist = new ArrayList();
		List<Map<String, Object>> ListArr = new ArrayList();
		ArrayList myKeyList = new ArrayList(map.keySet());
		String pram = "";
		String pramValue = "";
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			if(!"".equals(value)) {
				if (i == 0) {
					pram += key + "=" + "'".concat(value).concat("'") + " ";

				} else {
					pram += " AND " + key + "=" + "'".concat(value).concat("'");
				}
			}
		}
		String SQL = "SELECT * FROM DTCM0004_FIELD_OPTION_LIST WHERE " + pram;

		JdbcHelper helper = new JdbcHelper();
		Connection con1 = null;

		try {
			con1 = helper.getConnection();
		} catch (ClassNotFoundException e) {
			con1.close();
			e.printStackTrace();
		}
		try {
			System.out.println(SQL);
			ListArr = con1.createQuery(SQL).executeAndFetchTable().asList();
			for (int i = 0; i < ListArr.size(); i++) {
				fOLlist.add(setField(ListArr,i));
			}

		} catch (Exception e) {
			con1.rollback();
			e.printStackTrace();
		} finally {
			con1.close();
		}
		return fOLlist;

	}

	public fOL getfolvo(Map map) {
		fOL fOLlist = new fOL();
		List<Map<String, Object>> ListArr = new ArrayList();
		ArrayList myKeyList = new ArrayList(map.keySet());
		String pram = "";
		String pramValue = "";
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			if (i == 0) {
				pram += key + "=" + "'".concat(value).concat("'") + " ";

			} else {
				pram += " AND " + key + "=" + "'".concat(value).concat("'");
			}
		}
		String SQL = "SELECT * FROM DTCM0004_FIELD_OPTION_LIST WHERE " + pram;

		JdbcHelper helper = new JdbcHelper();
		Connection con1 = null;

		try {
			con1 = helper.getConnection();
		} catch (ClassNotFoundException e) {
			con1.close();
			e.printStackTrace();
		}
		try {
			System.out.println(SQL);
			ListArr = con1.createQuery(SQL).executeAndFetchTable().asList();

			if (ListArr.size() > 0) {
				int i = 0;
				fOLlist = setField(ListArr,i);
			}

		} catch (Exception e) {
			con1.rollback();
			e.printStackTrace();
		} finally {
			con1.close();
		}
		return fOLlist;

	}
	public fOL setField(List<Map<String, Object>> listArr,int i) {
		fOL vo = new fOL();
		vo.setsYSID(ObjectUtils.toString(listArr.get(i).get("sysid")));
		vo.setfIELD_NAME(ObjectUtils.toString(listArr.get(i).get("field_name")));
		vo.setsETTING(ObjectUtils.toString(listArr.get(i).get("setting")));
		vo.setnAME(ObjectUtils.toString(listArr.get(i).get("name")));
		vo.setfIELD_DESCRIPTION(ObjectUtils.toString(listArr.get(i).get("field_description")));
		return vo;
	}
	public void save(String sYSID, String fIELD_NAME, String sETTING, String nAME,String fIELD_DESCRIPTION) throws Exception {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		map.put("SETTING", sETTING);
		map.put("NAME", nAME);
		map.put("FIELD_DESCRIPTION", fIELD_DESCRIPTION);
		saveDB(map);
	}
	public void save(String sYSID, String fIELD_NAME, String sETTING, String nAME) throws Exception {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		map.put("SETTING", sETTING);
		map.put("NAME", nAME);
		saveDB(map);
	}

	public void save(String sYSID, String fIELD_NAME, String sETTING) throws Exception {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		map.put("SETTING", sETTING);
		saveDB(map);
	}

	public List<fOL> getFol(String sYSID, String fIELD_NAME, String sETTING) {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		map.put("SETTING", sETTING);
		List<fOL> li = getfol(map);
		return li;
	}

	public List<fOL> getFol(String sYSID, String fIELD_NAME) {

		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		List<fOL> li = getfol(map);
		return li;
	}

	public List<fOL> getFol(String sYSID) {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		List<fOL> li = getfol(map);
		return li;
	}
	public fOL getFol_vo(String sYSID, String fIELD_NAME, String sETTING) {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		map.put("SETTING", sETTING);
		fOL li = getfolvo(map);
		return li;
	}
	public fOL getFol_vo(String sYSID, String fIELD_NAME) {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		map.put("FIELD_NAME", fIELD_NAME);
		fOL li = getfolvo(map);
		return li;
	}
	public fOL getFol_vo(String sYSID) {
		Map map = new HashMap();
		map.put("SYSID", sYSID);
		fOL li = getfolvo(map);
		return li;
	}
	public static void main(String args[]) {
		try {
			System.out.println(new fOL().getFol("AA", "", "").get(0).getfIELD_NAME());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

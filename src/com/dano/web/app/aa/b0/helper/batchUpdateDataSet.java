package com.dano.web.app.aa.b0.helper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.jcodec.common.ArrayUtil;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;
import com.telegram.core.ChatBotConfig;

import org.sql2o.*;

public class batchUpdateDataSet {

	private static Logger logger = Logger.getLogger(batchUpdateDataSet.class);
	private String tableCodeMap = Enviroment.CODEMAP_TABLE;
	private static int BATCH_SIZE = 0;
	private static int BATCH_COUNT;
	private static Sql2o batchSql2o;
	public Query query;
	private static ChatBotConfig SendMessErr;
	private static Connection connection;
	private static String SYS_ID;
	private static String BATCH_NAME;
	private static String GROUP_BATCH;
	private static String EROR;
	private static String STATUS;
	private static boolean notify;
	private static boolean autoCommit;
	private static Map mapSetField = new HashMap();

	public static void clear() {
		mapSetField.clear();
	}

	public Query setField(String key, Object value) {
		query.addParameter(key, value);
		return query;
	}

	static {
		JdbcHelper hp = new JdbcHelper();
		SendMessErr = new ChatBotConfig();
		SendMessErr.setBotToken(Enviroment.SYS_BOT_TOKEN);
		SendMessErr.setBotUsername(Enviroment.SYS_BOT_USERNA);
		SendMessErr.setChatId(Enviroment.SYS_BOT_CHATID);
		batchSql2o = new Sql2o(Enviroment.URL_CONNECT_DB, Enviroment.USERNAME_DB, Enviroment.PASSWORD_DB);
		batchSql2o.open();
		notify = false;
		autoCommit = false;
		BATCH_COUNT = 0;
	}

	public batchUpdateDataSet() {

	}

	public batchUpdateDataSet(String url, String username, String password) {
		this.batchSql2o = new Sql2o(url, username, password);
	}

	public void setUpBatchInfo(Class<?> clazz) {
		this.SYS_ID = clazz.getSimpleName();
		this.BATCH_NAME = clazz.getSimpleName();
		this.GROUP_BATCH = clazz.getName();
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	public Connection beginTransaction() {
		connection = batchSql2o.beginTransaction();
		logger.fatal("beginTransaction");
		return connection;
	}

	public Connection rollbackTransaction() {
		connection.rollback();
		logger.fatal("rollbackTransaction");
		return connection;
	}

	public Connection endTransaction() {
		if (connection != null) {
			connection.close();
			logger.fatal("endTransaction");
		}
		return connection;
	}

	public static void setBATCH_SIZE(int bATCH_SIZE, int dataSize) {
		BATCH_SIZE = bATCH_SIZE;
		int batchCount = (int) Math.ceil((double) dataSize / BATCH_SIZE);
		BATCH_COUNT = batchCount;
	}

	public Query addBatch() {
		return query.addToBatch();
	}

	public Query preparedBatch(String sql) {
		query = connection.createQuery(sql);
		return query;
	}

	public Sql2o commit() {
		return connection.commit();
	}

	@SuppressWarnings("finally")
//	public int[] executeBatch(boolean autoComit) {
//
//		int[] result = null;
//		Connection con = null;
//		try {
//			try {
//				query.getConnection().getJdbcConnection().setAutoCommit(autoComit);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//			con = query.executeBatch();
//			result = con.getBatchResult();
//		} finally {
//			commit();
//			return result;
//		}
//
//	}
	public int[] executeBatch(boolean autoComit) throws SQLException {
		int[] result = null;
		try {
			query.getConnection().getJdbcConnection().setAutoCommit(autoComit);
			result = query.executeBatch().getBatchResult();
			connection.commit();
		} catch (Sql2oException e) {
			connection.rollback();
			logger.error(e.getMessage());
			// bạn có thể log lỗi ở đây hoặc bỏ qua nó và tiếp tục commit
			connection.commit();
		} finally {
			if (!autoComit) {
				connection.close();
			}
		}
		return result;
	}

	public Query setQuery(Map map) {
		ArrayList<String> myKeyList = new ArrayList<>(map.keySet());
		Map<String, List<Integer>> mapCheckItem = query.getParamNameToIdxMap();
		List<String> missingParams = new ArrayList<>();

		for (String paramName : mapCheckItem.keySet()) {
			if (!map.containsKey(paramName)) {
				missingParams.add(paramName);
			}
		}

		if (!missingParams.isEmpty()) {
			StringBuilder errorMessage = new StringBuilder("MISSING PARAM: [:");
			for (String missingParam : missingParams) {
				errorMessage.append(missingParam + "]").append(", ");
			}
			errorMessage.delete(errorMessage.length() - 2, errorMessage.length());
			logger.fatal(errorMessage.toString());

			throw new IllegalArgumentException(errorMessage.toString());
		}

		for (String key : myKeyList) {
			Object value = (Object) map.get(key);
			try {
				query.addParameter(key, value);
				BATCH_COUNT++;
				logger.fatal("ADD_TO_BATCH [" + key + "=" + value + "]");
			} catch (Exception e) {
				if (e.getCause() != null && !e.getCause().equals("null")) {
					logger.fatal("SET PARAM ERR [" + key + "=" + value + "]");
					logger.fatal("=>>>>>>" + e.getCause());

					throw new Sql2oException(e);
				}
			}
		}

		return query;
	}

	public static void main(String[] args) {
		batchUpdateDataSet buds = new batchUpdateDataSet();

//		try {
//			String sql = // buds.getSqlFromKey("com.dano.web.app.aa.b1.modun.AAB1_0100_PUT_IP");
//					// "UPDATE cxlaa_aab1_0100_ip SET PATH='AAAAAAAAA' WHERE IP=:IP";
//					"DELETE FROM cxlaa_aab1_0100_ip";
//			buds.setUpBatchInfo(batchUpdateDataSet.class);
//			buds.beginTransaction();
//			buds.preparedBatch(sql);
//			buds.setAutoCommit(false);
//			// buds.setNotify(true);
//			List<Map> li = new ArrayList();
//			for (int i = 0; i < 10; i++) {
//				Map<String, Object> map = new HashMap<>();
//				map.put("IP", i);
//				buds.setQuery(map);
//				buds.addBatch();
//			}
//			int i = buds.executeBatch();
//
//			// b.handleBatchErrors(batchResult);
//		} finally {
//			buds.endTransaction();
//		}
		try {
			// String sql = buds.getSqlFromKey("UPDATE cxlaa_aab1_0100_ip SET
			// PATH='AAAAAAAAA' WHERE IP=:IP");
			String sql = "INSERT INTO cxlaa_aab1_0100_ip (IP, `PATH`) VALUES(:IP, 'AAAAAAAAAAAA')";
			System.out.println(sql + " AAAAAAAAAAAAAAAAAA");
			// "UPDATE cxlaa_aab1_0100_ip SET PATH='AAAAAAAAA' WHERE IP=:IP";
			// "DELETE FROM cxlaa_aab1_0100_ip WHERE IP=:IP";
			buds.setUpBatchInfo(batchUpdateDataSet.class);
			buds.beginTransaction();
			buds.preparedBatch(sql);
			buds.setAutoCommit(true);
			// buds.setNotify(true);
			List<Map> li = new ArrayList();
			for (int i = 0; i < 110; i++) {
				Map<String, Object> map = new HashMap<>();
				map.put("IP", i);
				buds.setQuery(map);
				buds.addBatch();
			}
			try {
				System.out.println(buds.executeBatch(true));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// b.handleBatchErrors(batchResult);
		} finally {
			buds.endTransaction();
		}
	}

	public Connection execute() {
		return query.executeBatch();
	}

	public void setAutoCommit(boolean autoCommit) {
		try {
			this.autoCommit = autoCommit;
			connection.getJdbcConnection().setAutoCommit(autoCommit);
			query.getConnection().getJdbcConnection().setAutoCommit(autoCommit);

		} catch (SQLException e) {
			this.autoCommit = false;
		}
	}

	public int getBatchMaxAdd() {
		return (int) Math.ceil((double) BATCH_COUNT / BATCH_SIZE);
	}

	public int executeBatch() {
		int checkStatus = -1;
		try {

			connection = query.executeBatch();
			if (!this.autoCommit) {
				connection.commit();
			}
			this.EROR = "";
			this.STATUS = "【COMPLETE!】 ";
			checkStatus = 0;
		} catch (Sql2oException e) {
			this.EROR = e.getMessage();
			this.STATUS = "【FAILED!】 ";

			if (!this.autoCommit) {
				connection.rollback();
			}
			logger.error(e.getMessage());
			if (e.getMessage().contains("Error while executing batch operation: Duplicate entry '")) {
				checkStatus = 1;
			}	
		}
		if (notify) {
			if (!autoCommit) {
				String mess = "[" + BATCH_NAME + "] " + STATUS + " " + EROR;
				SendMessErr.sendMessenger(mess);
			}
		} else {
			logger.info("[" + BATCH_NAME + "] " + STATUS + " " + EROR);
		}
		return checkStatus;

	}

	public String getSqlFromKey(String keysql) {
		Connection con = beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() > 0) {
				for (Map<String, Object> map : map1) {
					sql = ObjectUtils.toString(map.get("sqlstring"), "");
				}
			}
		} finally {
			endTransaction();

		}
		return sql;
	}

}

package com.dano.web.app.aa.b0.helper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dano.web.app.aa.b0.modun.AAB0_0100_mod;
import com.google.api.client.util.Key;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

public class fileHelper {
	private static String INSERT_UPLOADFILE = "com.dano.web.app.aa.b1.modun.AAB1_0100_INSERT_UPLOAD_FILE";
	private static String osName = ObjectUtils.toString(System.getProperties().get("os.name"));

	public static String getPathTomcat() {
		String path = "";
		JdbcHelper hp = new JdbcHelper();
		AAB0_0100_mod mod = new AAB0_0100_mod();

		List<fOL> liFol = new fOL().getFol("SYS", "TOMCAT");
		for (fOL f : liFol) {
			if (osName.toUpperCase().startsWith(f.getsETTING().toUpperCase())) {
				path = f.getnAME();
			}
		}

		return path;
	}

	public static String getPath(String Path) {
		String path = "";
		JdbcHelper hp = new JdbcHelper();
		AAB0_0100_mod mod = new AAB0_0100_mod();

		List<fOL> liFol = new fOL().getFol("SYS", Path);
		for (fOL f : liFol) {
			if (osName.toUpperCase().startsWith(f.getsETTING().toUpperCase())) {
				path = f.getnAME();
			}
		}

		return path;
	}

	public static String getPathUpload() {
		String path = "";
		JdbcHelper hp = new JdbcHelper();
		AAB0_0100_mod mod = new AAB0_0100_mod();

		List<fOL> liFol = new fOL().getFol("SYS", "UPLOAD");
		for (fOL f : liFol) {
			if (osName.toUpperCase().startsWith(f.getsETTING().toUpperCase())) {
				path = f.getnAME();
			}
		}

		return path;
	}

	public static String saveFile(String Path, String fileName, MultipartFile multipartFile) throws IOException {
		Path uploadPath = Paths.get(getPathUpload());
		Map map = new HashMap();
		if (!Files.exists(uploadPath)) {
			Files.createDirectories(uploadPath);
		}
		if (!"".equals(Path)) {
			uploadPath = Paths.get(Path);
		}
		String fileCode = RandomStringUtils.randomAlphanumeric(8);
		try (InputStream inputStream = multipartFile.getInputStream()) {
			// Path filePath = uploadPath.resolve(fileCode + "-" + fileName);
			Path filePath = uploadPath.resolve(fileName);
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
			JdbcHelper mod = new JdbcHelper();
			// :FILE_NAME, :FILE_PATH, :FILE_SIZE, :NAME_MODIFY, :AUTHOR, :IP_UPLOAD,
			// CURRENT_TIMESTAMP, :IS_SHOW
			map.put("FILE_NAME", ObjectUtils.toString(fileName));
			map.put("FILE_SIZE", "0");
			map.put("FILE_PATH", ObjectUtils.toString(filePath));
			map.put("NAME_MODIFY", "null");
			map.put("AUTHOR", "null");
			map.put("IP_UPLOAD", "null");
			map.put("IS_SHOW", "1");
			mod.setDataFromKey(INSERT_UPLOADFILE, map);
		} catch (IOException ioe) {
			throw new IOException("Could not save file: " + fileName, ioe);
		}

		return fileName;
	}

	private Path foundFile;

	public Resource getFileAsResource(String fileCode) throws IOException {
		Path dirPath = Paths.get(getPathUpload());

		Files.list(dirPath).forEach(file -> {
			if (file.getFileName().toString().startsWith(fileCode)) {
				foundFile = file;
				return;
			}
		});

		if (foundFile != null) {
			return new UrlResource(foundFile.toUri());
		}

		return null;
	}

	public Resource getFileAsResource(String path, String fileName) throws IOException {
		Path dirPath = Paths.get(path);
		if ("".equals(path)) {
			dirPath = Paths.get(getPathUpload());
		}

		Files.list(dirPath).forEach(file -> {
			if (file.getFileName().toString().startsWith(fileName)) {
				foundFile = file;
				return;
			}
		});

		if (foundFile != null) {
			return new UrlResource(foundFile.toUri());
		}

		return null;
	}

	public List getListOfFiles(String Path) {
		List<Map> li = new ArrayList();

		File directoryPath = new File(Path);
		String contents[] = directoryPath.list();
		for (int i = 0; i < contents.length; i++) {
			Map item = new HashMap();
			item.put("NAME", contents[i]);
			File file = new File(Path + "/" + contents[i]);

			System.out.println(file.getParent());
			item.put("ROOT", file.getAbsoluteFile());
			item.put("PREROOT", file.getParentFile().getParent());
			if (ObjectUtils.toString(file.getName()).indexOf(".") == -1) {
				item.put("TYPE", 0);
			} else {
				item.put("TYPE", 1);
			}

			li.add(item);
		}
		return li;

	}

	public static boolean is_getFile(String fileUrl) {
		long fileSize = getFileSizeFromURL(fileUrl);
		if (fileSize > -1) {
			if (fileSize < 2_000_000_000) {
				// Thá»±c hiá»‡n tÃ¡c vá»¥ náº¿u dung lÆ°á»£ng file < 2GB
				return true;
			} else {
				System.out.println("File size exceeds 2GB.");
				return false;
			}
		} else {
			return false;
		}
	}

	public static long getFileSizeFromURL(String fileUrl) {
		try {
			URL url = new URL(fileUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("HEAD");
			conn.getInputStream();
			return conn.getContentLengthLong();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static void downloadFile(String fileUrl, String destinationPath) {
		try {
			URL url = new URL(fileUrl);
			try (InputStream in = url.openStream()) {
				Path outputPath = Path.of(destinationPath);
				Files.copy(in, outputPath, StandardCopyOption.REPLACE_EXISTING);
				System.out.println("File downloaded successfully.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getFileType(String url) {
		try {
			URLConnection connection = new URL(url).openConnection();
			String contentType = connection.getContentType();

			if (contentType != null) {
				return contentType.split("/")[1];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<Map> findSourceFile(String rootPath, String fileEndsWith) {
		File rootDir = new File(rootPath);
		List<Map> listFileBatch = new ArrayList();
		if (rootDir.exists() && rootDir.isDirectory()) {
			List<String> fileNames = new ArrayList<>();
			List<String> packageNames = new ArrayList<>();
			List<String> fullPath = new ArrayList<>();
			findPropertiesFilesRecursive(rootDir, fileNames, packageNames, fullPath, fileEndsWith);
			for (int i = 0; i < fileNames.size(); i++) {
				String fileName = fileNames.get(i);
				String packageName = packageNames.get(i);
				Map map = new HashMap();
				map.put("FILE_NAME", fileName);
				map.put("PACKAGE_NAME", packageName);
				map.put("FULL_PATH", fullPath);
				listFileBatch.add(map);
			}
		}
		return listFileBatch;
	}

	public static List<Map> checkSourceCodeNames(String rootPath) {
		File rootDir = new File(rootPath);
		List<Map> listFileBatch = new ArrayList();
		if (rootDir.exists() && rootDir.isDirectory()) {
			List<String> fileNames = new ArrayList<>();
			List<String> packageNames = new ArrayList<>();
			checkNamesRecursive(rootDir, fileNames, packageNames);
			for (int i = 0; i < fileNames.size(); i++) {
				String fileName = fileNames.get(i);
				String packageName = packageNames.get(i);
				Map map = new HashMap();
				map.put("FILE_NAME", fileName);
				map.put("PACKAGE_NAME", packageName);
				listFileBatch.add(map);
			}
		}
		return listFileBatch;
	}

	public static List<Map> getListBatchFromSever(String path) {
		List<Map> listreturn = new ArrayList();
		List<Map> dataList = new ArrayList();
		dataList = checkSourceCodeNames(path);
		for (Map<String, String> data : dataList) {
			String packageName = data.get("PACKAGE_NAME")
					.substring(data.get("PACKAGE_NAME").indexOf(".classes.") + ".classes.".length())
					.replaceAll("\\$\\d", "");
			String fileName = data.get("FILE_NAME").replace(".class", ".java").replaceAll("\\$\\d", "");
			String className = extractClassName(packageName, fileName);
			Map map = new HashMap();
			map.put("FILE_NAME", fileName);
			map.put("PACKAGE_NAME", packageName);
			listreturn.add(map);

		}
		return listreturn;
	}

	public static String extractClassName(String packageName, String fileName) {
		packageName = packageName.replace("..", ".");
		packageName = packageName.replace(".", "/");
		packageName = packageName.replaceFirst("/", "");
		String className = fileName.substring(0, fileName.lastIndexOf("."));
		String absoluteClassName = packageName + "/" + className;
		return absoluteClassName;
	}

	private static void checkNamesRecursive(File directory, List<String> fileNames, List<String> packageNames) {
		File[] files = directory.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isFile()) {
					String fileName = file.getName();
					String packageName = getPackageName(file);

					// Kiá»ƒm tra náº¿u tÃªn package chá»©a tá»« "batch"
					if (packageName.contains("batch")) {
						fileNames.add(fileName);
						packageNames.add(packageName);
					}
				} else if (file.isDirectory()) {
					// Tiáº¿p tá»¥c kiá»ƒm tra trong thÆ° má»¥c con
					checkNamesRecursive(file, fileNames, packageNames);
				}
			}
		}
	}

	private static void findPropertiesFilesRecursive(File directory, List<String> fileNames, List<String> packageNames,
			List<String> fullPath, String fileEndsWith) {
		File[] files = directory.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isFile() && file.getName().endsWith("." + fileEndsWith)) {
					String fileName = file.getName();
					String packageName = getPackageName(file);
					String fileFullPath = file.getPath();
					fileNames.add(fileName);
					packageNames.add(packageName);
					fullPath.add(fileFullPath);
				} else if (file.isDirectory()) {
					findPropertiesFilesRecursive(file, fileNames, packageNames, fullPath, fileEndsWith);
				}
			}
		}
	}

	private static void findFileInSever(File directory, List<File> files, String fileName) {
		if (files != null) {
			for (File file : files) {
				System.out.println(fileName);
				if (file.isFile() && file.getName().endsWith(fileName)) {
					System.out.println("á đây rồi!!!!");
					// log.debug("á đây rồi!!!!");
					files.add(file);
				} else if (file.isDirectory()) {
					findFileInSever(file, files, fileName);
				}
			}
		}
	}

	public static List<File> findFileInSever(String rootPath, String fileName) {
		File rootDir = new File(rootPath);
		List<File> files = new ArrayList<>();
		if (rootDir.exists() && rootDir.isDirectory()) {
			findFileInSever(rootDir, files, fileName);
		}
		return files;
	}

	private static String getPackageName(File file) {
		StringBuilder packageName = new StringBuilder();
		File parent = file.getParentFile();

		while (parent != null) {
			if (packageName.length() > 0) {
				packageName.insert(0, ".");
			}
			packageName.insert(0, parent.getName());
			parent = parent.getParentFile();
		}

		return packageName.toString();
	}

//	public static void main(String[] args) {
//		String rootPath = ".";
//		checkSourceCodeNames(rootPath);
//	}
	public static void main(String[] args) {
		String folderPath = "C:\\Program Files (x86)\\BraveSoftware\\Update"; // Thay thế bằng đường dẫn thư mục của bạn
		DataSet ds = DataSet.getDataSet();
		ds.clear();
		ds.setField("ID", "AA");
		System.out.println(ds.searchAndRetrieve("SELECT * FROM dtcm0004_field_option_list Where SYSID=:ID"));
		/*
		 * File folder = new File(folderPath); File[] files = folder.listFiles();
		 * 
		 * if (files != null) { for (File file : files) { String fileName =
		 * file.getName(); String newFileName = fileName + ".mp4"; File newFile = new
		 * File(folderPath + "/" + newFileName);
		 * 
		 * if (file.renameTo(newFile)) { System.out.println("Đổi tên thành công: " +
		 * fileName + " -> " + newFileName); } else {
		 * System.out.println("Đổi tên không thành công: " + fileName); } } }
		 */
	}
}
// }
//	public static void main(String args[]) throws IOException {
//		System.out.println(getPathTomcat());
//		String imageUrl = "https://api.telegram.org/file/bot5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0/videos/file_43";
//		String videoUrl = "https://www.w3schools.com/html/mov_bbb.mp4";
//		if (videoUrl.startsWith("octet-stream")) {
//			System.out.println("is_file");
//		}
//
//		String imageType = getFileType(imageUrl);
//		String videoType = getFileType(videoUrl);
//
//		System.out.println(is_getFile(imageUrl));
//		System.out.println("Video type: " + videoType);
//	}

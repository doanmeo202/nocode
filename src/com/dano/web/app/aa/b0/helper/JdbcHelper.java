package com.dano.web.app.aa.b0.helper;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;
import com.dano.web.app.aa.b0.modun.AAB0_0100_mod;
import com.dano.web.app.database.vo.SYSTEM_BATCH_INFO1;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class JdbcHelper {
	static Logger logger = Logger.getLogger(JdbcHelper.class);
	static String Duplicate = "Duplicate";
	private static String hOST;
	private static String dB;
	private static String uSER;
	private static String pASSWORD;
	private static String uRL;
	private Connection connection;
	private String tableCodeMap = new Enviroment().CODEMAP_TABLE;
	private static Map mapSetField = new HashMap();
	private static Map mapDataUpdate = new HashMap();
	private static Map mapKeyTable = new HashMap();
	private static Map Enviroment = new HashMap();
	private Connection con;

	public Connection getConnection() throws ClassNotFoundException {
		reSetUrl();
		Sql2o JdbcHeper = new Sql2o(this.getuRL(), this.getuSER(), this.getpASSWORD());
		Connection con = JdbcHeper.beginTransaction();
		return con;
	}

	public String getuRL() {
		return uRL;
	}

	public void reSetUrl() {
		this.uRL = "jdbc:mysql://" + this.gethOST() + ":3306/" + this.getdB()
				+ "?useUnicode=yes&characterEncoding=UTF-8";
	}

	private void setuRL(String uRL) {
		this.uRL = uRL;
	}

	public String gethOST() {
		return hOST;
	}

	public void sethOST(String hOST) {
		this.hOST = hOST;
	}

	public static String getdB() {
		return dB;
	}

	public void setdB(String dB) {
		this.dB = dB;
	}

	public static String getuSER() {
		return uSER;
	}

	public void setuSER(String uSER) {
		this.uSER = uSER;
	}

	public static String getpASSWORD() {
		return pASSWORD;
	}

	public void setpASSWORD(String pASSWORD) {
		this.pASSWORD = pASSWORD;
	}

	static {
		try {
			Enviroment = new Enviroment().DATABASE_CONFIG;
			Class.forName(Enviroment.get("drive").toString());
		} catch (ClassNotFoundException e) {
			throw new Sql2oException("không tìm thấy driver!");
		}
	}

	public static void clear() {
		mapSetField.clear();
	}

	public static void setField(String key, String value) {
		mapSetField.put(key, value);
	}

	public static void setFieldMap(Map map) {
		mapSetField = map;
	}

	public List<Map<String, Object>> searchAndRetrieve(String keysql) {
		// System.out.println(mapSetField);
		return getDataFromKey(keysql, mapSetField);
	}

	public int update(String keysql) {
		return setDataFromKey(keysql, mapSetField);
	}

	public List<Map<String, Object>> getDataFromKey(String keysql, Map map) {
		List<Map<String, Object>> map1 = new ArrayList();
		List<Map<String, Object>> mapoutput = new ArrayList();
		Connection con = null;
		Connection con1 = null;
		try {
			con = getConnection();
			con1 = getConnection();

			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() < 1) {
				con.rollback();
				con1.rollback();

				con.close();
				con1.close();
				throw new Sql2oException("NOT FOUND CODE_NAME:" + keysql);
			}
			if (map1 != null || map1.size() > 0) {
				String SQLSTRING = ObjectUtils.toString(map1.get(0).get("sqlstring"));
				logger.fatal("SQL STRING = [" + SQLSTRING + "]");
				logger.fatal("CODE_NAME: " + keysql);
				if (!SQLSTRING.equals("")) {
					if (map != null || map.size() > 0) {
						Query query = con1.createQuery(SQLSTRING);

						ArrayList myKeyList = new ArrayList(map.keySet());

						for (int i = 0; i < myKeyList.size(); i++) {

							String key = (String) myKeyList.get(i);
							String value = (String) map.get(myKeyList.get(i));

							try {
								query.addParameter(key, value);
								logger.fatal("SET PARAM [" + key + "=" + value + "]");
							} catch (Exception e) {
								if (e.getCause() == null || e.getCause().equals("null")) {

								} else {
									logger.fatal("SET PARAM ERR [" + key + "=" + value + "]");
									logger.fatal("=>>>>>>" + e.getCause());
									con.rollback();
									con1.rollback();
									con.close();
									con1.close();
									throw new Sql2oException(e);
								}

							}
						}

						mapoutput = query.executeAndFetchTable().asList();
					} else {
						mapoutput = con1.createQuery(SQLSTRING).executeAndFetchTable().asList();
					}
				}
			}

		} catch (ClassNotFoundException e) {
			logger.debug("rollback Connection!");
			con.rollback();
			con1.rollback();
			resetNullConnection();
			e.printStackTrace();
		} finally {
			if (con != null) {

				con.close();
			}
			if (con1 != null) {

				con1.close();
			}

		}
		return mapoutput;
	}

	public void resetNullConnection() {
		this.setdB(null);
		this.sethOST(null);
		this.setuSER(null);
		this.setpASSWORD(null);
		reSetUrl();
	}

	public int setDataFromKey(String keysql, Map map) {
		List<Map<String, Object>> map1 = new ArrayList();
		List<Map<String, Object>> mapoutput = new ArrayList();
		int insert = 0;
		Connection con = null;
		try {
			con = getConnection();

			Connection con1 = getConnection();
			logger.fatal("SET PARAM [CODE_NAME=" + keysql + "]");
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() < 1) {
				con.rollback();
				con1.rollback();

				con.close();
				con1.close();
				throw new Sql2oException("NOT FOUND CODE_NAME:" + keysql);
			}
			if (map1 != null || map1.size() > 0) {
				String SQLSTRING = ObjectUtils.toString(map1.get(0).get("sqlstring"));
				logger.fatal("SQL STRING = [" + SQLSTRING + "]");
				logger.fatal("CODE_NAME: " + keysql);
				if (!SQLSTRING.equals("")) {
					if (map != null || map.size() > 0) {
						Query query = con1.createQuery(SQLSTRING);

						ArrayList myKeyList = new ArrayList(map.keySet());

						for (int i = 0; i < myKeyList.size(); i++) {

							String key = (String) myKeyList.get(i);
							String value = (String) map.get(myKeyList.get(i));

							try {
								query.addParameter(key, value);
								logger.fatal("SET PARAM [" + key + "=" + value + "]");
							} catch (Exception e) {
								if (e.getCause() == null || e.getCause().equals("null")) {

								} else {
									logger.fatal("SET PARAM ERR [" + key + "=" + value + "]");
									logger.fatal("=>>>>>>" + e.getCause());
									con.rollback();
									con1.rollback();
									con.close();
									con1.close();
									return -1;
								}

							}
						}
						try {
							insert = query.executeUpdate().commit().hashCode();
						} catch (Exception e) {
							insert = 0;
							e.printStackTrace();
							logger.fatal("ERR: " + e.getCause());
							logger.fatal(e.getCause().toString().indexOf("Duplicate"));
							con1.rollback();
							con1.close();
							return -2;
						}

					} else {
						try {
							insert = con1.createQuery(SQLSTRING).executeUpdate().commit().hashCode();
						} catch (Exception e) {
							insert = 0;
							con1.rollback();
							con1.close();
							return -1;
						}
					}
				}
			}

			con.close();
			con1.close();

		} catch (ClassNotFoundException e) {
			insert = 0;
			e.printStackTrace();
			con.close();
			resetNullConnection();
		}
		return insert;
	}

	public List<Map<String, Object>> QueryCODE_MAPLikeAll(String keysql) {
		List<Map<String, Object>> ListArr = new ArrayList();
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM " + tableCodeMap + " A where A.CODE_NAME LIKE '%" + keysql + "%'")
					.executeAndFetchTable().asList();

			con.close();
		} catch (ClassNotFoundException e) {
			resetNullConnection();
			e.printStackTrace();
		}
		return ListArr;

	}

	public List<Map<String, Object>> QueryCODE_MAP(String keysql) {
		List<Map<String, Object>> ListArr = new ArrayList();
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM " + tableCodeMap + " A where A.CODE_NAME LIKE :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();

			con.close();
		} catch (ClassNotFoundException e) {
			resetNullConnection();
			e.printStackTrace();
		}
		return ListArr;

	}

	public int InsertCODE_MAP(String keysql, String Content) {

		List<Map<String, Object>> ListArr = new ArrayList();
		int check = 0;
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM " + tableCodeMap + " A where A.CODE_NAME LIKE :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();

			con.close();
			if (ListArr.size() >= 1) {
				throw new Sql2oException("data not exit with key:" + keysql);
			} else {
				Connection con1 = getConnection();
				con1.createQuery(
						"INSERT INTO " + tableCodeMap + " (CODE_NAME, SQLSTRING, FLAG) VALUES(:CODE_NAME,:SQLSTRING,0)")
						.addParameter("CODE_NAME", keysql).addParameter("SQLSTRING", Content).executeUpdate();
				check = con1.getResult();
				con1.commit();

				con1.close();
				AAB0_0100_mod mod = new AAB0_0100_mod();
				Map mapBackup = new HashMap();
				mapBackup.put("CODE_NAME", keysql);
				mapBackup.put("SQLSTRING", Content);
				mod.backUpCodeMap(mapBackup);
			}

		} catch (ClassNotFoundException e) {
			resetNullConnection();
			e.printStackTrace();
			check = 0;
		}

		return check;

	}

	public int updateCODE_MAP(String keysql, String Content) {
		List<Map<String, Object>> ListArr = new ArrayList();
		List<Map<String, Object>> ListArr2 = new ArrayList();
		int check = 0;
		try {
			Connection con = getConnection();
			con.createQuery("UPDATE " + tableCodeMap + " SET  SQLSTRING=:SQLSTRING WHERE CODE_NAME=:CODE_NAME")
					.addParameter("CODE_NAME", keysql).addParameter("SQLSTRING", Content).executeUpdate();
			check = con.getResult();
			con.commit();

			con.close();
			AAB0_0100_mod mod = new AAB0_0100_mod();
			Map mapBackup = new HashMap();
			mapBackup.put("CODE_NAME", keysql);
			mapBackup.put("SQLSTRING", Content);
			mod.backUpCodeMap(mapBackup);

		} catch (ClassNotFoundException e) {
			resetNullConnection();
			e.printStackTrace();
			check = 0;
		}

		return check;

	}

	public int deleteCODE_MAP(String keysql) {

		int check = 0;
		try {
			Connection con = getConnection();
			con.createQuery("DELETE FROM  " + tableCodeMap + " WHERE CODE_NAME=:CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeUpdate();
			logger.fatal("[DELETE FROM  " + tableCodeMap + " WHERE CODE_NAME=" + keysql + "]");
			logger.fatal("[CODE_NAME=" + keysql + "]");
			check = con.getResult();
			logger.fatal("[UPDATE RESURL=" + check + "]");
			con.commit();

			con.close();

		} catch (ClassNotFoundException e) {
			resetNullConnection();
			e.printStackTrace();
			check = -1;
		}

		return check;

	}

	public void insertFromMap(Map map, String table) throws Exception {
		ArrayList myKeyList = new ArrayList(map.keySet());
		String pram = "";
		String pramValue = "";
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			if (i == (myKeyList.size() - 1)) {

				pram += key;
				pramValue += "'" + value + "'";
			} else {
				pram += key + ",";
				pramValue += "'" + value + "', ";
			}
		}
		String SQL = "INSERT INTO " + table + " (" + pram + ") VALUES(" + pramValue + ")";
		System.out.println(SQL);

		Connection con1 = null;
		try {
			con1 = getConnection();
		} catch (ClassNotFoundException e) {

			con1.close();
			e.printStackTrace();
		}
		try {
			con1.createQuery(SQL).executeUpdate();
			con1.commit();
		} catch (Exception e) {
			con1.rollback();
			throw new Exception(e.getMessage());
		} finally {

			con1.close();
		}

	}

	public void clearDataUpdate() {
		mapDataUpdate.clear();
	}

	public void clearKeyTable() {
		mapKeyTable.clear();
	}

	public void setDataUpdate(String key, String value) {
		mapDataUpdate.put(key, value);
	}

	public void setDataUpdateMap(Map map) {
		mapDataUpdate = map;
	}

	public void setKeyTable(String key, String value) {
		mapKeyTable.put(key, value);
	}

	public void setKeyTableMap(Map map) {
		mapKeyTable = map;
	}

	public List<Map<String, Object>> query(String sql) {
		List<Map<String, Object>> ListArr = new ArrayList();
		try {
			Connection con = getConnection();
			ListArr = con.createQuery(sql).executeAndFetchTable().asList();

			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ListArr;
	}

	public void excute(String sql) throws Exception {
		excute_withMap(sql, mapSetField);
	}

	public void excute_withMap(String sql, Map map) throws Exception {
		Connection con1 = null;
		try {
			con1 = getConnection();
		} catch (ClassNotFoundException e) {

			con1.close();
			e.printStackTrace();
		}
		try {

			Query query = con1.createQuery(sql);
			ArrayList myKeyList = new ArrayList(map.keySet());

			for (int i = 0; i < myKeyList.size(); i++) {

				String key = (String) myKeyList.get(i);
				Object value = map.get(myKeyList.get(i));

				try {
					query.addParameter(key, value);
					logger.fatal("SET PARAM [" + key + "=" + value + "]");
				} catch (Exception e) {
					if (e.getCause() == null || e.getCause().equals("null")) {

					} else {
						logger.fatal("SET PARAM ERR [" + key + "=" + value + "]");
						logger.fatal("=>>>>>>" + e.getCause());
						con1.rollback();
						con1.close();
						throw new Sql2oException(e);
					}

				}
			}
			query.executeUpdate().commit();
		} catch (Exception e) {
			logger.error(e);
			con1.rollback();
			throw new Exception(e.getMessage());
		} finally {
			con1.close();
		}
	}

	public List<String> getPRIMARY(String Table) {
		List<Map<String, Object>> ListArr = new ArrayList();
		String sql = "SHOW INDEX FROM " + Table + " WHERE Key_name = 'PRIMARY'";
		ListArr = query(sql);
		List<String> PRIMARY = new ArrayList<String>();
		for (Map<String, Object> item : ListArr) {
			String column_name = ObjectUtils.toString(item.get("column_name"));
			PRIMARY.add(column_name);
		}
		return PRIMARY;
	}

	public String getSqlUpdate(String Table) {
		Map mapDataUpdate1 = mapDataUpdate;
		System.out.println(mapDataUpdate1);
		List<String> keyTable = getPRIMARY(Table);
		if (mapDataUpdate1.size() < 1) {
			return null;
		}
		StringBuilder sqlUpdate = new StringBuilder("UPDATE " + Table + " SET ");
		ArrayList myKeyList = new ArrayList(mapDataUpdate1.keySet());
		for (Object entry : myKeyList) {
			String keyEntry = ObjectUtils.toString(entry);
			Object value = mapDataUpdate1.get(keyEntry);

			boolean check = false;
			for (String key : keyTable) {
				if (keyEntry.equals(key)) {
					check = true;
				}
			}
			if (!check) {
				if (value != null && !ObjectUtils.toString(value).equals("")) {
					sqlUpdate.append(keyEntry).append(" = '").append(value).append("', ");
				}
			}
		}
		sqlUpdate.setLength(sqlUpdate.length() - 2);
		sqlUpdate.append(" WHERE ");
		int count = 0;
		int size = keyTable.size();
		for (String entry : keyTable) {
			sqlUpdate.append(entry).append(" = '").append(mapDataUpdate1.get(entry)).append("' ");
			if (++count != size) {
				sqlUpdate.append("AND ");
			}
		}

		return sqlUpdate.toString();
	}

	public String getSqlInsert(String Table, Map<String, String> mapDataUpdate) {
		StringBuilder sqlInsert = new StringBuilder("INSERT INTO " + Table + " (");
		StringBuilder values = new StringBuilder("VALUES (");
		ArrayList<String> myKeyList = new ArrayList<>(mapDataUpdate.keySet());
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = myKeyList.get(i);
			if (mapDataUpdate.get(key) != null) {
				sqlInsert.append(key);
				values.append(":").append(key);
				if (i != myKeyList.size() - 1) {
					sqlInsert.append(", ");
					values.append(", ");
				}
			}
		}
		sqlInsert.setLength(sqlInsert.length() - 2);
		values.setLength(values.length() - 2);
		sqlInsert.append(") ").append(values).append(")");
		return sqlInsert.toString();
	}

	public JdbcHelper() {
		Map map = Enviroment;
		if (this.getuRL() == null) {
			if (this.getdB() == null) {
				this.setdB(map.get("database").toString());

			}
			if (this.gethOST() == null) {
				// this.sethOST("webdano.ddns.net");
				this.sethOST(map.get("host").toString());
			}
			if (this.getuSER() == null) {
				// this.setuSER("cxluser");
				this.setuSER(map.get("username").toString());
			}
			if (this.getpASSWORD() == null) {
				// this.setpASSWORD("Thanh0974135042!");
				this.setpASSWORD(map.get("password").toString());
			}
			this.reSetUrl();
		}

	}

	public String getPackage(String name, String simpleName) {
		int indexOf = name.indexOf("." + simpleName);
		return name.substring(0, indexOf);
	}

	public static void main(String[] args) throws UnknownHostException {

		BasicConfigurator.configure();
		JdbcHelper hp = new JdbcHelper();
		try {
			// hp.setField("TABLE","SYSTEM_BATCH_INFO");
			SYSTEM_BATCH_INFO1 fol = new SYSTEM_BATCH_INFO1();
			fol = fol.getSysBatchInfo("AAB0_0100");
			fol.setid("Test");
			Map map = new VoTooL().objToMap(fol);
			map.remove("returnCode");
			// hp.setDataUpdateMap(map);

			hp.setFieldMap(map);
			hp.excute(hp.getSqlInsert("SYSTEM_BATCH_INFO", map));
			// hp.excute();
			// hp.Backupdbtosql();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		hp.sethOST("webdano.com");
//		hp.reSetUrl();
//		System.out.println(hp.getuRL());
//		Map map = new HashMap();
//		System.out.println(hp.getDataFromKey("com.google.email.setting.getlogsendmail", map));
//		logger.fatal("hi");

	}

}

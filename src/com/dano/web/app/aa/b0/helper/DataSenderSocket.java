package com.dano.web.app.aa.b0.helper;
import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DataSenderSocket {
    private WebSocketSession socket;
    private ScheduledExecutorService executorService;

    public DataSenderSocket(WebSocketSession session) {
        this.socket = session;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    public void start(List<Map<String, Object>> list,WebSocketSession session) {
        executorService.scheduleAtFixedRate(() -> {
            try {
            	ObjectMapper obj = new ObjectMapper();
    			String json = obj.writeValueAsString(list);
    			session.sendMessage(new TextMessage(json));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, 0, 5, TimeUnit.SECONDS);
    }
}

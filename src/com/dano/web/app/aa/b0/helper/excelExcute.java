package com.dano.web.app.aa.b0.helper;

import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class excelExcute {
	private File fileInput;
	private Workbook workbook;
	private FileInputStream fis;
	private Sheet sheet;
	private List<Map<Integer, Object>> DATA;

	private File getFileInput() {
		return fileInput;
	}

	public synchronized void setFileInput(File file_input, int sheet_) {
		fileInput = file_input;
		try {
			fis = new FileInputStream(getFileInput());
			workbook = WorkbookFactory.create(fis);
			workbook.getSpreadsheetVersion();
			sheet = getSheet(sheet_);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized void setFileInput(String file_input, int sheet_) {
		fileInput = new File(file_input);
		try {
			fis = new FileInputStream(getFileInput());
			workbook = WorkbookFactory.create(fis);
			workbook.getSpreadsheetVersion();
			sheet = getSheet(sheet_);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Sheet getSheet(int sheet_) {
		sheet = workbook.getSheetAt(sheet_);
		return sheet;
	}

	public Row getRow(int row) {
		return sheet.getRow(row);
	}

	public synchronized List<Map<Integer, Object>> SnapShortData() {
		DATA = new ArrayList<>();
		Row headerRow = getRow(0);
		for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			Row row = getRow(rowIndex);

			Map<Integer, Object> rowData = new TreeMap<>();
			for (int cellIndex = 0; cellIndex < headerRow.getLastCellNum(); cellIndex++) {
				Cell cell = row.getCell(cellIndex);
				if (cell != null) {

					if (cell.getCellType() == CellType.NUMERIC) {
						rowData.put(cellIndex, cell.getNumericCellValue());
					} else if (cell.getCellType() == CellType.STRING) {
						rowData.put(cellIndex, cell.getStringCellValue());
					} else if (cell.getCellType() == CellType.BOOLEAN) {
						rowData.put(cellIndex, cell.getBooleanCellValue());
					} else if (cell.getCellType() == CellType.BLANK) {
						rowData.put(cellIndex, ""); // Xử lý ô trống
					} else {
						rowData.put(cellIndex, null); // Xử lý các kiểu dữ liệu khác
					}
				} else {
					rowData.put(cellIndex, null); // Xử lý ô null
				}
			}
			DATA.add(rowData);
		}
		return DATA;

	}

	public static void main(String[] args) {

	}

}

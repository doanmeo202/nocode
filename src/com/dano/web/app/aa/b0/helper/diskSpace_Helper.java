package com.dano.web.app.aa.b0.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class diskSpace_Helper {
	public static void main(String[] args) {
		File[] drives = File.listRoots();
		List<Long> driveSizes = new ArrayList<>();
		if (drives != null && drives.length > 0) {
			for (File drive : drives) {
				long totalSpace = drive.getTotalSpace();
				long freeSpace = drive.getFreeSpace();
				long usedSpace = totalSpace - freeSpace;
				double usedPercentage = (double) usedSpace / totalSpace * 100;
				double freePercentage = 100 - usedPercentage;
				driveSizes.add(usedSpace);
				System.out.println("Drive: " + drive + " - Total space: " + totalSpace + " - Used space: " + usedSpace
						+ " - Used percentage: " + String.format("%.1f%%", usedPercentage) + "% - Free percentage: "
						+ String.format("%.1f%%", freePercentage) + "%");

			}
		}
//		Collections.sort(driveSizes, Collections.reverseOrder());
//		System.out.println("Drives with largest used space: ");
//		for (Long size : driveSizes) {
//			System.out.println(size);
//		}
	}

	public static File[] getLogList(String path) {
		File directory = new File(path);
		File[] files = directory.listFiles((dir, name) -> name.endsWith(".txt"));
		Arrays.sort(files, Comparator.comparing(File::getName).reversed());
		for (int i = 0; i < Math.min(files.length, 10); i++) {
			System.out.println(files[i].getName());
		}
		return files;
	}
}
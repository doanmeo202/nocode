package com.dano.web.app.aa.b0.helper;

public class VideoEntry {
	private String videoId;
	private byte[] data;
	private long priority;

	public VideoEntry(String videoId, byte[] data, long priority) {
		this.videoId = videoId;
		this.data = data;
		this.priority = priority;
	}

	public String getVideoId() {
		return videoId;
	}

	public byte[] getData() {
		return data;
	}

	public long getPriority() {
		return priority;
	}
}

package com.dano.web.app.aa.b0.helper;

import java.util.LinkedHashMap;
import java.util.Map;

public class VideoBufferManager {
    private static final long MAX_BUFFER_SIZE = 5 * 1024 * 1024 * 1024; // 5GB
    private static final LinkedHashMap<String, byte[]> videoBuffer = new LinkedHashMap<String, byte[]>(16, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, byte[]> eldest) {
            long currentBufferSize = videoBuffer.values().stream()
            		.mapToLong(bytes -> bytes.length)
                    .sum();

            if (currentBufferSize > MAX_BUFFER_SIZE) {
                videoBuffer.remove(eldest.getKey());
                return true;
            }
            return false;
        }
    };

    public static void addVideoToBuffer(String fileId, byte[] videoData) {
        videoBuffer.put(fileId, videoData);
    }

    public static byte[] getVideoFromBuffer(String fileId) {
        return videoBuffer.get(fileId);
    }
}

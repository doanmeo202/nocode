package com.dano.web.app.aa.b0.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class propertiesHelper {
	private static Map mapSetField = new HashMap();
	private static final String PROPERTIES_FILE = "database.properties";

	public static void clearProperties() {
		mapSetField.clear();
	}

	public static void addProperties(String key, String value) {
		mapSetField.put(key, value);
	}

	public static Properties getPropertiesFile(String filePath) {
		Properties props = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(filePath);
			props.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("PathFileConfig :" + "file not found!");
			return null;
		}
		return props;
	}

	public static void savePropertiesFile(String fileName) {
		savePropertiesFile(mapSetField, "", fileName);
	}

	public static void savePropertiesFile(String desc, String fileName) {
		savePropertiesFile(mapSetField, desc, fileName);
	}

	public static void savePropertiesFile(Map propertiesValue, String desc, String fileName) {
		ArrayList myKeyList = new ArrayList(propertiesValue.keySet());
		Properties props = new Properties();
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) propertiesValue.get(myKeyList.get(i));
			props.setProperty(key, value);
		}
		try {
			FileOutputStream fos = new FileOutputStream(fileName + ".properties");
			File file = new File(fileName + ".properties");
			props.setProperty("filePath", file.getAbsolutePath());
			props.store(fos, desc);
			System.out.println("saved!!! " + file.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Properties JdbcHelperConfig() {
		return getPropertiesFile("jbdcConfig.properties");
	}

	public static Properties loadProperties() throws IOException {
		Properties properties = new Properties();
		try (InputStream inputStream = propertiesHelper.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
			properties.load(inputStream);
		}
		return properties;
	}

	public static void main(String[] args) {
		propertiesHelper pr = new propertiesHelper();
		Map map = new HashMap();
		map.put("username_db", "root");
		map.put("password_db", ""); // for localhost
		map.put("database_db", "xxxxxxx");
		map.put("host_db", "localhost");
		map.put("tablecodemap", "xxxxxx");
		pr.addProperties("log4j.rootCategory", "DEBUG, all");
		pr.addProperties("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		pr.addProperties("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		pr.addProperties("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		pr.savePropertiesFile("abcd", "abcd");
		// pr.savePropertiesFile(map, "jbdcConfig", "jbdcConfig");

	}
}

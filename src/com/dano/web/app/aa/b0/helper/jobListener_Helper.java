package com.dano.web.app.aa.b0.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.springframework.web.socket.WebSocketSession;
import com.dano.web.app.database.vo.SYSTEM_BATCH_INFO1;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;
import com.telegram.core.ChatBotConfig;

public class jobListener_Helper implements JobListener {
	private Logger log = Logger.getLogger(jobListener_Helper.class.getName());
	private final boolean isDebug = log.isDebugEnabled();
	private static ChatBotConfig chatbot = new ChatBotConfig();
	private static List<String> statusIsRunning = new ArrayList();
	private static List<String> statusNoRunning = new ArrayList();
	static {
		statusIsRunning.add("Start");
		statusIsRunning.add("Order");
		statusIsRunning.add("Done");
		statusNoRunning.add("Kill");
		statusNoRunning.add("Hold");
		statusNoRunning.add("Eror");
		statusNoRunning.add("Free");
		chatbot.setBotToken(Enviroment.SYS_BOT_TOKEN);
		chatbot.setBotUsername(Enviroment.SYS_BOT_USERNA);
		chatbot.setChatId(Enviroment.SYS_BOT_CHATID);
	}

	public void erorAlert(Exception e, SYSTEM_BATCH_INFO1 info) {
		chatbot.sendMessenger("【Eror!】 Job: " + info.getjob_name() + ", Program: " + info.getjob_class()
				+ " crashed(err= " + e.getMessage() + ")");
	}

	public String getName() {
		return "myJobListener";
	}

	batchHelper batch = new batchHelper();
	SYSTEM_BATCH_INFO1 info = new SYSTEM_BATCH_INFO1();
	ObjectMapper mapper = new ObjectMapper();

	public SYSTEM_BATCH_INFO1 setSysBatch(String Status, JobExecutionContext context) {
		JobKey key = context.getJobDetail().getKey();
		info = info.getSysBatchInfo(key.getName());
		info.setid(key.getName());
		info.setjob_info(Status);
		info.setnext_fire_time(ObjectUtils.toString(context.getTrigger().getNextFireTime()));
		info.settrigger_name(ObjectUtils.toString(context.getTrigger().getKey()));
		info.setprev_fire_time(ObjectUtils.toString(context.getTrigger().getPreviousFireTime()));
		return info;
	}

	public void jobToBeExecuted(JobExecutionContext context) {
		System.out.println("Job to be executed: " + context.getJobDetail().getKey());
		SYSTEM_BATCH_INFO1 job = new SYSTEM_BATCH_INFO1();
		JobKey key = context.getJobDetail().getKey();
		Scheduler scheduler = SchedulerProvider.getScheduler();
		TriggerKey triggerKey = TriggerKey.triggerKey(key.getName(), key.getGroup());
		String StatusJobDB = job.getSysBatchInfo(key.getName()).getjob_info();
		if (statusIsRunning.contains(StatusJobDB)) {
			info = setSysBatch("Start", context);
			if (info != null) {
				info.UpdateDB();
			}
		} else if (statusNoRunning.contains(StatusJobDB)) {
			if (StatusJobDB.startsWith("K")) {
				info = setSysBatch("Kill", context);
				if (info != null) {
					info.UpdateDB();
				}
				try {
					scheduler.unscheduleJob(triggerKey);
				} catch (SchedulerException e) {
					erorAlert(e, info);
				}

			} else if (StatusJobDB.startsWith("H")) {
				info = setSysBatch("Hold", context);
				if (info != null) {
					info.UpdateDB();
				}
				try {
					scheduler.pauseJob(key);
				} catch (SchedulerException e) {
					// TODO Auto-generated catch block
					erorAlert(e, info);
				}
			} else if (StatusJobDB.startsWith("F")) {
				info = setSysBatch("Free", context);
				if (info != null) {
					info.UpdateDB();
				}
			}
		}

	}

	public void jobExecutionVetoed(JobExecutionContext context) {
		// Called if the job execution was vetoed by a trigger listener
		System.out.println("Job execution vetoed: " + context.getJobDetail().getKey());
		info = setSysBatch("Kill", context);
		if (info != null) {
			info.UpdateDB();
		}
	}

	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		// Called after the job has been executed"Complete Executed!"
		SYSTEM_BATCH_INFO1 job = new SYSTEM_BATCH_INFO1();
		JobKey key = context.getJobDetail().getKey();
		Scheduler scheduler = SchedulerProvider.getScheduler();
		TriggerKey triggerKey = TriggerKey.triggerKey(key.getName(), key.getGroup());
		String StatusJobDB = job.getSysBatchInfo(key.getName()).getjob_info();
		if ("Done".equals(StatusJobDB)) {

		}
		info = setSysBatch("Done", context);
		try {

			if (jobException != null) {
				info = setSysBatch("Eror", context);
				erorAlert(jobException, info);
				scheduler.unscheduleJob(triggerKey);
			}

			info.UpdateDB();
		} catch (SchedulerException e) {
			erorAlert(e, info);
			e.printStackTrace();
		}

	}
}

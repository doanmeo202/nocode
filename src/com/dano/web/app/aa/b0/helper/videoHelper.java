package com.dano.web.app.aa.b0.helper;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

public class videoHelper {
	private static final String CLOUDINARY_CLOUD_NAME = "dtwvji2yi";
	private static final String CLOUDINARY_API_KEY = "823629794281893";
	private static final String CLOUDINARY_API_SECRET = "20_1LTJCGvCLsWoBBwK7vhlPRxM";

	public static void main(String[] args) {
		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", CLOUDINARY_CLOUD_NAME, "api_key",
				CLOUDINARY_API_KEY, "api_secret", CLOUDINARY_API_SECRET));

		String telegramVideoUrl = "https://api.telegram.org/file/bot5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0/videos/file_49";

		String cloudinaryVideoUrl = convertTelegramVideoToCloudinary(cloudinary, telegramVideoUrl);
		System.out.println(cloudinaryVideoUrl); // Đường dẫn video Cloudinary đã chuyển đổi
	}

	private static String convertTelegramVideoToCloudinary(Cloudinary cloudinary, String telegramVideoUrl) {
		try {
			// Tạo một URL từ đường dẫn video Telegram
			URL url = new URL(telegramVideoUrl);

			// Tạo tệp cục bộ để tải xuống video
			File videoFile = File.createTempFile("temp", ".mp4");

			// Tải xuống video từ URL Telegram vào tệp cục bộ
			Files.copy(url.openStream(), Path.of(videoFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);

			// Tải lên video từ tệp cục bộ lên Cloudinary
			Map map = new HashMap();
			Map<String, Object> uploadResult = cloudinary.uploader().upload(videoFile,map);

			// Lấy đường dẫn bảo mật của video đã tải lên
			String secureUrl = (String) uploadResult.get("secure_url");

			// Xóa tệp cục bộ sau khi tải lên thành công
			videoFile.delete();

			return secureUrl;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	/*
	 * private static String convertTelegramVideoToCloudinary(Cloudinary cloudinary,
	 * String telegramVideoUrl) { try { Map<String, Object> uploadResult =
	 * cloudinary.uploader().upload(telegramVideoUrl, ObjectUtils.asMap(
	 * "resource_type", "video", "format", "mp4", "eager", ObjectUtils.asArray(new
	 * Map[]{ ObjectUtils.asMap( "format", "webm", "transformation",
	 * ObjectUtils.asArray(new Map[]{ ObjectUtils.asMap("width", 720, "height", 480,
	 * "crop", "pad"), ObjectUtils.asMap("audio_codec", "none") }) ) }) ));
	 * 
	 * return (String) uploadResult.get("secure_url"); } catch (IOException e) {
	 * e.printStackTrace(); return null; } }
	 */
}

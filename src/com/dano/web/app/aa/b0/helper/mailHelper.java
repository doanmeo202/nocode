package com.dano.web.app.aa.b0.helper;

import java.nio.charset.StandardCharsets;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.ObjectUtils;

public class mailHelper {
	public static String findUser = "com.google.email.setting.finduser";
	public static String insertLogMail = "com.google.email.setting.maillogs";
	static JdbcHelper ds = new JdbcHelper();

	public static List<Map<String, Object>> findEmail(String USERNAME) {
		Map mapin = new HashMap();
		mapin.put("USERNAME", USERNAME);
		return ds.getDataFromKey(findUser, mapin);
	}

	public static Properties getPropertiesMail() {
		Properties props = new Properties();
		fOL fol = new fOL();
		for (fOL map : fol.getFol("EML")) {
			String is_show = ObjectUtils.toString(map.getnAME());
			if ("1".equals(is_show)) {
				System.out.println(map.getfIELD_NAME() + " = " + map.getsETTING());
				props.setProperty(map.getfIELD_NAME(), map.getsETTING());
			}

		}
		return props;
	}
	public static void sendMailHTML(String from, String to, String subject, String html) throws Exception {
		byte[] bytes = html.getBytes(StandardCharsets.UTF_8);
		Map logs = new HashMap();
		List<Map<String, Object>> list;
		try {
			list = findEmail(from);
		} catch (Exception e) {
			e.printStackTrace();
			list = findEmail(from);
		}
		if (list.size() < 1) {
			throw new Exception("Email [" + from + "] chưa được liên kết đến hệ thống.");
		}
		for (Map<String, Object> map : list) {
			logs.put("FROM", from);
			logs.put("TO", to);
			logs.put("STATUS", "0");
			String host = map.get("host").toString();
			String transport = map.get("protocol").toString();
			String password = map.get("password").toString();
			Properties props = getPropertiesMail();
			props.setProperty("mail.transport.protocol", transport);
			props.setProperty("mail.host", host);
			Session session = Session.getDefaultInstance(props);
			session.setDebug(true);
			try {
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(from));
				InternetAddress[] toAddresses = { new InternetAddress(to) };
				msg.setRecipients(Message.RecipientType.TO, toAddresses);
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				msg.setContent(new String(bytes, StandardCharsets.UTF_8), "text/html;charset=UTF-8");
				Transport t = session.getTransport(transport);
				t.connect(from, password);
				t.sendMessage(msg, msg.getAllRecipients());
				t.close();
			} catch (MessagingException mex) {
				logs.put("STATUS", "EM");
				mex.printStackTrace();
			}
			try {
				ds.setDataFromKey(insertLogMail, logs);
			} catch (Exception e) {
				System.out.println("insert Log err " + e.getMessage());
			}
		}
	}
	public static void sendMail(String from, String to, String subject, String decs) throws Exception {
		Map logs = new HashMap();
		List<Map<String, Object>> list;
		try {
			list = findEmail(from);
		} catch (Exception e) {
			e.printStackTrace();
			list = findEmail(from);
		}
		if (list.size() < 1) {
			logs.put("STATUS", "NFE");
			throw new Exception("Email [" + from + "] chưa được liên kết đến hệ thống.");
		}
		for (Map<String, Object> map : list) {
			logs.put("FROM", from);
			logs.put("TO", to);
			logs.put("STATUS", "0");
			String host = map.get("host").toString();
			String transport = map.get("protocol").toString();
			String password = map.get("password").toString();
			Properties props = getPropertiesMail();
			props.setProperty("mail.transport.protocol", transport);
			props.setProperty("mail.host", host);
			Session session = Session.getDefaultInstance(props);
			session.setDebug(true);
			try {
				
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(from));
				InternetAddress[] toAddresses = { new InternetAddress(to) };
				msg.setRecipients(Message.RecipientType.TO, toAddresses);
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				msg.setText(decs);
				//msg.setContent(message, "text/html");

				Transport t = session.getTransport(transport);
				t.connect(from, password);
				t.sendMessage(msg, msg.getAllRecipients());
				t.close();
			} catch (MessagingException mex) {
				logs.put("STATUS", "MEX");
				mex.printStackTrace();
			}
			try {
				ds.setDataFromKey(insertLogMail, logs);
			} catch (Exception e) {
				System.out.println("insert Log err " + e.getMessage());
			}
		}

	}

	public static void main(String[] args) {
		String from = "hotro.webdano@gmail.com";
		String to = "doanmeo202@gmail.com";
        String html="<!DOCTYPE html>\r\n" + 
   		"<html>\r\n" + 
   		"<head><meta charset=\"UTF-8\">\r\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n" + 
   		"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n" + 
   		"    <title>Chào Tuấn = ))</title>" + 
   		"<style>\r\n" + 
   		"/* This style sets the width of all images to 100%: */\r\n" + 
   		"img {\r\n" + 
   		"  width: 100%;\r\n" + 
   		"}\r\n" + 
   		"</style>\r\n" + 
   		"</head>\r\n" + 
   		"<body>\r\n" + 
   		"\r\n" + 
   		"<h2>Chào Tuấn!</h2>\r\n" + 
   		"\r\n" + 
   		"<p>Không biết mình có thể gửi email cho bạn được không?</p>\r\n" + 
   		"\r\n" + 
   		"<img src=\"https://i.imgur.com/KpFhPnz.png\" alt=\"HTML5 Icon\" width=\"128\" height=\"128\">\r\n" + 
   		"\r\n" + 
   		"<p>Chú ý: Mình viết chú ý này để xin phép gửi email này để xin phép bạn cho mình được gửi email,Cảm ơn bạn đã chú ý đến xin phép của mình!!!!!</p>\r\n" + 
   		"\r\n" + 
   		"<img src=\"https://png.pngtree.com/element_our/20190601/ourlarge/pngtree-attention-hazard-warning-sign-psd-transparent-bottom-image_1329342.jpg\" alt=\"HTML5 Icon\" style=\"width:128px;height:128px;\">\r\n" + 
   		"\r\n" + 
   		"</body>\r\n" + 
   		"</html>\r\n" + 
   		"\r\n" + 
   		"";
		// String to = "dnt.doanngocthanh@gmail.com";
		try {
			sendMailHTML(from, to, "【Notify】 Test Mail", html);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
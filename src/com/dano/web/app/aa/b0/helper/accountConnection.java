package com.dano.web.app.aa.b0.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sql2o.Sql2o;

public class accountConnection {
	private String driver;
	private String port;
	private String serverDB;
	private String hostname;
	private String username;
	private String password;
	private String database;
	private static String JDBC = "jdbc:";
	private static String PREFIX0 = "://";
	private static String PREFIX1 = ":";
	private static String PREFIX2 = "/";
	private static String UTF8 = "?useUnicode=yes&characterEncoding=UTF-8";

	public void checkEmtryData(String key, String value) throws Exception {
		if (value.isEmpty()) {
			throw new Exception(key.toUpperCase() + " is empty!");
		}
	}

	public accountConnection() {

	}

	public accountConnection(String driver, String port, String serverDB, String hostname, String username,
			String password, String database) {
		super();
		this.driver = driver;
		this.port = port;
		this.serverDB = serverDB;
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		this.database = database;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String getUrl() throws Exception {
		Map map = new HashMap();
		map.put("serverDB", serverDB);
		map.put("hostname", hostname);
		map.put("port", port);
		map.put("database", database);
		ArrayList myKeyList = new ArrayList(map.keySet());
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			checkEmtryData(key, value);
		}
		return JDBC.concat(serverDB).concat(PREFIX0).concat(hostname).concat(PREFIX1).concat(port).concat(PREFIX2)
				.concat(database).concat(UTF8);
	}

	public String getServerDB() {
		return serverDB;
	}

	public void setServerDB(String serverDB) {
		this.serverDB = serverDB;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public Sql2o getDataSet(accountConnection account) {

		return null;
	}

	public static void main(String[] args) {
		accountConnection ac = new accountConnection(); // new accountConnection("1123", "3306", "mysql", "aaaa",
														// "cxluser", "Thanh0974135042!","DB_STAG");
		ac.setServerDB("mysql");
		ac.setHostname("webdano.ddns.net");
		ac.setPort("3306");
		ac.setDatabase("4");
		try {
			System.out.println(ac.getUrl());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package com.dano.web.app.aa.b0.helper;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jetty.util.log.Log;

import com.dano.web.app.aa.b0.modun.AAB0_0100_mod;
import com.smattme.MysqlExportService;

public class backup_helper {
	static {
		Properties properties = new Properties();
		properties.setProperty("log4j.rootCategory", "DEBUG, all");
		properties.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		properties.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		properties.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		JdbcHelper ds = new JdbcHelper();
		AAB0_0100_mod mod = new AAB0_0100_mod();
		int i = 1;
		List listPackage = new ArrayList();
		while (true) {
			List<Map<String, Object>> li = mod.getFieldOptionList("SYS", "PACKAGE", "" + i);
			if (li.size() < 1) {
				break;
			} else {
				for (Map<String, Object> map : li) {
					String name = map.get("name").toString();
					String path = name + ".txt";

					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String formatted = df.format(new Date());
					String file_name = fileHelper.getPathTomcat() + "/log/" + formatted + "_" + path;
					properties.setProperty("log4j.category." + name, "DEBUG," + name);
//					properties.setProperty("log4j.appender." + name + ".layout", "org.apache.log4j.PatternLayout");
//					properties.setProperty("log4j.appender." + name + ".layout.ConversionPattern",
//							"%-5p: %3rms %C.%M():%3L %m%n");
					properties.setProperty("log4j.appender." + name + ".layout", "org.apache.log4j.PatternLayout");
					properties.setProperty("log4j.appender." + name + ".layout.ConversionPattern",
							"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
					properties.setProperty("log4j.appender." + name, "org.apache.log4j.RollingFileAppender");
					properties.setProperty("log4j.appender." + name + ".File", file_name);
					properties.setProperty("log4j.appender." + name + ".MaxFileSize", "500KB");
					// properties.setProperty("log4j.appender." + name,
					// "org.apache.log4j.ConsoleAppender");

					
				}

			}
			i++;

		}
		BasicConfigurator.configure();
		PropertyConfigurator.configure(properties);
	}
	public void backup() throws ClassNotFoundException, IOException, SQLException {
		JdbcHelper helper= new JdbcHelper();
		Properties properties = new Properties();
		properties.setProperty(MysqlExportService.DB_NAME, helper.getdB());
		properties.setProperty(MysqlExportService.DB_USERNAME, helper.getuSER());
		properties.setProperty(MysqlExportService.DB_PASSWORD, helper.getpASSWORD());
		properties.setProperty(MysqlExportService.EMAIL_HOST, "smtp.gmail.com");
		properties.setProperty(MysqlExportService.EMAIL_PORT, "25");
		properties.setProperty(MysqlExportService.EMAIL_USERNAME, "hotro.webdano@gmail.com");
		properties.setProperty(MysqlExportService.EMAIL_PASSWORD, "kqruosfvtozvivyv");
		properties.setProperty(MysqlExportService.EMAIL_FROM, "hotro.webdano@gmail.com");
		properties.setProperty(MysqlExportService.EMAIL_TO, "dnt.doanngocthanh@gmail.com");
		properties.setProperty(MysqlExportService.ADD_IF_NOT_EXISTS, "true");
		properties.setProperty(MysqlExportService.JDBC_DRIVER_NAME, "com.mysql.cj.jdbc.Driver");
		properties.setProperty(MysqlExportService.PRESERVE_GENERATED_ZIP, "true");
		properties.setProperty(MysqlExportService.PRESERVE_GENERATED_SQL_FILE, "false");
		properties.setProperty(MysqlExportService.JDBC_CONNECTION_STRING, helper.getuRL());
		properties.setProperty(MysqlExportService.TEMP_DIR, new File(fileHelper.getPath("BACKUP")).getPath());
		MysqlExportService mysqlExportService = new MysqlExportService(properties);
		mysqlExportService.export();
	}
	public backup_helper() {
		
	}
	public static void main(String[] args) throws Exception {
		backup_helper bk = new backup_helper();
		try {
			bk.backup();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package com.dano.web.app.aa.b0.helper;

import java.util.Random;

public class OPTHelper {
	public static synchronized String generateKey() {
		return Long.toString(System.currentTimeMillis());
	}

	/**
	 * 產生密碼
	 * 
	 * @param otpLength 長度
	 * @return 密碼
	 */
	public static synchronized String generatePassword(int otpLength) {
		Random rm = new Random(System.currentTimeMillis());
		StringBuffer str = new StringBuffer();
		int randomBound = 10;

		for (int i = 0; i < otpLength; i++) {
			str.append(rm.nextInt(randomBound));
		}

		return str.toString();
	}

	public static void main(String[] args) {
		System.out.println(generatePassword(4));
	}
}

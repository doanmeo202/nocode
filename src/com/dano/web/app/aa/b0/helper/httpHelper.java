package com.dano.web.app.aa.b0.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class httpHelper {

	public static InputStream getInputStreamFromURL(String urlString) throws IOException {
		URL url = new URL(urlString);
		URLConnection connection = url.openConnection();
		return connection.getInputStream();
	}

	public static String callCathayApi(String urlToRead, String meThod) throws Exception {

		String charset = "UTF-8";
		StringBuilder result = new StringBuilder();

		String urlParameters = "LANG=" + URLEncoder.encode("0", charset);
		urlParameters += "&CSIDNO=" + URLEncoder.encode("A000000168", charset);
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		String request = "http://localhost:8080/ZSWeb/api-gate/com.cathay.ak.mv.module.AKMV_utils.getPoliciesCoverage4cp";
		URL url = new URL(request);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("charset", "utf-8");
		conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		conn.setUseCaches(false);
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(postData);
		}
		if (conn.getResponseCode() == 200) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
				for (String line; (line = reader.readLine()) != null;) {
					result.append(line);
				}
			}
		}
		return result.toString();
	}

	public static String callHttp(String urlToRead, String filePath, String meThod) throws Exception {
		String charset = "UTF-8";
		File binaryFile = new File(filePath);
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(meThod);
		conn.setRequestProperty("Authorization", "Bearer " + "YcysNOk9d5LdfVOJJ7R1Vd4c2oTTGgDegadCFlok");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		try (OutputStream output = conn.getOutputStream();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);) {

			// Send binary file.
			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"")
					.append(CRLF);
			writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			writer.append(CRLF).flush();
			try {
				Files.copy(binaryFile.toPath(), output);
				output.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Important before continuing with writer!
			writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

			// End of multipart/form-data.
			writer.append("--" + boundary + "--").append(CRLF).flush();
		}
		if (conn.getResponseCode() == 200) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
				for (String line; (line = reader.readLine()) != null;) {
					result.append(line);
				}
			}
		} else {
			throw new Exception(conn.getInputStream().toString());
		}
		return result.toString();
	}

	// HttpServletRequest request, HttpServletResponse response
	public HttpServletResponse renderVideo(String url, HttpServletResponse response) {
		try {
			URL videoUrl = new URL(url);
			InputStream inputStream = videoUrl.openStream();
			OutputStream outputStream = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			inputStream.close();
			outputStream.close();
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("video/mp4");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	public static void main(String[] args) throws Exception {
		String url = "https://api.telegram.org/file/bot5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0/videos/file_30.MP4";
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				InputStream inputStream = httpEntity.getContent();
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				byte[] buffer = new byte[4096];
				int bytesRead = -1;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				byte[] videoBytes = outputStream.toByteArray();
				outputStream.close();
				EntityUtils.consume(httpEntity);
				System.out.println("Video downloadedsuccessfully!");

				// Trả về response với HTTP status code 200 và body là nội dung của video
				// Response response = Response.status(200).entity(videoBytes).build();
				// return response;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Trả về response với HTTP status code 500 nếu có lỗi xảy ra
		// Response response = Response.status(500).entity("Failed to download
		// video.").build();
		// return response;

	}

}

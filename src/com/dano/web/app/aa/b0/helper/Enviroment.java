package com.dano.web.app.aa.b0.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/*@Author: DoanNgocThanh
 *Version: 1.0*/
public class Enviroment {
	private static Logger log = Logger.getLogger(Enviroment.class);
	private static DataSet ds;
	private static boolean STAG = false;
	private static boolean PROD = false;
	private static String pathFileConfig;
	private static Properties SYS_PROPERTIES = System.getProperties();
	private static String OS_NAME = SYS_PROPERTIES.get("os.name").toString();
	public static String IP_ENV;
	public static String USERNAME_DB;
	public static String PASSWORD_DB;
	public static String DATABASE_DB;
	public static String URL_CONNECT_DB;
	public static String STRING_ENVIROMENT;
	public static Map<String, String> DATABASE_CONFIG;
	public static String url_local;
	public static String url_prod;
	public static String username_prod;
	public static String password_prod;
	public static String username_local;
	public static String password_local;
	public static String FIREBASE_PARAM;
	public static String CODEMAP_TABLE;
	public static String SYS_BOT_TOKEN;
	public static String SYS_BOT_USERNA;
	public static String SYS_BOT_CHATID;
	public static String MAIL_ACCOUNT_SYS;
	public batchRealTimeDB batchDataRealTime;

	public static void logWrite(String text) {
		String dateLog = new Date().toGMTString();
		System.err.println(dateLog + " " + Enviroment.class.getSimpleName() + " :" + text);
		log.debug(text);
	}

	static {
		System.err.println("================================================================================");
		String dateLog = new Date().toGMTString();
		DATABASE_CONFIG = new HashMap<String, String>();
		CODEMAP_TABLE = "DTCM0004_CODEMAPSQL";
		if (OS_NAME.startsWith("W")) {// check OS
			STAG = true;
			PROD = false;
			pathFileConfig = "D://New folder/nocode/WebContent/WEB-INF/database.properties";
			STRING_ENVIROMENT = "STAG";
		} else {
			STAG = false;
			PROD = true;
			// pathFileConfig = "/opt/tomcat/conf/database.properties";
			STRING_ENVIROMENT = "PROD";
		}
		logWrite(STRING_ENVIROMENT);
		logWrite(pathFileConfig);
		if (STAG) {
			DATABASE_CONFIG.put("username", "cxluser");
			DATABASE_CONFIG.put("password", "Thanh0974135042@");
			DATABASE_CONFIG.put("database", "DB_STAG");
			DATABASE_CONFIG.put("host", "115.76.108.89");
			DATABASE_CONFIG.put("port", "3306");
			DATABASE_CONFIG.put("drive", "com.mysql.cj.jdbc.Driver");
		}
		if (PROD) {
			Properties properties = new Properties();
			properties = getPropertiesFile(pathFileConfig);
			DATABASE_CONFIG.put("username", properties.getProperty("username"));
			DATABASE_CONFIG.put("password", properties.getProperty("password"));
			DATABASE_CONFIG.put("database", properties.getProperty("database"));
			DATABASE_CONFIG.put("host", properties.getProperty("host"));
			DATABASE_CONFIG.put("port", properties.getProperty("port"));
			DATABASE_CONFIG.put("drive", properties.getProperty("drive"));
		}
		URL_CONNECT_DB = "jdbc:mysql://" + DATABASE_CONFIG.get("host").toString() + ":"
				+ DATABASE_CONFIG.get("port").toString() + "/" + DATABASE_CONFIG.get("database").toString()
				+ "?useUnicode=yes&characterEncoding=UTF-8";
		DATABASE_CONFIG.put("url", URL_CONNECT_DB);
		USERNAME_DB = DATABASE_CONFIG.get("username").toString();
		PASSWORD_DB = DATABASE_CONFIG.get("password").toString();
		DATABASE_DB = DATABASE_CONFIG.get("database").toString();

		if (DataSet.pingConnection()) {
			// Get From Config DB
			ds = DataSet.getDataSet();
			IP_ENV = ds.get999Config("IP_" + STRING_ENVIROMENT);
			FIREBASE_PARAM = ds.get999Config("FIREBASE_PARAM_" + STRING_ENVIROMENT);
			SYS_BOT_TOKEN = ds.get999Config("SYS_BOT_TOKEN_" + STRING_ENVIROMENT);
			SYS_BOT_USERNA = ds.get999Config("SYS_BOT_USERNAME_" + STRING_ENVIROMENT);
			SYS_BOT_CHATID = ds.get999Config("SYS_BOT_CHATID_" + STRING_ENVIROMENT);
			MAIL_ACCOUNT_SYS = ds.get999Config("MAIL_ACCOUNT_SYS_" + STRING_ENVIROMENT);
			logWrite(STRING_ENVIROMENT);
		} else {
			System.err.println(dateLog + " [ERROR][Enviroment(98)] : connection database error please check!");
		}
		System.err.println("================================================================================");
	}

	public synchronized batchRealTimeDB getFireBaseConfig(String key) {
		batchDataRealTime = new batchRealTimeDB();
		try {
			batchDataRealTime.updateSetting(ds.get999Config("FIREBASE_APIKEY_" + key.toUpperCase()),
					ds.get999Config("FIREBASE_URL_" + key.toUpperCase()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return batchDataRealTime;
	}

	public synchronized batchRealTimeDB getFireBaseConfig() {
		batchDataRealTime = new batchRealTimeDB();
		try {
			batchDataRealTime.updateSetting(ds.get999Config("FIREBASE_APIKEY_" + FIREBASE_PARAM.toUpperCase()),
					ds.get999Config("FIREBASE_URL_" + FIREBASE_PARAM.toUpperCase()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return batchDataRealTime;
	}

	public static synchronized Properties getPropertiesFile(String filePath) {
		Properties props = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(filePath);
			props.load(fis);
		} catch (IOException e) {
			System.out.println("Not found " + filePath);
			e.printStackTrace();
			return null;
		}
		return props;
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		Enviroment e = new Enviroment();
		System.out.println(e.MAIL_ACCOUNT_SYS);
	}
}

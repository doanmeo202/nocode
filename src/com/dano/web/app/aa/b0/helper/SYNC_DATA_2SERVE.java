package com.dano.web.app.aa.b0.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;

public class SYNC_DATA_2SERVE {
	private batchQueryDataSet bqds_local;
	private batchUpdateDataSet buds_local;
	private batchUpdateDataSet buds_prod;
	private batchQueryDataSet bqds_prod;
	private DataSet ds_local;
	private DataSet ds_prod;

	private static Logger log = Logger.getLogger(SYNC_DATA_2SERVE.class);

	public String showDDL(batchQueryDataSet bqds, String table) {
		String DDL = "";
		bqds.beginTransaction();
		try {
			List<Map<String, Object>> data = bqds.searchAndRetrieve("SHOW CREATE TABLE " + table);
			for (Map<String, Object> map : data) {
				DDL = map.get("create table").toString();
			}
		} finally {
			bqds.endTransaction();
		}
		return DDL;
	}

	public synchronized List<String> listTableInSchema(batchQueryDataSet bqds, String schema) {
		bqds.beginTransaction();
		List<String> tables = new ArrayList<String>();
		try {
			List<Map<String, Object>> li = bqds.searchAndRetrieve("SELECT * FROM information_schema.tables");
			for (Map<String, Object> map : li) {
				if (schema.toUpperCase().equals(map.get("table_schema").toString().toUpperCase())) {
					tables.add(map.get("table_name").toString());
				}
			}
		} finally {
			bqds.endTransaction();
		}
		return tables;
	}

	public List<Map<String, Object>> getDataFromTable(DataSet ds, String Table) {
		List<Map<String, Object>> li = new ArrayList<Map<String, Object>>();

		li = ds.searchAndRetrieve("SELECT * FROM " + Table);

		return li;
	}

	public void setBatchUpdateDataSet(batchUpdateDataSet buds, String Table, List<Map> data, String Sql) {

	}

	public String getSqlInsert(Map<String, Object> map, String Table) {
		StringBuilder columns = new StringBuilder();
		StringBuilder values = new StringBuilder();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() != null && !ObjectUtils.toString(entry.getValue()).isEmpty()) {
				columns.append(entry.getKey()).append(", ");
				values.append("'").append(entry.getValue()).append("', ");
			}
		}
		columns.setLength(columns.length() - 2); // Xóa dấu phẩy cuối cùng
		values.setLength(values.length() - 2); // Xóa dấu phẩy cuối cùng
		String sqlInsert = "INSERT INTO " + Table + " (" + columns.toString() + ") VALUES (" + values.toString() + ");";
		return sqlInsert;
	}

	public String getSqlUpdate(Map<String, Object> map, String CODE_NAME) {
		StringBuilder sqlUpdate = new StringBuilder("UPDATE DTCM0004_CODEMAPSQL SET ");
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() != null && !ObjectUtils.toString(entry.getValue()).isEmpty()) {
				sqlUpdate.append(entry.getKey()).append(" = '").append(entry.getValue()).append("', ");
			}
		}
		sqlUpdate.setLength(sqlUpdate.length() - 2);
		sqlUpdate.append(" WHERE CODE_NAME = '").append(CODE_NAME).append("'");
		return sqlUpdate.toString();
	}

	public String getSqlInsertWithParam(List<Map<String, Object>> li, String Table) {
		StringBuilder columns = new StringBuilder();
		StringBuilder values = new StringBuilder();
		if (li.size() > 0) {

		} else {
			return null;
		}
		Map<String, Object> map = li.get(0);
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() != null && !ObjectUtils.toString(entry.getValue()).isEmpty()) {
				columns.append(entry.getKey()).append(", ");
				values.append(":").append(entry.getKey()).append(", ");
			}
		}
		columns.setLength(columns.length() - 2); // Xóa dấu phẩy cuối cùng
		values.setLength(values.length() - 2); // Xóa dấu phẩy cuối cùng
		String sqlInsert = "INSERT INTO " + Table + " (" + columns.toString() + ") VALUES (" + values.toString() + ");";
		return sqlInsert;
	}

	public synchronized void backup(String usernamelocal, String passwordlocal, String urllocal, String usernameprod,
			String passwordprod, String urlprod, String TYPE) throws Exception {
		if (TYPE.toUpperCase().startsWith("S")) { // PROD to local
			ds_local = DataSet.getDataSet(urllocal, usernamelocal, passwordlocal);
			ds_prod = DataSet.getDataSet(urlprod, usernameprod, passwordprod);
			bqds_local = new batchQueryDataSet(urllocal, usernamelocal, passwordlocal);
			bqds_prod = new batchQueryDataSet(urlprod, usernameprod, passwordprod);
			buds_local = new batchUpdateDataSet(urllocal, usernamelocal, passwordlocal);
			buds_prod = new batchUpdateDataSet(urlprod, usernameprod, passwordprod);
		} else if (TYPE.toUpperCase().startsWith("P")) { // local to PROD
			ds_prod = DataSet.getDataSet(urllocal, usernamelocal, passwordlocal);
			ds_local = DataSet.getDataSet(urlprod, usernameprod, passwordprod);
			bqds_prod = new batchQueryDataSet(urllocal, usernamelocal, passwordlocal);
			bqds_local = new batchQueryDataSet(urlprod, usernameprod, passwordprod);
			buds_prod = new batchUpdateDataSet(urllocal, usernamelocal, passwordlocal);
			buds_local = new batchUpdateDataSet(urlprod, usernameprod, passwordprod);
		} else {
			throw new Exception("không tìm thấy môi trường");
		}

		List<String> tables = listTableInSchema(bqds_prod, "db_stag");
		for (String table : tables) {
			String DDL_STAG = showDDL(bqds_local, table).replace("utf8mb4_0900_ai_ci", "utf8mb4_unicode_ci");
			String DDL_PROD = showDDL(bqds_prod, table).replace("utf8mb4_0900_ai_ci", "utf8mb4_unicode_ci");
			if (DDL_STAG.toUpperCase().equals(DDL_PROD.toUpperCase())) {
				System.out.println("CHECK DDL OK: " + table);

			} else {

				log.debug("====================================================================================");
				log.debug(DDL_PROD);
				log.debug("========> DDL CHANGE: " + table);
				log.debug(DDL_STAG);
				log.debug("====================================================================================");
				// STEP1 :Get DataBackup
				List<Map<String, Object>> dataBackup = getDataFromTable(ds_prod, table);
				// log.debug((dataBackup);

				if (!"dtcm0004_codemapsql".toUpperCase().equals(table.toUpperCase())) {
					// STEP2 :DROP TABLE
					try {
						String SQL = "DROP TABLE IF EXISTS " + table;
						ds_local.update(SQL);
					} catch (Exception e) {
						log.debug("bor");
					}
					// STEP3 :CREATE TABLE
					ds_local.update(DDL_PROD);
				}

				// STEP4 :BACKUP DATA
				if (dataBackup.size() > 0) {
					String sqlinsert = getSqlInsertWithParam(dataBackup, table);
					for (Map map : dataBackup) {
						String sqlinsertNe = getSqlInsert(map, table);
						ds_local.update(sqlinsertNe);
					}
				}
			}

		}
	}

	public synchronized void dePloySqlToPROD(String keySql) {
		try {
			List<Map<String, Object>> li = ds_local.getSQL(keySql);
			List<Map<String, Object>> li2 = ds_prod.getSQL(keySql);
			if (li2.size() > 0) {
				for (Map<String, Object> map : li) {
					String sqlupdate = getSqlUpdate(map, keySql);
					ds_prod.update(sqlupdate);
				}
			} else {
				for (Map<String, Object> map : li) {
					String sqlinsertNe = getSqlInsert(map, "dtcm0004_codemapsql".toUpperCase());
					ds_prod.update(sqlinsertNe);
				}
			}
			log.debug("deploy sql ds_local to ds_prod complete!!!!");
		} catch (Exception e) {
			log.debug("deploy sql ds_local to ds_prod ISFAILED!!!!");
			log.error(e);
			// TODO: handle exception
		}
	}

	public synchronized void backupTable(String Table, int choice) {
		// 0 ->PROD về STAG
		// 1 ->STAG về PROD
		String DDL_STAG = showDDL(bqds_local, Table).replace("utf8mb4_0900_ai_ci", "utf8mb4_unicode_ci");
		String DDL_PROD = showDDL(bqds_prod, Table).replace("utf8mb4_0900_ai_ci", "utf8mb4_unicode_ci");
		// STEP1 :Get DataBackup
		List<Map<String, Object>> dataBackup;
		if (choice == 0) {
			log.debug("bqds_prod....................");
			dataBackup = getDataFromTable(ds_prod, Table);

		} else {
			log.debug("bqds_local....................");
			dataBackup = getDataFromTable(ds_local, Table);

		}
		if (DDL_STAG.toUpperCase().equals(DDL_PROD.toUpperCase())) {
			System.out.println("CHECK DDL OK: " + Table);
		} else {
			DDL_PROD = DDL_PROD.replace(Table.toLowerCase(), Table.toUpperCase());
			DDL_STAG = DDL_STAG.replace(Table.toLowerCase(), Table.toUpperCase());
			log.debug("====================================================================================");
			log.debug(DDL_PROD);
			log.debug("========> DDL CHANGE: " + Table);
			log.debug(DDL_STAG);
			log.debug("====================================================================================");

			if (!"dtcm0004_codemapsql".toUpperCase().equals(Table.toUpperCase())) {
				// STEP2 :DROP TABLE
				try {
					String SQL = "DROP TABLE IF EXISTS " + Table;
					if (choice == 0) {
						ds_local.update(SQL);
					} else {
						ds_prod.update(SQL);
					}

				} catch (Exception e) {
					log.debug("bor");
				}
				// STEP3 :CREATE TABLE
				try {
					if (choice == 0) {
						ds_local.update(DDL_PROD);
					} else {
						ds_prod.update(DDL_STAG);
					}
				} catch (Exception e) {
					log.info("STEP3");
					log.info(e.getMessage());
				}

			}

		}
		// STEP4 :BACKUP DATA
		if (dataBackup.size() > 0) {
			String sql = getSqlInsertWithParam(dataBackup, Table);
			log.debug(sql);
			for (Map map : dataBackup) {
				String sqlinsertNe = getSqlInsert(map, Table);
				try {
					if (choice == 0) {
						ds_local.update(sqlinsertNe);

					} else {
						ds_prod.update(sqlinsertNe);

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		System.out.println("backup Done!");
	}

	public SYNC_DATA_2SERVE() {
		ds_local = DataSet.getDataSet(Enviroment.url_local, Enviroment.username_local, Enviroment.password_local);
		ds_prod = DataSet.getDataSet(Enviroment.url_prod, Enviroment.username_prod, Enviroment.password_prod);
		bqds_local = new batchQueryDataSet(Enviroment.url_local, Enviroment.username_local, Enviroment.password_local);
		bqds_prod = new batchQueryDataSet(Enviroment.url_prod, Enviroment.username_prod, Enviroment.password_prod);
		buds_local = new batchUpdateDataSet(Enviroment.url_local, Enviroment.username_local, Enviroment.password_local);
		buds_prod = new batchUpdateDataSet(Enviroment.url_prod, Enviroment.username_prod, Enviroment.password_prod);
	}

	public static void main(String[] args) {
		SYNC_DATA_2SERVE b = new SYNC_DATA_2SERVE();
		// b.dePloySqlToPROD("com.dano.web.app.aa.b1.modun.AAB1_0100_GET_IP");
	System.out.println(b.ds_prod.getSQL("com.system.update.status.batch"));
		//com.system.update.status.batch
		//b.backupTable("DTCM0004_CODEMAPSQL".toUpperCase(), 0);
		// System.out.println(b.ds_prod.get999Config("FIREBASE_URL_WDANO48"));
	}
}

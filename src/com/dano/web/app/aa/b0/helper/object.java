package com.dano.web.app.aa.b0.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

public class object {
	private static DataSet ds;
	private List<FieldSpec> liFieldSpec;
	private List<MethodSpec> liMethodSpec;
	private javax.lang.model.element.Modifier PRIVATE = javax.lang.model.element.Modifier.PRIVATE;
	private javax.lang.model.element.Modifier PUBLIC = javax.lang.model.element.Modifier.PUBLIC;

	public List<String> listTable() {
		List<String> returnList = new ArrayList<String>();
		ds = DataSet.getDataSet();
		ds.setField("TABLE_SCHEMA", "DB_STAG");
		List<Map<String, Object>> li = ds.searchAndRetrieve("com.system.selecttablename");
		for (Map map2 : li) {
			if (!ObjectUtils.toString(map2.get("table_name")).equals("")) {
				returnList.add(ObjectUtils.toString(map2.get("table_name")).toUpperCase());
			}

		}
		return returnList;
	}

	public List<Map<String, Object>> getDataFromTable(String table) {

		List<String> returnList = new ArrayList<String>();
		ds = DataSet.getDataSet();
		ds.setField("TABLE_SCHEMA", "DB_STAG");
		List<Map<String, Object>> li = ds.searchAndRetrieve("SELECT * FROM " + table + "  limit 1");
		return li;
	}

	public FieldSpec nameField(String key) {
		FieldSpec nameField = FieldSpec.builder(String.class, key).addModifiers(PRIVATE).build();
		return nameField;
	}

	public MethodSpec getMethod(String key) {
		MethodSpec getNameMethod = MethodSpec.methodBuilder("get" + key).returns(String.class)
				.addStatement("return this." + key).addModifiers(PUBLIC).build();
		return getNameMethod;
	}

	public MethodSpec setMethod(String key) {
		MethodSpec setNameMethod = MethodSpec.methodBuilder("set" + key).addParameter(String.class, key)
				.addStatement("this." + key + " =" + key).addModifiers(PUBLIC).build();
		return setNameMethod;
	}

	public void createFileObject(String table) {
		liFieldSpec = new ArrayList<FieldSpec>();
		liMethodSpec = new ArrayList<MethodSpec>();
		List<Map<String, Object>> li = getDataFromTable(table);
		for (Map map : li) {
			ArrayList myKeyList = new ArrayList(map.keySet());
			for (int i = 0; i < myKeyList.size(); i++) {
				String key = (String) myKeyList.get(i);
				liFieldSpec.add(nameField(key));
				liMethodSpec.add(getMethod(key));
				liMethodSpec.add(setMethod(key));
			}

		}
		Builder personClass = TypeSpec.classBuilder(table);
		for (FieldSpec fieldSpec : liFieldSpec) {
			personClass.addField(fieldSpec);
		}
		for (MethodSpec methodSpec : liMethodSpec) {
			personClass.addMethod(methodSpec);
		}
		TypeSpec personClass_ = personClass.addModifiers(PUBLIC).build();
		String voPath = "com.dano.web.app.database.vo";
		JavaFile javaFile = JavaFile.builder(voPath, personClass_).build();
		try {
			File file = new File("./src/");
			javaFile.writeTo(file);
			System.out.println(file.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createAllVoInTable() {
		List<String> li = listTable();
		for (String table : li) {
			createFileObject(table);
		}
	}

	public static void main(String[] args) {
		object obj = new object();
		obj.createFileObject("system_batch_info".toUpperCase());
	}
}

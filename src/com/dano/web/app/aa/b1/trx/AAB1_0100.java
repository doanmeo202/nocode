package com.dano.web.app.aa.b1.trx;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dano.web.app.aa.b0.helper.GetServerIP;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.fileHelper;
import com.dano.web.app.aa.b1.modun.AAB1_0100_mod;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class AAB1_0100 {
	@Autowired
	private HttpServletRequest request;
	private Logger log = Logger.getLogger(AAB1_0100.class.getName());
	private GetServerIP sever = new GetServerIP();
	private ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping("/dev")
	public String indexPage(Model model, HttpServletResponse response) {

		model.addAttribute("IP", sever.getPath());
		return "dev/AAB1_0100";
	}

	@RequestMapping(value = "/uploadFile", produces = "text/plain;charset=UTF-8", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(@RequestParam("file") MultipartFile file[], @RequestParam Map<String, Object> params, HttpServletResponse response)
			throws UnsupportedEncodingException, JsonProcessingException {
		List<Map> returnjson = new ArrayList();
		try {
			for (int i = 0; i < file.length; i++) {
               Map info = new HashMap();
				String fileName = StringUtils.cleanPath(file[i].getOriginalFilename());
				long size = file[i].getSize();
				String filecode;
				String path= ObjectUtils.toString(params.get("path"),"");
				filecode=fileHelper.saveFile(path,fileName, file[i]);
				info.put("code", "0");
				info.put("FileName", fileName);
				info.put("Size", size);
				info.put("DownloadUri", "/downloadFile/" + filecode);
				returnjson.add(info);
			
			}
		
		} catch (Exception e) {
			 Map info = new HashMap();
			 info.put("err", e);
			 returnjson.add(info);
			System.out.println(e);
		}
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/downloadFile/{fileCode}")
	public ResponseEntity<String> downloadFile(@PathVariable("fileCode") String fileCode) throws IOException {
		ResponseEntity respEntity = null;
		File result = null;
		fileHelper downloadUtil = new fileHelper();
		Resource resource = null;
		try {
			resource = downloadUtil.getFileAsResource(fileCode);
			result = resource.getFile();
			if (result.exists()) {
				InputStream inputStream = resource.getInputStream();
				String type = result.toURL().openConnection().guessContentTypeFromName(resource.getFilename());
				String realPathtoUploads = request.getServletContext().getRealPath(resource.getFile().getPath());
				byte[] out = org.apache.commons.io.IOUtils.toByteArray(inputStream);

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("content-disposition", "attachment; filename=" + resource.getFilename());
				responseHeaders.add("Content-Type", type);
				System.out.println("realPathtoUploads: " + resource.getFile().getPath());
				respEntity = new ResponseEntity(out, responseHeaders, HttpStatus.OK);
			} else {
				respEntity = new ResponseEntity("File Not Found", HttpStatus.OK);
			}
		} catch (IOException e) {

		}
		return respEntity;
	}

	@RequestMapping(value = "/dispatcher", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile(@RequestParam Map<String, Object> params) {
		File file = null;
		try {
			String filePath = ObjectUtils.toString(params.get("path"), "");
			String fileName = ObjectUtils.toString(params.get("fileName"), "");
			fileHelper downloadUtil = new fileHelper();
			file = downloadUtil.getFileAsResource(filePath, fileName).getFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new FileSystemResource(file);
	}

	@RequestMapping(value = "/getFile/{file_name}", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFileToLoad(@PathVariable("file_name") String fileName) {
		File file = null;
		try {
			fileHelper downloadUtil = new fileHelper();
			file = downloadUtil.getFileAsResource(fileName).getFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new FileSystemResource(file);
	}

	@RequestMapping("/log")
	public String logPage(Model model, HttpServletResponse response) throws UnknownHostException {

		/*
		 * Map returnjson = new HashMap(); returnjson.put("listFile",
		 * listFilesUsingJavaIO(pathFile()));
		 */
		model.addAttribute("listFile", listFilesUsingJavaIO(sever.getPathLog()));
		return "dev/AAB1_0100/AAB1_0100_LOG";
	}

	@RequestMapping("/test-mail")
	public String testMailPage(Model model, HttpServletResponse response) throws UnknownHostException {

		return "dev/AAB1_0100/AAB1_0100_TEST_MAIL";
	}

	@RequestMapping(value = "/send-mail/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String senMail(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
		Map returnjson = new HashMap();
		try {
			String fromEmail = ObjectUtils.toString(params.get("from"));
			// Mat khai email cua ban
			String password = ObjectUtils.toString(params.get("password"));
			// dia chi email nguoi nhan
			String toEmail = ObjectUtils.toString(params.get("to"));
			String subject = "Java Example Test";
			String body = "Hello Admin";
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true"); // enable STARTTLS
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
			props.put("mail.debug", "true");
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail, password);
				}
			};
			Session session = Session.getInstance(props, auth);
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setFrom(new InternetAddress(fromEmail, "NoReply-JD"));
			msg.setReplyTo(InternetAddress.parse(fromEmail, false));
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			Transport.send(msg);
			System.out.println("Gui mail thanh cong");
			returnjson.put("msg", "Gui mail thanh cong");
		} catch (Exception e) {
			// TODO: handle exception
			returnjson.put("msg", "loi ne: " + e);
		}

		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/log-listfiles", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String logListFile(Model model, HttpServletResponse response) throws JsonProcessingException {

		Map returnjson = new HashMap();

		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping("/sql-tool")
	public String sqlPage(Model model, HttpServletResponse response) {

		return "dev/AAB1_0100/AAB1_0100_SQLTOOLS";
	}

	@RequestMapping(value = "/sql-tool/select/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String sqlsqlToolSelect(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
	
		String KeySql = ObjectUtils.toString(params.get("keySql"));
		String host = ObjectUtils.toString(params.get("host"));
		String database = ObjectUtils.toString(params.get("database"));
		JdbcHelper mod = new JdbcHelper();
		if(!"".equals(host)) {
			mod.sethOST(host);
		}
		if(!"".equals(database)) {
			mod.setdB(database);
		}
		mod.reSetUrl();
		System.out.println("trx+=======================================");
		System.out.println(database);
		System.out.println(mod.getuRL());
		System.out.println("trx+=======================================");
		Map returnjson = new HashMap();
		returnjson.put("QueryCODE_MAP", mod.QueryCODE_MAP(KeySql));
		returnjson.put("QueryCODE_MAPLikeAll", mod.QueryCODE_MAPLikeAll(KeySql));
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/toGetFileDir", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String doGetFileDir(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
		fileHelper file= new fileHelper();
		Map returnjson = new HashMap();
		returnjson.put("listFile", file.getListOfFiles(ObjectUtils.toString(params.get("path"))));
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/sql-tool/insert/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String sqlsqlToolInsert(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
		String KeySql = ObjectUtils.toString(params.get("keySql"));
		String content = ObjectUtils.toString(params.get("content"));
		String host = ObjectUtils.toString(params.get("host"));
		String database = ObjectUtils.toString(params.get("database"));
		JdbcHelper mod = new JdbcHelper();
		if(!"".equals(host)) {
			mod.sethOST(host);
		}
		if(!"".equals(database)) {
			mod.setdB(database);
		}
		mod.reSetUrl();
		if (!KeySql.equals("")) {
			mod.InsertCODE_MAP(KeySql, content);
		}
		Map returnjson = new HashMap();
		returnjson.put("QueryCODE_MAPLikeAll", mod.QueryCODE_MAPLikeAll(KeySql));
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/getDevMenu/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String getDevMenu(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {

		JdbcHelper mod = new JdbcHelper();
		AAB1_0100_mod mod1 = new AAB1_0100_mod();

		Map returnjson = new HashMap();
		returnjson.put("menu", mod1.getMenu());
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/sql-tool/update/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String sqlsqlToolUpdate(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
	
		String KeySql = ObjectUtils.toString(params.get("keySql"));
		String content = ObjectUtils.toString(params.get("content"));
		String host = ObjectUtils.toString(params.get("host"));
		String database = ObjectUtils.toString(params.get("database"));
		JdbcHelper mod = new JdbcHelper();
		if(!"".equals(host)) {
			mod.sethOST(host);
		}
		if(!"".equals(database)) {
			mod.setdB(database);
		}
		mod.reSetUrl();
		int check = mod.updateCODE_MAP(KeySql, content);
		Map returnjson = new HashMap();
		returnjson.put("QueryCODE_MAPLikeAll", mod.QueryCODE_MAPLikeAll(KeySql));
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	@RequestMapping(value = "/sql-tool/delete/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String sqlsqlToolDelete(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws JsonProcessingException {
		String host = ObjectUtils.toString(params.get("host"));
		String database = ObjectUtils.toString(params.get("database"));
		JdbcHelper mod = new JdbcHelper();
		JdbcHelper mod1 = new JdbcHelper();
		if(!"".equals(host)) {
			mod.sethOST(host);
			mod1.sethOST(host);
		}
		if(!"".equals(database)) {
			mod.setdB(database);
			mod1.setdB(database);
		}
		mod.reSetUrl();
		String KeySql = ObjectUtils.toString(params.get("keySql"));
		mod.deleteCODE_MAP(KeySql);
		
		Map returnjson = new HashMap();
		returnjson.put("QueryCODE_MAPLikeAll", mod1.QueryCODE_MAPLikeAll(KeySql));
		response.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);
	}

	public static Set<String> listFilesUsingJavaIO(String dir) {
		return Stream.of(new File(dir).listFiles()).filter(file -> !file.isDirectory()).map(File::getName)
				.collect(Collectors.toSet());
	}

	@RequestMapping(value = "/show-log/", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public void readLogPage(@RequestParam Map<String, Object> params, Model model, HttpServletResponse response)
			throws IOException {
		String logName = ObjectUtils.toString(params.get("logName"));
		String logtext = "";

		// response.setContentType("text/plain");
		// response.setHeader("Content-Disposition","attachment;filename=D:/wsadlog
		// (7).log");
		Writer out = response.getWriter();
		try (BufferedReader br = Files.newBufferedReader(Paths.get(sever.getPathLog() + logName))) {
			for (String line = null; (line = br.readLine()) != null;) {
				out.write(line);
			}
			out.flush();
			out.close();
		}

		// return objectMapper.writeValueAsString(returnjson);

	}

	@RequestMapping("/batchAction")
	public String batchAction(@RequestParam Map<String, Object> params) throws IOException {
//		System.out.println(params);
//
//		if ("Stop".equals(params.get("action"))) {
//			try {
//				System.out.println(ObjectUtils.toString(params.get("jobName")));
//				rg.deleteJob(ObjectUtils.toString(params.get("jobName")));
//			} catch (ClassNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SchedulerException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else if ("Run".equals(params.get("action"))) {
//			rg.schedule(params.get("jobName").toString());
//		} else if ("Hold".equals(params.get("action"))) {
//			try {
//				rg.pauseJob(params.get("jobName").toString());
//			} catch (ClassNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SchedulerException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else if ("Check".equals(params.get("action"))) {
//
//		}
		return "dev/AAB1_0100/AAB1_0100_BATCH";
	}

	public static void main(String[] args) throws Exception {
		final String dir = System.getProperty("user.dir");
		System.out.println("current dir = " + dir);
		String cmd = "cd " + dir + " && dir";
		cmd += "";
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", cmd);
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}
			System.out.println(line);
		}
	}
}

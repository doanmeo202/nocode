package com.dano.web.app.aa.b1.modun;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dano.web.app.aa.b0.helper.JdbcHelper;

public class AAB1_0100_mod {
	private Map map;
    private String GET_MENU="com.dano.web.app.aa.b0.modun.AAB1_0100_GET_MENU";
    
	public List<Map<String, Object>> getMenu() {
		JdbcHelper ds = new JdbcHelper();
		map = new HashMap();
	    return ds.getDataFromKey(GET_MENU, map);
	}
}

package com.telegram.core;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import com.dano.web.app.aa.b0.helper.JdbcHelper;
import com.dano.web.app.aa.b0.helper.VoTooL;

public class TelegramBotSetup extends TelegramLongPollingBot {
	private String botUsername;
	private String botToken;
	private String sqlinsert = "com.telegram.template.insertjson";
	private static final Logger log = Logger.getLogger(TelegramBotSetup.class);

	public void setBotUsername(String botUsername) {
		this.botUsername = botUsername;
	}

	public void setBotToken(String botToken) {
		this.botToken = botToken;
	}

	public String getBotUsername() {
		return botUsername;
	}

	public String getBotToken() {
		return botToken;
	}

	public String getFileID(String FileId, ChatBotConfig chatBot) throws TelegramApiException {
		GetFile request = new GetFile(FileId);
		String URL_DOWNLOAD = chatBot.execute(request).getFileUrl(chatBot.getBotToken());
		return URL_DOWNLOAD;
	}

	@Override
	public void onUpdateReceived(Update update) {

		JdbcHelper hp = new JdbcHelper();
		hp.setField("IS_SETUP", "N");
		hp.setField("BOT_USERNAME", getBotUsername());
		boolean check = false;
		if (update.getMessage().getVideo() != null) {			
			hp.setField("DATA", new VoTooL().getJSONString(update.getMessage().getVideo()));
			hp.setField("TYPE_FILE", "V");
			hp.setField("FILE_ID", update.getMessage().getVideo().getFileId());
			check = true;

		} else if (update.getMessage().getPhoto() != null) {
			hp.setField("DATA", new VoTooL().getJSONString(update.getMessage().getPhoto()));
			hp.setField("FILE_ID",
					update.getMessage().getPhoto().get(update.getMessage().getPhoto().size()).getFileId());
			hp.setField("TYPE_FILE", "P");
			check = true;
		} else {
			hp.setField("DATA", new VoTooL().getJSONString(update.getMessage()));
			hp.setField("TYPE_FILE", "T");
			hp.setField("FILE_ID", "" + update.getMessage().getMessageId());
			check = true;
		}
		try {
			if (check) {
				hp.update(sqlinsert);
			}

		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			log.debug("====================================================================");
			TelegramBotSetup bot = new TelegramBotSetup();
			List<Map<String, Object>> li = bot.getList_File();
			for (Map map : li) {
				String Url = bot.getURL_File(ObjectUtils.toString(map.get("bot_username")), ObjectUtils.toString(map.get("file_id")));
				log.debug("=======================================================");
				log.debug("=======================================================");
				log.debug(Url);
				log.debug("=======================================================");
				log.debug("=======================================================");
				hp.clear();
				hp.setField("URL", Url);
				hp.setField("BATCH", this.getClass().getSimpleName());
				hp.setField("FILE_ID", ObjectUtils.toString(map.get("file_id")));
				hp.setField("DATA", ObjectUtils.toString(map.get("data")));
				hp.setField("SETUP", "N");
				if (hp.searchAndRetrieve("com.telegram.template.getfileIdfromdb").size() > 0) {
					hp.setField("SETUP", "Y");
					hp.update("com.telegram.template.updatetableData");
					hp.update("com.telegram.template.updatefileIdfromdb");
				} else {
					hp.setField("SETUP", "Y");
					hp.update("com.telegram.template.updatetableData");
					hp.update("com.telegram.template.insertfileIdfromdb");
				}

			}
			log.debug("====================================================================");
		}
		System.out.println(new VoTooL().getJSONString(update));

	}

	public void getFile(String FILE_URL) {

		try (BufferedInputStream in = new BufferedInputStream(new URL(FILE_URL).openStream());
				FileOutputStream fileOutputStream = new FileOutputStream("D:/abc.mp4")) {
			byte dataBuffer[] = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
				fileOutputStream.write(dataBuffer, 0, bytesRead);
			}
		} catch (IOException e) {
			// handle exception
		}

	}

	public static final String getURL_File(String botUsername, String FileId) {
		JdbcHelper hp = new JdbcHelper();
		hp.setField("ID", botUsername);
		List<Map<String, Object>> li = hp.searchAndRetrieve("com.telegram.template.findbotusername");
		String url = "";
		for (Map<String, Object> map : li) {
			ChatBotConfig chatbot = new ChatBotConfig();
			chatbot.setBotToken(ObjectUtils.toString(map.get("token")));
			chatbot.setBotUsername(ObjectUtils.toString(map.get("id")));
			try {
				url = chatbot.getFileID(FileId, chatbot);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
		return url;
	}

	public static List<Map<String, Object>> getList_File() {
		JdbcHelper hp = new JdbcHelper();
		List<Map<String, Object>> li = hp.searchAndRetrieve("com.telegram.template.selectfilefromdb");
		return li;
	}
	public List<Map<String, Object>> getList_File1() {
		JdbcHelper hp = new JdbcHelper();
		List<Map<String, Object>> li = hp.searchAndRetrieve("com.telegram.template.selectfilefromdb1");
		return li;
	}
	public static void main(String[] args) {
		String botUsername = "baotele_bot";
		String token = "5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0";

		// chatbot.setChatId("5717458324");
		// System.out.println(getURL_File("baotele_bot",
		// "BAACAgUAAxkDAAMpZBwGT6hHpfkdDEvPWX_p3_9QqVUAAjMLAAInzeFUSlH-i9hXLd0vBA"));
	//	System.out.println(getList_File());

		try {
			TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
			TelegramBotSetup bot = new TelegramBotSetup();
			bot.setBotUsername(botUsername);
			bot.setBotToken(token);
			botsApi.registerBot(bot);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}

		// Hoặc bạn có thể tạo một đối tượng TelegramLongPollingBot khác và gọi phương
		// thức onUpdateReceived trên đó.
		// TelegramBotSetup bot2 = new TelegramBotSetup(botUsername, token);
		// bot2.onUpdateReceived(new Update());
	}
	/*
	 * public static void main(String[] args) { TelegramBot bot = new
	 * TelegramBot("BOT_TOKEN");
	 * 
	 * // Register for updates bot.setUpdatesListener(updates -> { // ... process
	 * updates // return id of last processed update or confirm them all return
	 * UpdatesListener.CONFIRMED_UPDATES_ALL; // Create Exception Handler }, e -> {
	 * if (e.response() != null) { // got bad response from telegram
	 * e.response().errorCode(); e.response().description(); } else { // probably
	 * network error e.printStackTrace(); } });
	 * 
	 * // Send messages long chatId = update.message().chat().id(); SendResponse
	 * response = bot.execute(new SendMessage(chatId, "Hello!")); }
	 */
}

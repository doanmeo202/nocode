package com.telegram.core;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

public class TelegramBotManager {
	private static TelegramBotManager instance;
	private TelegramBotsApi botsApi;
	private boolean botActivated;

	private TelegramBotManager() {
		try {
			botsApi = new TelegramBotsApi(DefaultBotSession.class);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		botActivated = false;
	}

	public static TelegramBotManager getInstance() {
		if (instance == null) {
			synchronized (TelegramBotManager.class) {
				if (instance == null) {
					instance = new TelegramBotManager();
				}
			}
		}
		return instance;
	}

	public void registerBot(TelegramBotSetup bot) {
		if (!botActivated) {
			try {
				botsApi.registerBot(bot);
				botActivated = true;
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		TelegramBotSetup bot1 = new TelegramBotSetup();
		bot1.setBotUsername("botUsername1");
		bot1.setBotToken("botToken1");
		TelegramBotManager.getInstance().registerBot(bot1);
		TelegramBotSetup bot2 = new TelegramBotSetup();
		bot2.setBotUsername("botUsername2");
		bot2.setBotToken("botToken2");
		TelegramBotManager.getInstance().registerBot(bot2);
		// Đăng ký các bot khác (nếu có)

	}
}

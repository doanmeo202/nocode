package com.telegram.core;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.sql2o.Connection;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.GetFile.GetFileBuilder;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramBot;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import com.dano.web.app.aa.b0.helper.JdbcHelper;

public class ChatBotConfig extends TelegramLongPollingBot {
	private static final Logger log = Logger.getLogger(ChatBotConfig.class);

	private static String selectTokenbox = "com.telegram.template";
	private static String findchatid = "com.telegram.template.findchatid";
	private static String findroomchatid = "com.telegram.template.findgroup";
	private static String resigntoken = "com.telegram.template.resigntoken";
	private static String addUser = "com.telegram.template.adduser";
	private static String addGroup = "com.telegram.template.addgroup";
	private static String sendFile = "com.telegram.template.sendfile";

	static JdbcHelper helpme = new JdbcHelper();
	private String botUsername;
	private String botToken;
	private String chatId;
	private String fileName;
	private String fileId;
	private String fileSize;
	private String filePath;
	private String urlDownload;
	private String fileNameOld;
	private String inGroup;
	private String fileType;
	private String fileUniQueId;
	private String fileFK;
	private String userID;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFileFK() {
		return fileFK;
	}

	public void setFileFK(String fileFK) {
		this.fileFK = fileFK;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getUrlDownload() {
		return urlDownload;
	}

	public void setUrlDownload(String urlDownload) {
		this.urlDownload = urlDownload;
	}

	public String getFileNameOld() {
		return fileNameOld;
	}

	public void setFileNameOld(String fileNameOld) {
		this.fileNameOld = fileNameOld;
	}

	public String getInGroup() {
		return inGroup;
	}

	public void setInGroup(String inGroup) {
		this.inGroup = inGroup;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileUniQueId() {
		return fileUniQueId;
	}

	public void setFileUniQueId(String fileUniQueId) {
		this.fileUniQueId = fileUniQueId;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public void setBotUsername(String botUsername) {
		this.botUsername = botUsername;
	}

	public void setBotToken(String botToken) {
		this.botToken = botToken;
	}

	public String getBotUsername() {
		return botUsername;
	}

	public String getBotToken() {
		return botToken;
	}

	static {
		BasicConfigurator.configure();
		Properties properties = new Properties();
		properties.setProperty("log4j.rootCategory", "DEBUG, all");
		properties.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		properties.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		properties.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		// ===============================================
		properties.setProperty("log4j.appender.NotConsole", "org.apache.log4j.RollingFileAppender");
		properties.setProperty("log4j.appender.NotConsole.fileName", "D:\\Log\\ChatBotConfig.log");
		properties.setProperty("log4j.appender.NotConsole.maxFileSize", "20MB");
		PropertyConfigurator.configure(properties);
	}

	public void setUserToDB(Update update) {
		System.out.println(update);
		Map map = new HashMap();
		map.put("CHATID", ObjectUtils.toString(update.getMessage().getFrom().getId().toString()));
		Map room = new HashMap();
		room.put("GROUP_ID", ObjectUtils.toString(update.getMessage().getChat().getId().toString()));
		List li = helpme.getDataFromKey(findchatid, map);
		List li2 = helpme.getDataFromKey(findroomchatid, room);
		if (li2.size() < 1) {
			room.put("GROUP_TITLE", update.getMessage().getChat().getTitle());
			helpme.setDataFromKey(addGroup, room);
		}
		if (li.size() < 1) {
			Map mapadd = new HashMap();
			String CHATID = null;
			String USERNAME = null;
			String PHONE = null;
			String FROM_GROUP_BOT = null;
			try {
				CHATID = ObjectUtils.toString(update.getMessage().getFrom().getId().toString());
				USERNAME = ObjectUtils.toString(update.getMessage().getFrom().getUserName().toString());
				PHONE = ObjectUtils.toString(update.getMessage().getViaBot().getUserName());
				FROM_GROUP_BOT = ObjectUtils.toString(update.getMessage().getChatId());
			} catch (Exception e) {
				if ("".equals(USERNAME)) {
					USERNAME = "NOT FOUND";
				}
			}

			String GENDER = "";
			mapadd.put("CHATID", CHATID);
			mapadd.put("USERNAME", USERNAME);
			mapadd.put("PHONE", PHONE);
			mapadd.put("FROM_GROUP_BOT", FROM_GROUP_BOT);
			mapadd.put("IS_DISABLE", "0");
			helpme.setDataFromKey(addUser, mapadd);
		}
		System.out.println(update.getMessage().getChatId().toString());
	}

	public void onUpdateReceived(Update update) {
		setUserToDB(update);

	}

	public static void resignTokenBot(String token, String id) {
		Map mapadd = new HashMap();
		mapadd.put("TOKEN", token);
		mapadd.put("ID", id);
		helpme.setDataFromKey(resigntoken, mapadd);
	}

	public Map sendMessenger(String mess, String chatId) {
		Map map = new HashMap();
		SendMessage resp = new SendMessage();
		resp.setText(mess);
		resp.setChatId(chatId);
		map.put("returnCode", "0");
		try {
			execute(resp);
		} catch (Exception e) {
			map.put("returnCode", "-1");
			map.put("data", e.getCause());
			map.put("message", e.getMessage());
			// e.printStackTrace();
		}
		return map;
	}public Map sendMessenger(String mess) {
		Map map = new HashMap();
		SendMessage resp = new SendMessage();
		resp.setText(mess);
		resp.setChatId(this.getChatId());
		map.put("returnCode", "0");
		try {
			execute(resp);
		} catch (Exception e) {
			map.put("returnCode", "-1");
			map.put("data", e.getCause());
			map.put("message", e.getMessage());
			// e.printStackTrace();
		}
		return map;
	}
 
	public void sendVideo(String chatId) {
		try {
			SendVideo message = new SendVideo();
			File file = new File("C:\\Users\\0100644068\\Videos\\Captures\\Zalo 2023-02-09 17-19-36.mp4");
			InputFile fileIP = new InputFile();
			fileIP.setMedia(file);
			message.setVideo(fileIP);
			message.setChatId(chatId);
			execute(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    public String getFileID(String FileId,ChatBotConfig chatBot) throws TelegramApiException {
    	GetFile request = new GetFile(FileId);
    	String URL_DOWNLOAD = chatBot.execute(request).getFileUrl(chatBot.getBotToken());
		return URL_DOWNLOAD;
    	
    }
	public Message sendPhoto(BufferedImage buffImage, String chatId, ChatBotConfig chatBot) {
		Message mes = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Date date = new Date();
		long timestamp = date.getTime();
		try {
			ImageIO.write(buffImage, "png", os);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} // Passing: ​(RenderedImage im, String formatName, OutputStream output)
		InputStream is = new ByteArrayInputStream(os.toByteArray());
		InputFile input = new InputFile(is, "buffImage.png");
		SendPhoto message = new SendPhoto();
		message.setPhoto(input);
		message.setChatId(chatId);
		try {
			mes = execute(message);
			String FILE_PATH = null;
			String URL_DOWNLOAD = null;
			String ID = null;
			String FILE_NAME = null;
			String FILE_NAME_OLD = "FILE" + timestamp;
			for (int i = 0; i < mes.getPhoto().size(); i++) {
				GetFile request = new GetFile(mes.getPhoto().get(i).getFileId());
				URL_DOWNLOAD = chatBot.execute(request).getFileUrl(chatBot.getBotToken());
				FILE_PATH = chatBot.execute(request).getFilePath();
				String[] KEY_NAME = FILE_PATH.split("/");
				if (KEY_NAME.length > 0) {
					FILE_NAME = KEY_NAME[1];
				}
				ID = chatBot.execute(request).getFileId();
				System.out.println("FILE URL========================: "
						+ chatBot.execute(request).getFileUrl(chatBot.getBotToken()));
				System.out.println("FILE_PATH========================: " + chatBot.execute(request).getFilePath());
				System.out.println("FILE_ID========================: " + chatBot.execute(request).getFileId());
			}

			String FILE_TYPE = null;
			String IN_GROUP = null;
			String CHAT_ID = null;
			String FILE_UNIQUE_ID = null;
			String FILE_SIZE = null;

			try {
				IN_GROUP = mes.getChat().getId().toString();
			} catch (Exception e) {
				// TODO: handle exception
			}
			logSendFile(ID, FILE_NAME, FILE_TYPE, IN_GROUP, CHAT_ID, URL_DOWNLOAD, FILE_UNIQUE_ID, FILE_SIZE,
					FILE_PATH);
			// System.out.println(mes.getChat().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mes;
	}

//	public String getBotUsername() {
//		return "baotele_bot";
//	}
//
//	@Override
//	public String getBotToken() {
//		return "5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0";
//	}

	public static void messageToLogFile(Message message, String FILE_NAME_OLD) {
		String ID = null;
		String FILE_NAME = null;
		String FILE_TYPE = null;
		String IN_GROUP = null;
		String CHAT_ID = null;
		String URL_DOWNLOAD = null;
		String FILE_UNIQUE_ID = null;
		String FILE_SIZE = null;
		String FILE_PATH = null;

		try {
			ID = message.getPhoto().get(0).getFileId();
			FILE_NAME = message.getPhoto().get(0).getFilePath();
			FILE_PATH = message.getPhoto().get(0).getFilePath();
			CHAT_ID = message.getFrom().getId().toString();
			IN_GROUP = message.getChat().getId().toString();

		} catch (Exception e) {
			// TODO: handle exception
		}
		logSendFile(ID, FILE_NAME, FILE_TYPE, IN_GROUP, CHAT_ID, URL_DOWNLOAD, FILE_UNIQUE_ID, FILE_SIZE, FILE_PATH);

	}

	public static void logSendFile(String ID, String FILE_NAME, String FILE_TYPE, String IN_GROUP, String CHAT_ID,
			String URL_DOWNLOAD, String FILE_UNIQUE_ID, String FILE_SIZE, String FILE_PATH) {
		Map mapSendFile = new HashMap();
		// :ID, :FILE_NAME, :FILE_TYPE, :IN_GROUP, :CHAT_ID, :URL_DOWNLOAD
		// FILE_ID, FILE_NAME, FILE_TYPE, IN_GROUP, CHAT_ID, URL_DOWNLOAD,
		// FILE_UNIQUE_ID, FILE_SIZE, FILE_PATH
		mapSendFile.put("FILE_ID", ID);
		mapSendFile.put("FILE_NAME", FILE_NAME);
		mapSendFile.put("FILE_TYPE", FILE_TYPE);
		mapSendFile.put("IN_GROUP", IN_GROUP);
		mapSendFile.put("CHAT_ID", CHAT_ID);
		mapSendFile.put("URL_DOWNLOAD", URL_DOWNLOAD);
		mapSendFile.put("FILE_UNIQUE_ID", FILE_UNIQUE_ID);
		mapSendFile.put("FILE_SIZE", FILE_SIZE);
		mapSendFile.put("FILE_PATH", FILE_PATH);
		helpme.setDataFromKey(sendFile, mapSendFile);
	}

	public static void a() { // main121(String[] args) {
		// resignTokenBot("5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0",
		// "baotele_bot");
		// log.fatal(helpme.getDataFromKey(selectTokenbox, new HashMap()));
		try {
			TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
			ChatBotConfig baotapviet = new ChatBotConfig();
			ChatBotConfig baotapdoc = new ChatBotConfig();
			baotapviet.setBotToken("5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0");
			baotapviet.setBotUsername("baotele_bot");
//			
			baotapdoc.setBotToken("6012182824:AAEziZ7YSLsJ_zWimqmfp_diKakYaQXm29I");
			baotapdoc.setBotUsername("LogQrCodeBot");
			botsApi.registerBot(baotapviet);
			botsApi.registerBot(baotapdoc);

			String text = "【Test】  Bot Gửi Tin Nhắn Nè.";
			 baotapviet.sendMessenger(text + "Từ con báo tập viết ", "-1001636496421");
			// baotapdoc.sendMessenger(text + "Từ con báo tập đọc ", "-1001636496421");
			BufferedImage image;
			// baotapdoc.sendVideo("-1001636496421");
//			try {
//				image = ImageIO.read(new File("C:\\Users\\0100644068\\Documents\\Zalo Received Files\\0862124731.png"));
//				Message message= baotapdoc.sendPhoto(image,"5717458324",baotapdoc);
//				
//			
//						
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	         

	}

	public static String identifyFileTypeUsingFilesProbeContentType(final String fileName) {
		String fileType = "Undetermined";
		final File file = new File(fileName);
		try {
			fileType = Files.probeContentType(file.toPath());
		} catch (IOException ioException) {
			System.out.println(
					"ERROR: Unable to determine file type for " + fileName + " due to exception " + ioException);

		}
		return fileType;
	}

	public SendPhoto setParamSendPhoto(InputFile file, ChatBotConfig config) {
		SendPhoto sendPhoto = new SendPhoto();
		sendPhoto.setPhoto(file);
		sendPhoto.setChatId(config.getChatId());
		return sendPhoto;
	}

	public SendVideo setParamSendVideo(InputFile file, ChatBotConfig config) {
		SendVideo sendVideo = new SendVideo();
		sendVideo.setVideo(file);
		sendVideo.setChatId(config.getChatId());
		return sendVideo;
	}

	public SendDocument setParamSendDocument(InputFile file, ChatBotConfig config) {
		SendDocument sendDocument = new SendDocument();
		sendDocument.setDocument(file);
		sendDocument.setChatId(config.getChatId());
		return sendDocument;
	}

	public void saveDB(Map map, String table) throws Exception {
		ArrayList myKeyList = new ArrayList(map.keySet());
		String pram = "";
		String pramValue = "";
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) map.get(myKeyList.get(i));
			if (i == (myKeyList.size() - 1)) {

				pram += key;
				pramValue +="'"+value+"'";
			} else {
				pram += key + ",";
				pramValue += "'"+value+"', ";
			}
		}
		String SQL = "INSERT INTO " + table + " (" + pram + ") VALUES(" + pramValue + ")";
		System.out.println(SQL);
		JdbcHelper helper = new JdbcHelper();
		Connection con1 = null;
		try {
			con1 = helper.getConnection();
		} catch (ClassNotFoundException e) {
			con1.close();
			e.printStackTrace();
		}
		try {
			con1.createQuery(SQL).executeUpdate();
			con1.commit();
		} catch (Exception e) {
			con1.rollback();
			throw new Exception(e.getMessage());
		} finally {
			con1.close();
		}

	}
	public void logSendVideo(Message send, ChatBotConfig chatBot) throws Exception {
		Map map = new HashMap();
		map.put("FILENAME_OLD", chatBot.getFileNameOld());
		map.put("FILE_SIZE", chatBot.getFileSize());
		map.put("FILE_PATH", chatBot.getFilePath());
		map.put("CHAT_ID", chatBot.getChatId());
		map.put("FILE_TYPE", chatBot.getFileType());
		map.put("FILE_ID", send.getVideo().getFileId());
		map.put("FILE_NAME", send.getVideo().getFileName());
		map.put("IN_GROUP", send.getChat().getId().toString());
		map.put("FILE_FK", chatBot.getFileFK());
		GetFile request = new GetFile(send.getVideo().getFileId());
		try {
			map.put("URL_DOWNLOAD", chatBot.execute(request).getFileUrl(chatBot.getBotToken()));
		} catch (TelegramApiException e) {
			map.put("URL_DOWNLOAD", "https://api.telegram.org/bot"+chatBot.getBotToken()+"/getFile?file_id="+send.getVideo().getFileId());
		}
		map.put("FILE_UNIQUE_ID", "0");
		try {
			saveDB(map, "TELEGRAM_TEMPLATE_FILES");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	public void logSendDoccument(Message send, ChatBotConfig chatBot) throws Exception {
		Map map = new HashMap();
		map.put("FILENAME_OLD", chatBot.getFileNameOld());
		map.put("FILE_SIZE", chatBot.getFileSize());
		map.put("FILE_PATH", chatBot.getFilePath());
		map.put("CHAT_ID", chatBot.getChatId());
		map.put("FILE_TYPE", chatBot.getFileType());
		map.put("FILE_ID", send.getDocument().getFileId());
		map.put("FILE_NAME", send.getDocument().getFileName());
		map.put("IN_GROUP", send.getChat().getId().toString());
		map.put("FILE_FK", chatBot.getFileFK());
		GetFile request = new GetFile(send.getDocument().getFileId());
		try {
			map.put("URL_DOWNLOAD", chatBot.execute(request).getFileUrl(chatBot.getBotToken()));
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		map.put("FILE_UNIQUE_ID", "0");
		try {
			saveDB(map, "TELEGRAM_TEMPLATE_FILES");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	public void logSendPhoto(Message send, ChatBotConfig chatBot) throws Exception {
		Map map = new HashMap();
		map.put("FILENAME_OLD", chatBot.getFileNameOld());
		map.put("FILE_SIZE", chatBot.getFileSize());
		map.put("FILE_PATH", chatBot.getFilePath());
		map.put("CHAT_ID", chatBot.getChatId());
		map.put("FILE_TYPE", chatBot.getFileType());
		map.put("FILE_FK", chatBot.getFileFK());
		int count = send.getPhoto().size();
		for (int i = 0; i < count; i++) {
			GetFile request = new GetFile(send.getPhoto().get(i).getFileId());
		
			map.put("FILE_ID", send.getPhoto().get(i).getFileId());
			map.put("FILE_NAME", "file_"+i+"jpg");
			map.put("IN_GROUP", send.getChat().getId().toString());
			try {
				map.put("URL_DOWNLOAD", chatBot.execute(request).getFileUrl(chatBot.getBotToken()));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				throw e;
			}
			map.put("FILE_UNIQUE_ID", "0");
			try {
				saveDB(map, "TELEGRAM_TEMPLATE_FILES");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}

		}
	}

	public void SenderMutipartFile(File file, ChatBotConfig config) throws Exception {
		String fileType = ObjectUtils.toString(identifyFileTypeUsingFilesProbeContentType(file.getName()),"");
		config.setFileNameOld(file.getName());
		config.setFileSize("" + file.length());
		config.setFilePath(file.getAbsolutePath());
		config.setFileType(fileType);
		Date date = new Date();
		long timestamp = date.getTime();
		config.setFileFK(""+timestamp);
		InputStream inputStream = null;
		InputFile input = null;
		Map listFileUser=new HashMap();
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw e;
		}
		input = new InputFile(inputStream, file.getName());
		Message send = new Message();
		try {
			if (fileType.startsWith("image")) {
				SendPhoto sendPhoto = config.setParamSendPhoto(input, config);
				send = execute(sendPhoto);
				logSendPhoto(send,config);
			} else if (fileType.startsWith("video")) {
				SendVideo sendVideo = config.setParamSendVideo(input, config);
				send = execute(sendVideo);
				logSendVideo(send,config);
			} else if (fileType.startsWith("app")) {
				SendDocument sendDocument = config.setParamSendDocument(input, config);
				send = execute(sendDocument);
				logSendDoccument(send,config);
			}else {
				SendDocument sendDocument = config.setParamSendDocument(input, config);
				send = execute(sendDocument);
				logSendDoccument(send,config);
				//throw new Exception("file này chưa được hỗ trợ tải lên!");
			}
			listFileUser.put("FILE_NAME", file.getName());
			listFileUser.put("FILE_FK", config.getFileFK());
			saveDB(listFileUser, "TELEGRAM_TEMPLATE_LIST_FILE");
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public static void main(String[] args) {
		File file = new File("C:\\Users\\0100644068\\Downloads\\Upload_demo.jpeg");
		ChatBotConfig chatbot = new ChatBotConfig();

		chatbot.setBotToken("5833973174:AAHIXRHjHnbAmeowY9qe2mIuQapIeIXHfr0");
		chatbot.setBotUsername("baotele_bot");
		chatbot.setChatId("5717458324");
		try {
			chatbot.sendMessenger("helo");
			//chatbot.SenderMutipartFile(file, chatbot);
			System.out.println(chatbot.getFileID("AgACAgUAAxkDAAMxZCO83yM3PDzvN98kkqyDa7EF7m4AAtm4MRv7thlVqSZxuPSOiVgBAAMCAAN3AAMvBA", chatbot));
		} catch (Exception e) {
		System.out.println("err: "+e.getMessage());

		}
	}
	//ID_AUTO INT AUTO_INCREMENT
//	public void onUpdateReceived(Update update) {
//		// TODO Auto-generated method stub
//		
//	}
}

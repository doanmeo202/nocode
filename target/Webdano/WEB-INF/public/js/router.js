var proxy="http://localhost:8080/Webdano/api/";
var socketProxy="localhost:8080/Webdano";
var apiGetMenu = proxy+`getMenu?WEB_USED=Wadmin`;
function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $.when($.ajax({
        url: apiGetMenu,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
    })).then(function (success, error) {
        var routes = JSON.stringify(success.data[0].menu);
        
        if (routes) {
            sessionStorage.setItem("menu", routes);
            JSON.parse(routes).forEach(element => {
              if(element.template_url!=null){
                $stateProvider.state(element.slug, {
                    url: element.slug,
                    templateUrl: element.template_url
                });
              }else{
                $stateProvider
                .state(element.slug, {
                    url: element.slug,
                    templateUrl: "view/404.html"
                })
              }
            });
        }
    })
    $stateProvider
        .state('/', {
            url: "/",
            templateUrl: "view/dashboad.html"
        })
}

app.config(config)

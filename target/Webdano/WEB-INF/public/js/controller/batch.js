function batchCtrl($scope, $state, $http) {
  var jobList = [];
  var webSocket = new WebSocket(
    "ws://"+socketProxy+"/websocket-endpoint"
  );
  webSocket.onopen = function () {
    $.ajax({
      url: proxy + `getListBatchAll?PARAM=noParam`,
      type: "POST",
      cache: false,
      contentType: false,
      processData: false,
    })
      .done(function (success) {
        jobList = success.data[0].batchInfo;
        $scope.$apply(function () {
          $scope.jobs = jobList;
        });
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseJSON.returnMess);
      });
  };
  webSocket.onmessage = function (event) {
    var message = JSON.parse(event.data).batchInfo;
    var jobId = message.id;
    var jobInfo = message.job_info;
    // Tìm đối tượng với id tương ứng và cập nhật trường job_info
    jobList.forEach(function (job) {
      if (job.id === jobId) {
        job.job_info = jobInfo;
        job.next_fire_time = message.next_fire_time;
        job.trigger_cron_expression=message.trigger_cron_expression;
        job.job_description=message.job_description;
      }
    });

    $scope.$apply(function () {
      $scope.jobs = jobList;
    });
  };
  $scope.setupBatch=function(){
    var batchClassName=document.getElementById("batchClassName");
    var batchTime=document.getElementById("batchTime");
    var batchDescription=document.getElementById("batchDescription");
    var batchId=document.getElementById("batchId");
    var formData =new FormData();
    //formData.append("JOB_NAME",this.props.job.job_name);  
    formData.append("BACTH",batchClassName.value);  
    formData.append("TIMES",batchTime.value);  
    formData.append("DESCRIPTION",batchDescription.value);
    $.ajax({
        url: proxy + "excuteBatch",
        type: 'POST',
        cache: false,
        data: formData,
        contentType: false,
        processData: false
    }).done(function (success) {
        console.log(success);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseJSON.returnMess);
    });
  }



  // Tạo WebSocket và thiết lập event listener
}
app.controller("batchCtrl", batchCtrl);
function getCronStringFromText(text) {
    var res = document.getElementById('textResutl');
    var formData = new FormData();
    formData.append('LANGUAGE', "en");
    formData.append('TEXT', text);
    $.when($.ajax({
        url: proxy + 'translate',
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false

    })).then(function (data, textStatus, jqXHR) {
    
    res.value = getCronString(data.data[0].data);
    })
}
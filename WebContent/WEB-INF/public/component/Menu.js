class Menu extends React.Component {
  state = {
    clients: [],
    activeSlug: null,
  };
  handleItemClick = (slug) => {
    this.setState({ activeSlug: slug });
  };
  componentDidMount() {
    const { menu } = this.props;
    this.setState({ clients: menu });
  }
  render() {
      const { clients, activeSlug } = this.state;
    if (clients) {
      return (
        <ul className="metismenu list-unstyled mm-show" id="side-menu">
          <li className="menu-title">Menu</li>
         
          {clients.map((client) => {
            const slug = client.slug.toString().replace('/', '');
            const isActive = slug === activeSlug;
            const className = isActive
              ? ' active'
              : '';

            return (
              <li  className={className} key={client.slug}>
                <a
                  id={slug}
                  className=" waves-effect"
                  onClick={() => this.handleItemClick(slug)}
                  aria-expanded="false"
                  href={'#!' + client.slug}
                >
                  <i className={client.class_icon}></i>{' '}
                  <span>{client.name}</span>
                </a>
              </li>
            );
          })}
        </ul>
      );
    } else {
      return 'No data';
    
    }
  }
}

const MyComponent = ({ clients }) => {
    const [activeSlug, setActiveSlug] = React.useState(null);
    return (
        React.createElement('ul', null,
            clients.map((client) =>
                React.createElement('li', { className: 'sidebar-item', key: client.slug },
                    React.createElement('a', {
                        className: `sidebar-link waves-effect waves-dark sidebar-link ${client.slug === activeSlug ? 'active' : ''}`,
                        'aria-expanded': 'false',
                        href: '#!' + client.slug,
                        onClick: () => setActiveSlug(client.slug)
                    },
                        React.createElement('i', { className: client.class_icon }),
                        React.createElement('span', { className: 'hide-menu' }, client.name)
                    )
                )
            )
        )
    );
};


function Menu1(props) {
    const { useState, useEffect } = React;
    const [clients, setClients] = useState([]);
    useEffect(() => {
        setClients(props.menu);
    }, [props.menu]);
    const [showItem, setShowItem] = useState(true);
    if (clients) {
        return (<ul id="sidebarnav">
             {showItem && (
          <li>
            <div className="user-profile d-flex no-block dropdown m-t-20">
              <div className="user-content -menu m-l-10"></div>
            </div>
          </li>
        )}
            <li>
                <div className="user-profile d-flex no-block dropdown m-t-20">
                    <div className="user-content -menu m-l-10"></div></div>
            </li></ul>)
    }
}

class Carousel extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         menu: [],
    //         socical: []
    //     };
    // }
    // componentDidMount() {
    //     // $.ajax({
    //     //     url: urlMenu,
    //     //     type: 'GET',
    //     //     contentType: false,
    //     //     cache: false,
    //     //     processData: false,
    //     //     success: data => {
    //     //         this.setState({ menu: JSON.parse(data).menu });
    //     //     }
    //     // });
    // }


    render() {
        const liStyle = {
            backgroundImage: '/images/img_bg_1.jpg)',
            // display:"block"
          };
        return (<aside id="" className="js-fullheight">
            <div className="flexslider js-fullheight">
                <ul className="slides">
                    <li style={liStyle}>
                        <div className="overlay"></div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text">
                                    <div className="slider-text-inner">
                                        <div className="desc">
                                            <h1>An Inspiring Built Space</h1>
                                            <h2>100% html5 bootstrap templates Made by <a href="https://colorlib.com/" target="_blank">colorlib.com</a></h2>
                                            <p><a className="btn btn-primary btn-learn">View Project <i className="icon-arrow-right3"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </aside>
        );
    }
}
function Carousel1() {
    const html ="<aside id=\"colorlib-hero\" class=\"js-fullheight\">"
    + "				< div class=\"flexslider js-fullheight\">"
    + "					<ul class=\"slides\">"
    + "				   	<li style=\"background-image: url(images/img_bg_1.jpg);\">"
    + "				   		<div class=\"overlay\"></div>"
    + "				   		<div class=\"container-fluid\">"
    + "				   			<div class=\"row\">"
    + "					   			<div class=\"col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text\">"
    + "					   				<div class=\"slider-text-inner\">"
    + "					   					<div class=\"desc\">"
    + "						   					<h1>An Inspiring Built Space</h1>"
    + "						   					<h2>100% html5 bootstrap templates Made by <a href=\"https://colorlib.com/\" target=\"_blank\">colorlib.com</a></h2>"
    + "												<p><a class=\"btn btn-primary btn-learn\">View Project <i class=\"icon-arrow-right3\"></i></a></p>"
    + "											</div>"
    + "					   				</div>"
    + "					   			</div>"
    + "					   		</div>"
    + "				   		</div>"
    + "				   	</li>"
    + "				  	</ul>"
    + "			  	</div></aside>";
  
    return <div dangerouslySetInnerHTML={{ __html: html }} />;
  }
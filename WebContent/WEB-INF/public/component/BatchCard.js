class BatchCard extends React.Component {
    state = {
      jobs: [],
      jobStatus: {
        Kill: "danger",
        Hold: "warning",
        Free: "primary",
        Done: "success",
        Eror: "danger",
        Order: "primary",
      },
    };
    loadFormData=()=>{
        var batchClassName=document.getElementById("batchClassName");
        var batchTime=document.getElementById("batchTime");
        var batchDescription=document.getElementById("batchDescription");
        var batchId=document.getElementById("batchId");
        batchClassName.value=this.props.job.job_class;
        batchTime.value=this.props.job.trigger_cron_expression;
        batchDescription.value=this.props.job.job_description;
        batchId.value=this.props.job.job_name;
    }
    handleActionClick = (action) => {
        console.log(`Action ${action} clicked for job ${this.props.job.job_name}`);
        var formData =new FormData();
        formData.append("JOB_NAME",this.props.job.job_name);  
        formData.append("BACTH",this.props.job.job_class);  
        formData.append("TIMES",this.props.job.trigger_cron_expression);  
        formData.append("DESCRIPTION",this.props.job.job_description);  
        
        $.ajax({
            url: proxy + action,
            type: 'POST',
            cache: false,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (success) {
            console.log(success);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseJSON.returnMess);
        });
      }
    render() {
      return (
        <div className="col-xl-4 col-sm-6">
          <div className="card task-box">
            <div className="card-body">
              <div className="float-right ml-2">
                <span
                  className={`badge badge-pill badge-soft-${this.state.jobStatus[this.props.job.job_info]} font-size-12`}
                >
                  {this.props.job.job_info}
                </span>
              </div>
              <div>
                <h5 className="font-size-15">
                  <button className={`btn btn-${this.state.jobStatus[this.props.job.job_info]}`} onClick={() => this.loadFormData()}>
                    {this.props.job.job_name}
                    </button>
                </h5>
                <p className="text-muted mb-4">
                  {this.props.job.trigger_cron_expression}
                </p>
                <p className="text-muted mb-4">
                  {this.props.job.next_fire_time}
                </p>
                <p className="text-muted mb-4">
                  {this.props.job.job_description}
                </p>
              </div>
  
              <div className="team float">
                <button className="btn btn-warning w-md" onClick={() => this.handleActionClick('holdJob')} >Hold</button>
                <button className="btn btn-danger w-md"  onClick={() => this.handleActionClick('killJob')}>Kill</button>
                <button className="btn btn-info w-md" onClick={() => this.handleActionClick('freeJob')}>Free</button>
                <button className="btn btn-success w-md" onClick={() => this.handleActionClick('excuteBatch')}>Start</button>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
  class BatchCardList extends React.Component {
    render() {
      const { jobs } = this.props;
      const jobStatus = {
        Kill: "danger",
        Hold: "warning",
        Free: "primary",
        Done: "success",
      };
  
      return (
        <div className="row">
          {jobs.map((job) => (
            <BatchCard key={job.id} job={job} jobStatus={jobStatus} />
          ))}
        </div>
      );
    }
  }
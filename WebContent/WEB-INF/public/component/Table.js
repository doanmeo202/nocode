
class Table extends React.Component {
    state = {
        currentPage: 1,
        data: [],
        rowsPerPage: 10,
        theader: [],
        dataAll: [],
    };

    componentDidMount() {
        this.setState({
            data: this.props.data,
            dataAll:this.props.dataAll,
        });

    }

    handlePageChange = (newPage) => {
        this.setState({ currentPage: newPage });
    };
    changeData = (row) => {
        const { dataAll} = this.state;
        var arr=[];
        var list=dataAll;
        var result=_.filter(list,{api_key:row.api_key});
        this.setState({ data: result });
        const event = new CustomEvent('tableApi', { detail: this.state.data });
        document.dispatchEvent(event);
    };
    render() {
        const { currentPage, data, rowsPerPage } = this.state;
        const totalPages = Math.ceil(data.length / rowsPerPage);
        const startIndex = (currentPage - 1) * rowsPerPage;
        const endIndex = startIndex + rowsPerPage;
        const currentData =data.slice(startIndex, endIndex);
    
        return (
            <div className="table-responsive">
                <table className="table v-middle">
                    <thead>
                        <tr className="bg-light">
                            {_.keys(currentData[0]).map(key => (
                                <th className="border-top-0">{key.toString().toUpperCase()}</th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {currentData.map(row => (
                            <tr key={row.api_key} onClick={() => this.changeData(row)}>
                                {Object.values(row).map(value => (
                                    <td>{value}</td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div>
                    {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                        (pageNumber) => {
                            const isActive = pageNumber === currentPage;
                            const className = isActive ? 'btn btn-purple active' : 'btn btn-success';
                            return (
                                <button
                                    className={className}
                                    key={pageNumber}
                                    onClick={() => this.handlePageChange(pageNumber)}
                                >
                                    {pageNumber}
                                </button>
                            );
                        }
                    )}
                </div>
            </div>
        );
    }

}


class MyComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }}

        handleItemClick(apiKey) {
            const selectedData = this.state.data.getDTZSAP03.find(
                item => item.api_key === apiKey
            );
            this.setState({ selectedData });
        }

        render() {
            return (
                <div>
                    <h2>getDTZSAP01</h2>
                    <ul>
                        {this.state.data.getDTZSAP01.map(item => (
                            <li key={item.api_key} onClick={() => this.handleItemClick(item.api_key)}>
                                {item.description}
                            </li>
                        ))}
                    </ul>
                    <h2>getDTZSAP02</h2>
                    <ul>
                        {this.state.data.getDTZSAP02.map(item => (
                            <li key={item.api_key} onClick={() => this.handleItemClick(item.api_key)}>
                                {item.remark}
                            </li>
                        ))}
                    </ul>
                    {
                        this.state.selectedData && (
                            <>
                                <h2>Selected Data</h2>
                                <p>{this.state.selectedData.remark}</p>
                            </>
                        )
                    }
                </div >
            );
        }
    }


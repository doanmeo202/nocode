<%-- 
	Author(s): 0100582956 LUUTUANCUONG
	Date: 20220606  
	Description: JSP process message (diaglog, alert, toast,... for info, error, success)
--%>
<%
	String htmlBase = request.getContextPath() + "";
String cssBase = request.getContextPath() + "/css";
String js = request.getContextPath() + "/js";
String font = request.getContextPath() + "/fonts";

pageContext.setAttribute("htmlBase", htmlBase);
pageContext.setAttribute("cssBase", cssBase);
pageContext.setAttribute("js", js);
pageContext.setAttribute("font", font);
%>

<!-- Begin import resource -->
<link rel="stylesheet" href="<%=cssBase%>/bootstrap.min.css">
<link rel="stylesheet" href="<%=cssBase%>/all.css">
<link rel="stylesheet" href="<%=cssBase%>/dashboard.css">
<script type="text/javascript" src="<%=js%>/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=js%>/vendor_holder.min.js"></script>
<script type="text/javascript" src="<%=js%>/bootstrap.min.js"></script>

<!-- <script type="text/javascript" src="/html/CM/js/MsgJSUtils.js?v=20220921"></script> -->

<!-- End import resource -->

<!-- 
WARNING: include file='/html/CM/header.jsp' before MsgFEUtils.jsp
 -->

<!-- Begin modal dialog -->
<div>
	<!-- Modal dialog msg info -->
	<div class="modal fade" tabindex="-1" id="_modal-dialog-msgfe-info"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content border border-info">
				<div class="modal-header bg-info text-light">
					<i class="fas fa-exclamation-circle"> INFO</i>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p id="_modal-body-msgfe-info" class="text-break">Modal body
						text goes here.</p>
				</div>

			</div>
		</div>
	</div>
	<!-- <button type="button" id="_modal-action-msgfe-info" class="invisible" data-toggle="modal" data-target="#_modal-dialog-msgfe-info"></button> -->

	<!-- Modal dialog confirm -->
	<div class="modal fade" tabindex="-1" id="_modal-dialog-msgfe-confirm"
		aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content border border-primary">
				<div class="modal-header bg-primary text-light">
					<i class="fas fa-check"> COFIRM</i>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p id="_modal-body-msgfe-confirm" class="text-break">Modal body
						text goes here.</p>
				</div>
				<div class="modal-footer">
					<button id="_btn-ok-msgfe-confirm" type="button"
						class="btn btn-primary" data-dismiss="modal">OK</button>
					<button id="_btn-cancel-msgfe-confirm" type="button"
						class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal dialog msg error -->
	<div class="modal fade" tabindex="-1" id="_modal-dialog-msgfe-error"
		aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content border border-danger">
				<div class="modal-header bg-danger text-light">
					<i class="fas fa-times-circle"> ERROR</i>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p id="_modal-body-msgfe-error" class="text-break">Modal body
						text goes here.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- <button type="button" id="_modal-action-msgfe-error" class="invisible" data-toggle="modal" data-target="#_modal-dialog-msgfe-error"></button> -->

	<!-- Modal dialog msg success -->
	<div class="modal fade" tabindex="-1" id="_modal-dialog-msgfe-success"
		aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content border border-success">
				<div class="modal-header bg-success text-light">
					<i class="fas fa-check-circle fa-lg"> SUCCESS</i>
				</div>
				<div class="modal-body">
					<p id="_modal-body-msgfe-success" class="text-break">Modal body
						text goes here.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- <button type="button" id="_modal-action-msgfe-success" class="invisible" data-toggle="modal" data-target="#_modal-dialog-msgfe-success"></button> -->
</div>
<!-- End modal dialog -->

<!-- Begin notify fixed bottom -->
<div class="fixed-bottom p-2">
	<!-- Begin toast fixed bottom right -->
	<div class="d-flex justify-content-end">
		<!-- Toast msg info -->
		<div class="toast border border-info fade hide"
			id="_toast-popup-msgfe-info" role="alert" data-autohide="true">
			<div class="toast-header">
				<strong class="mr-auto text-info">Information</strong>
				<button type="button" class="ml-2 close" data-dismiss="toast"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="toast-body text-break" id="_toast-body-msgfe-info"></div>
		</div>

		<!-- Toast msg success-->
		<div class="toast border border-success  fade hide"
			id="_toast-popup-msgfe-success" data-autohide="true" role="alert">
			<div class="toast-header">
				<strong class="mr-auto text-success">Success</strong>
				<button type="button" class="ml-2 close" data-dismiss="toast">&times</button>
			</div>
			<div class="toast-body text-break" id="_toast-body-msgfe-success"></div>
		</div>

		<!-- Toast msg error -->
		<div class="toast border border-danger fade hide"
			id="_toast-popup-msgfe-error" role="alert" data-autohide="true">
			<div class="toast-header">
				<strong class="mr-auto text-danger">Error</strong>
				<button type="button" class="ml-2 close" data-dismiss="toast">&times</button>
			</div>
			<div class="toast-body text-break" id="_toast-body-msgfe-error"></div>
		</div>
	</div>
	<!-- End toast fixed bottom right -->

	<!-- Begin alert panel -->
	<!-- <div class="alert alert-success alert-dismissible fade hide" id="_alert-panel-msgfe-success" role="alert">
		<p class="m-0" id="_alert-body-msgfe-success"></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
	</div>
	
	<div class="alert alert-danger alert-dismissible fade hide" data-autohide="true" data-delay="5000" id="_alert-panel-msgfe-error" role="alert">
		<p class="m-0 alert-heading" id="_alert-body-msgfe-error"></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
	</div>
	
	<div class="alert alert-info alert-dismissible fade hide" id="_alert-panel-msgfe-info" role="alert">
		<p class="m-0" id="_alert-body-msgfe-info"></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
	</div> -->
	<!-- End alert panel -->

</div>
<!-- End notify fixed bottom -->

<!-- Begin front loading -->
<div id="_front-loading" style="display: none;">
	<div class="fixed-top d-flex justify-content-center vh-100">
		<div class="align-self-center">
			<div class="spinner-grow text-secondary" role="status"></div>
		</div>
	</div>
	<div class="fixed-top d-flex justify-content-center vh-100">
		<div class="align-self-center">
			<div class="spinner-border text-success" role="status"
				style="width: 3rem; height: 3rem;"></div>
		</div>
		<div class="modal-backdrop fade show"></div>
		<!-- <div class="fixed-top h-100 bg-secondary" style="opacity: 0.3;"></div> -->
	</div>
</div>
<!-- End front loading -->

<!-- Begin JS for MsgFEUtils -->
<script type="text/javascript">
	var MsgFEUtils = {};
	
	MsgFEUtils._PREFIX_MODAL_ACTION ="_modal-action-msgfe";
	MsgFEUtils._PREFIX_MODAL = "_modal-dialog-msgfe";
	MsgFEUtils._PREFIX_MODAL_BODY = "_modal-body-msgfe";
	
	MsgFEUtils._PREFIX_TOAST = "_toast-popup-msgfe";
	MsgFEUtils._PREFIX_TOAST_BODY = "_toast-body-msgfe";
	
	MsgFEUtils._PREFIX_ALERT = "_alert-panel-msgfe";
	MsgFEUtils._PREFIX_ALERT_BODY = "_alert-body-msgfe";
	
	MsgFEUtils.INFO = "-info";
	MsgFEUtils.ERROR = "-error";
	MsgFEUtils.SUCCESS = "-success";
	MsgFEUtils.CONFIRM = "-confirm";
	
	MsgFEUtils.showDialog = function(type, msg) {
		let msgFinal = formatMsg(msg);
		$(('#' + MsgFEUtils._PREFIX_MODAL_BODY + type)).html(msgFinal);
		$(('#' + MsgFEUtils._PREFIX_MODAL + type)).modal('show');
	}
	MsgFEUtils.showConfirm = function(callback) {

	}
	
	MsgFEUtils.showToast = function(type, msg, delay) {
		let msgFinal = formatMsg(msg);
		$(('#' + MsgFEUtils._PREFIX_TOAST_BODY + type)).html(msgFinal);
		$(('#' + MsgFEUtils._PREFIX_TOAST + type)).data('delay', delay);
		$(('#' + MsgFEUtils._PREFIX_TOAST + type)).toast('show');
	}
	
	/*MsgFEUtils.showAlert = function(type, msg) {
	$(('#' + MsgFEUtils._PREFIX_ALERT_BODY + type)).html(msg);
	$(('#' + MsgFEUtils._PREFIX_ALERT + type)).toast('show');
	$(('#' + MsgFEUtils._PREFIX_ALERT + type)).alert();
	}*/

	const formatMsg = msg => {
		msg=msg||"Can't get message response !";
		return msg.replace(/(?:\r\n|\\r\\n|\r|\\r|\n|\\n)/g,'<br>');
	}
	
	MsgFEUtils.showFrontLoading = function() {
	document.getElementById("_front-loading").style.display = "";
	}
	
	MsgFEUtils.hideFrontLoading = function() {
	document.getElementById("_front-loading").style.display = "none";
	}
</script>
<!-- End JS for MsgFEUtils -->

<!-- Begin JS run first to show notify -->
<script type="text/javascript">
	
	function displayMessage() {
		let returnCode = "${ErrMsg.getReturnCode()}";
		let msgDescs = "${ErrMsg.getDisplayMsgDescs()}";
		if(msgDescs!=null && msgDescs.trim().length>0) {
			let msgFinal = "*** Code: " + returnCode + "\n *** Message:\n" + msgDescs;
			if("0" == returnCode) {
				MsgFEUtils.showDialog(MsgFEUtils.SUCCESS, msgFinal);
			} else {
				MsgFEUtils.showDialog(MsgFEUtils.ERROR, msgFinal);
			}
		}
	}
</script>
<!-- End JS run first to show notify -->

<!-- Begin JS TableGen -->
<script>
	const TableGenUtils = {
		// set param pattern
		setParam: (oriText, data) => {
			// detect param
			let VAR_PAT = /{{(?<paramCBK>[^ }]+)}}/g;
			let listParam = [...(oriText||'').matchAll(VAR_PAT)];
			for(let idx in listParam) {
				let oriParam = listParam[idx][0];
				let paramName = listParam[idx].groups['paramCBK'];
				oriText= oriText.replaceAll(oriParam, data[paramName]||'');
			}
            return oriText;
		},
		setAttrs: (elm, attrs, data) => {
			// set attribute for this row (tr tag)
			for(let prop in attrs) {
				elm.setAttribute(prop, TableGenUtils.setParam(attrs[prop], data));
			}
			return elm;
		},
		setProps: (elm, props, data) => {
			// set attribute for this row (tr tag)
			for(let p in props) {
				elm[p]= TableGenUtils.setParam(props[p], data);
			}
			return elm;
		},
		rowData: (data, index, configCols, configRowAttrs) => { // create row data
			let row = document.createElement("tr");
			configCols.forEach((item) => {
				row.append(TableGenUtils.colRender(data, index, item));
			});
			TableGenUtils.setAttrs(row, configRowAttrs, data);
			return row;
		},
		colRender: (data, index, configCol) => { // create column
			let colElm = document.createElement("td");
			if(configCol.render) { // override method render, return element self define
				let elm = configCol.render(data, index, configCol);
				if(elm) {
					if(elm.nodeType) {
						colElm.appendChild(elm);
					} else {
						colElm.innerHTML = elm;
					}
				}
			} else { // render cols default config
				colElm.innerText = data[configCol.data_field];		
			}
			TableGenUtils.setAttrs(colElm, configCol.attrs, data);
			return colElm;
		},
		create: (id, config) => {
			let tableGen = $("#" + id);
			let theadGen = $("#" + id + " thead");
			let tbodyGen = $("#" + id + " tbody");
		
			// create head column
			const rowHeadRender = (thData, configCols) => {
				let rowHead = document.createElement("tr");
				for(let idx in configCols) {
					let colElm = document.createElement("th");
					colElm.innerText = configCols[idx].title;
					TableGenUtils.setAttrs(colElm, configCols[idx].th_attrs, thData);
					rowHead.appendChild(colElm);
				}
				return rowHead;
			}

			const noData = (noti, render) => {
				let htmlNoData = "<tr><td align='center'>" + (noti||'--') + "</td></tr>";
				if(render) {
					theadGen.append(render());
					return;
				}
				theadGen.append(htmlNoData);
			}
			if(tableGen.length && theadGen && tbodyGen) {
				if(config.data && config.data.length>0) {
					// start run main render table
					if(theadGen.find("tr").length ==0) {
						theadGen.append(rowHeadRender({}, config.columns)); // step 1: create and append row header
					}
					for(let i=0; i<config.data.length; i++) { // step 2: create and append col, row data for table > tbody
						tbodyGen.append(TableGenUtils.rowData(config.data[i], (i+1), config.columns, config.rows_attrs));
					}
				} else if(config.no_data) {
					noData(config.no_data.noti, config.no_data.render);
				} else { noData("NO DATA", null); }
			} else {
				MsgFEUtils.showToast(MsgFEUtils.ERROR, "Create data table failed !", 5000);
			}
		}
	};
</script>
<!-- End JS TableGen -->

<!-- Begin JS View Show -->
<script>
	const ViewRole = {
		view: (roles, rolesCheck) => {
			roles = JSON.parse(roles);
			rolesCheck = JSON.parse(rolesCheck);
			let listElm = $("body [data-viewrole]");
			listElm.attr("hidden", true); // hide all element check show
			for(let elm of listElm) { // for each elm need check role
				let viewName = elm.getAttribute("data-viewrole");
				let viewFuncRole = rolesCheck[viewName]; // get list role of function by viewrole
				if(viewFuncRole && viewFuncRole.length>0) { // function need role
					let isShow = false;
					for(role of viewFuncRole) { // check role
						if(roles[role]) {
							$(elm).attr("hidden", false);
							isShow = true;
							break; // break when have role, to next element
						}
					}
					if(!isShow) { elm.remove(); } // remove element if have role this function
				} else { // function dont need role
					$(elm).attr("hidden", false);
				}
			}
		}
	}
</script>
<!-- End JS View Show -->
<!-- Begin JS menu tree -->
<script>
$(function () {
	  'use strict'

	  $('[data-toggle="offcanvas"]').on('click', function () {
	    $('.offcanvas-collapse').toggleClass('open')
	  })
	})
</script>	
<script type="text/javascript" src="<%=js%>/dano.js"></script>

<!-- End JS menu tree -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Chi tiết dữ liệu</title>

<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	rel="stylesheet">

</head>

<body>
	<hr>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
				<!--left col-->
				<%@include file="../CM/MenuFE.jsp"%>
			</div>
			<!--/col-3-->
			<div class="col-sm-9">
				<div class="container-fluid">
					<div class="row">
						<section class="col-12">
							<table class="rtable" style="width: 100%;">
								<thead>
									<tr>
										<th class="left">Xem chi tiết dữ liệu đã tải lên</th>
										<th class="right">Mã Trang: APAA_0100_VIEW</th>
									</tr>

								</thead>

							</table>
						</section>

					</div>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Danh sách dữ liệu câu trả lời đúng</span>
					</div>
					<div class="form-group">
						<div align="center" class="border shadow p-2">
							<div class="table-responsive">
								<table class="table table-sm table-hover">
									<thead>
										<tr>
											<th>Câu trả lời</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="i" items="${getDataAnsDetail}">
											<tr>
												<td>${i.true_ans}</td>
											</tr>
											<p>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Danh sách dữ liệu tải lên</span>
					</div>
					<div class="form-group">
						<div align="center" class="border shadow p-2">
							<div class="table-responsive">
								<table class="table table-sm table-hover">
									<thead>
										<tr>
											<th>Mã lô</th>
											<th>Thời gian upload</th>
											<th>Tên</th>
											<th>Ngày Sinh</th>
											<th>ĐỊA CHỈ</th>
											<th>CCCD</th>
											<th>HTX</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="i" items="${getDataUploadDetail}">
											<tr>
												<td>${i.id_batch}</td>
												<td>${i.time_update}</td>
												<td>${i.col1}</td>
												<td>${i.col2}</td>
												<td>${i.col3}</td>
												<td>${i.col4}</td>
												<td>${i.col5}</td>
											</tr>
											<p>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<%@include file="../CM/MsgFEUtils.jsp"%>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Nhập dữ liệu</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	rel="stylesheet">

</head>

<body>
	<hr>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
				<!--left col-->
				<%@include file="../CM/MenuFE.jsp"%>
			</div>
			<!--/col-3-->
			<div class="col-sm-9">
				<div class="container-fluid">
					<div class="row">
						<section class="col-12">
							<table class="rtable" style="width: 100%;">
								<thead>
									<tr>
										<th class="left">Nhập dữ liệu tạo mã thực hiện hàng loạt
										</th>
										<th class="right">Mã Trang: uploadExcel</th>
									</tr>

								</thead>

							</table>
						</section>

					</div>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Nhập dữ liệu</span>
					</div>
					<section class="col-12 border shadow p-2">
						<table class="rtable" style="width: 100%;">
							<thead>
								<tr>
									<th class="left"><input id="fileInput" type="file" /></th>
									<th class="right">

										<button class="btn btn-sm btn-outline-info" id="btnUpload">
											TẢI LÊN</button>
									</th>
								</tr>

							</thead>

						</table>
					</section>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Danh sách dữ liệu tải lên</span>
					</div>

					<div class="form-group">
						<div align="center" class="border shadow p-2">
							<table class="rtable" style="width: 100%;" id="tblShow1">

							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<%@include file="../CM/MsgFEUtils.jsp"%>
	<script>
		var headerArrayID_BATCH = [ {
			key : 'id_batch',
			value : 'Mã lô dữ liệu'
		}, {
			key : 'count_data',
			value : 'Số lượng tải lên'
		}, {
			key : 'time_update',
			value : 'Thời gian tải lên'
		},{
			key : 'act',
			value : 'hành động'
		} ];

		var btnUpload = document.getElementById("btnUpload");
		var fileInput = document.getElementById("fileInput");

		btnUpload.onclick = function() {
			console.log(fileInput);
			var file = fileInput.files[0];
			if (file) {
				var formData = new FormData();
				formData.append('file', file, file.name);
				new http.callApi("upload", formData, function(resp) {
					console.log(resp);
					if (Number(resp.returnCode) < 0) {
						MsgFEUtils.showToast(MsgFEUtils.ERROR, resp.msgDescs);
					} else {
						query();
						MsgFEUtils.showToast(MsgFEUtils.SUCCESS, resp.msgDescs,
								3000);
					}
				});
			} else {
				MsgFEUtils.showToast(MsgFEUtils.ERROR, "vui lòng chọn File",
						3000);
			}

		};

		function query() {
			new http.callApi("query", null, function(resp) {
				if (Number(resp.returnCode) < 0) {
					MsgFEUtils.showToast(MsgFEUtils.ERROR, resp.msgDescs);

				} else {
					console.log(resp);
					loadDataToTable(resp.returnData);
					MsgFEUtils.showToast(MsgFEUtils.SUCCESS, resp.msgDescs,
							3000);

					//loadDataToTable(resp.listPost);
				}
			});
		}
		function loadDataToTable(jsonData) {
			var table = $('<table>').attr('id', 'tblShow1');
			table.attr('class', 'table table-sm table-hover');
			var thead = $('<thead>');
			var tbody = $('<tbody>');

			// Tạo header từ keyObject
			var headerRow = $('<tr>');

			var allKeys = getAllUniqueKeys(jsonData, headerArrayID_BATCH);
			$.each(allKeys,
					function(index, key) {
						var header = headerArrayID_BATCH.find(function(item) {
							return item.key === key;
						});
						try {
							headerRow.append($('<th>').text(
									header.value.toUpperCase()));
						} catch (err) {
							headerRow.append($('<th>').text());
						}
					});
			thead.append(headerRow);

			$.each(jsonData, function(index, data) {
				var dataRow = $('<tr>');
				$.each(allKeys, function(index, key) {
					var header = headerArrayID_BATCH.find(function(item) {
						return item.key === key;
					});
					var value = data[key]; 
					dataRow.append($('<td>').html(value));
				});
				/* dataRow.click(function() {
					showFormEdit(data);
				}); */
				tbody.append(dataRow);
			});

			// Gắn thead và tbody vào bảng
			table.append(thead);
			table.append(tbody);
			// Hiển thị bảng trong phần tử có id là "tblShow1"
			$('#tblShow1').replaceWith(table);
		}
		// Hàm trợ giúp để lấy tất cả các trường duy nhất từ dữ liệu JSON
		function getAllUniqueKeys(jsonData, headerArray) {
			var allKeys = [];
			$.each(jsonData, function(index, data) {
				$.each(Object.keys(data), function(index, key) {
					if (!allKeys.includes(key)) {
						allKeys.push(key);
					}
				});
			});
			$.each(headerArray, function(index, header) {
				if (!allKeys.includes(header.key)) {
					allKeys.push(header.key);
				}
			});
			return allKeys;
		}
		query();
	</script>
</body>
</html>
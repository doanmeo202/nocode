<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Nhập dữ liệu</title>
<script
	src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"></script>

<style type="text/css">
.rtable {
	/*!
    // IE needs inline-block to position scrolling shadows otherwise use:
    // display: block;
    // max-width: min-content;
    */
	/* display: inline-block; */
	vertical-align: top;
	max-width: 100%;
	overflow-x: auto;
	white-space: nowrap;
	border-collapse: collapse;
	border-spacing: 0;
}

.rtable, .rtable--flip tbody {
	-webkit-overflow-scrolling: touch;
	background: radial-gradient(left, ellipse, rgba(0, 0, 0, 0.2) 0%,
		rgba(0, 0, 0, 0) 75%) 0 center,
		radial-gradient(right, ellipse, rgba(0, 0, 0, 0.2) 0%,
		rgba(0, 0, 0, 0) 75%) 100% center;
	background-size: 10px 100%, 10px 100%;
	background-attachment: scroll, scroll;
	background-repeat: no-repeat;
}

.rtable td:first-child, .rtable--flip tbody tr:first-child {
	background-image: -webkit-gradient(linear, left top, right top, color-stop(50%, white),
		to(rgba(255, 255, 255, 0)));
	background-image: linear-gradient(to right, white 50%, rgba(255, 255, 255, 0)
		100%);
	background-repeat: no-repeat;
	background-size: 20px 100%;
}

.rtable td:last-child, .rtable--flip tbody tr:last-child {
	background-image: -webkit-gradient(linear, right top, left top, color-stop(50%, white),
		to(rgba(255, 255, 255, 0)));
	background-image: linear-gradient(to left, white 50%, rgba(255, 255, 255, 0)
		100%);
	background-repeat: no-repeat;
	background-position: 100% 0;
	background-size: 20px 100%;
}

th {
	font-size: 11px;
	text-align: left;
	text-transform: uppercase;
	background: #f2f0e6;
}

td {
	padding: 6px 12px;
	border: 1px solid #d9d7ce;
}

.rtable th, .rtable td {
	padding: 6px 12px;
	border: 1px solid #d9d7ce;
}

.rtable--flip {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	overflow: hidden;
	background: none;
}

.rtable--flip thead {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-ms-flex-negative: 0;
	flex-shrink: 0;
	min-width: -webkit-min-content;
	min-width: -moz-min-content;
	min-width: min-content;
}

.rtable--flip tbody {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	position: relative;
	overflow-x: auto;
	overflow-y: hidden;
}

.rtable--flip tr {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: column;
	min-width: -webkit-min-content;
	min-width: -moz-min-content;
	min-width: min-content;
	-ms-flex-negative: 0;
	flex-shrink: 0;
}

.rtable--flip td, .rtable--flip th {
	display: block;
}

.rtable--flip td {
	background-image: none !important;
	border-left: 0;
}

.rtable--flip th:not(:last-child), .rtable--flip td:not(:last-child) {
	border-bottom: 0;
}

.center {
	text-align: center !important;
}

.left {
	text-align: left !important;
}

.right {
	text-align: right !important;
}

.text {
	font-size: 14px;
	text-transform: uppercase;
}
</style>
</head>
<body>

</body>
<body class="bg-light">
	<div class="container-fluid">
		<div class="row">
			<%@include file="CM/MenuFE.jsp"%>
			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

				<div class="container-fluid">
					<div class="row">
						<section class="col-12">
							<table class="rtable" style="width: 100%;">
								<thead>
									<tr>
										<th class="left">Nhập dữ liệu tạo mã thực hiện hàng loạt
										</th>
										<th class="right">Mã Trang: uploadExcel</th>
									</tr>

								</thead>

							</table>
						</section>

					</div>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Nhập dữ liệu</span>
					</div>
					<section class="col-12 border shadow p-2">
						<table class="rtable" style="width: 100%;">
							<thead>
								<tr>
									<th class="left"><input id="fileInput" type="file" /></th>
									<th class="right">

										<button class="btn btn-sm btn-outline-info" id="btnUpload">
											TẢI LÊN</button>
									</th>
								</tr>

							</thead>

						</table>
					</section>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Danh sách dữ liệu tải lên</span>
					</div>

					<div class="form-group">
						<div>
							<div align="center" class="border shadow p-2">
								<table class="rtable" style="width: 100%;">
									<thead>
										<tr>

											<th rowspan="2" class="center" style="width: 20%;">Trạng
												thái</th>
											<th colspan="2" class="center">Thời Gian</th>
											<th rowspan="2" style="width: 20%;" class="center">Hành
												động</th>
										</tr>
										<tr>
											<th class="center" style="width: 20%;">Từ Ngày</th>
											<th class="center" style="width: 20%;">Đến Ngày</th>

										</tr>

									</thead>
									<tbody>
										<tr class="center">

											<td><select id="cboSTATUS_TYPE"
												class="form-control form-select">
													<option value="" selected>Tất cả</option>
													<option value="0">Chưa xử lý</option>
													<option value="1">Đã Xử lý</option>

											</select></td>
											<td><input class="form-control form-input"
												id="START_DATE" name="START_DATE" placeholder="yyyy/MM/dd"
												type="text" /></td>
											<td><input class="form-control form-input" id="END_DATE"
												name="END_DATE" placeholder="yyyy/MM/dd" type="text" /></td>


											<td>
												<div class="input-group center" style="display: block">

													<button class="btn btn-sm btn-outline-info" id="btnSearch">
														TRA TÌM</button>

												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>

						<div align="center" class="border shadow p-2">
							<table class="rtable" style="width: 100%;" id="tblShow1">

							</table>
						</div>
					</div>
				</div>
			</main>
		</div>
	</div>


	<%@include file="CM/MsgFEUtils.jsp"%>
	<script>
		var http = {
			callApi : function(action, json, callback) {
				$.ajax({
					method : 'POST',
					url : action,
					dataType : "json",
					data : json,
					processData : false,
					contentType : false,
					beforeSend : function(x) {
						MsgFEUtils.showFrontLoading();
					},
					success : function(resp) {
						callback(resp); // Gọi hàm callback và truyền giá trị resp
					},
					error : function(error) {
						MsgFEUtils.showToast(MsgFEUtils.ERROR,
								"ERROR WHEN REQUEST !!!");
					},
					complete : function() {
						MsgFEUtils.hideFrontLoading();
					}
				});
			}
		};

		var btnUpload = document.getElementById("btnUpload");
		var fileInput = document.getElementById("fileInput");

		btnUpload.onclick = function() {
			console.log(fileInput);
			var file = fileInput.files[0];
			var formData = new FormData();
			formData.append('file', file, file.name);

			new http.callApi("${htmlBase}/api-excel/getData", formData,
					function(resp) {
						console.log(resp);
						if (Number(resp.returnCode) < 0) {
							MsgFEUtils.showToast(MsgFEUtils.ERROR,
									resp.msgDescs);
						} else {
							MsgFEUtils.showToast(MsgFEUtils.SUCCESS,
									resp.msgDescs, 3000);
						}
					});
		};
	</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Đăng nhập</title>
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600)
	;

body {
	margin: 0;
	font-size: .9rem;
	font-weight: 400;
	line-height: 1.6;
	color: #212529;
	text-align: left;
	background-color: #f5f8fa;
}

.navbar-laravel {
	box-shadow: 0 2px 4px rgba(0, 0, 0, .04);
}

.navbar-brand, .nav-link, .my-form, .login-form {
	font-family: Raleway, sans-serif;
}

.my-form {
	padding-top: 1.5rem;
	padding-bottom: 1.5rem;
}

.my-form .row {
	margin-left: 0;
	margin-right: 0;
}

.login-form {
	padding-top: 1.5rem;
	padding-bottom: 1.5rem;
}

.login-form .row {
	margin-left: 0;
	margin-right: 0;
}
</style>
<%@include file="CM/MsgFEUtils.jsp"%>
</head>
<body>
	<main class="login-form">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">Đăng Ký</div>
						<div class="card-body">
							<form action="" method="">
								<div class="form-group row">
									<label for="email_address"
										class="col-md-4 col-form-label text-md-right">Tài
										Khoản Address</label>
									<div class="col-md-6">
										<input type="text" id="email_address" class="form-control"
											name="email-address" required autofocus>
									</div>
								</div>

								<div class="form-group row">
									<label for="password"
										class="col-md-4 col-form-label text-md-right">Mật Khẩu</label>
									<div class="col-md-6">
										<input type="password" id="password" class="form-control"
											name="password" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="password"
										class="col-md-4 col-form-label text-md-right">Xác Nhận
										Mật Khẩu</label>
									<div class="col-md-6">
										<input type="password" id="password" class="form-control"
											name="password" required>
									</div>
								</div>

								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">Đăng Ký</button>

								</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">Đăng Nhập</div>
						<div class="card-body">
							<form action="" method="">
								<div class="form-group row">
									<label for="email_address"
										class="col-md-4 col-form-label text-md-right">Tài
										Khoản Address</label>
									<div class="col-md-6">
										<input type="text" id="email_address" class="form-control"
											name="email-address" required autofocus>
									</div>
								</div>

								<div class="form-group row">
									<label for="password"
										class="col-md-4 col-form-label text-md-right">Mật Khẩu</label>
									<div class="col-md-6">
										<input type="password" id="password" class="form-control"
											name="password" required>
									</div>
								</div>



								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">Đăng
										Nhập</button>

								</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>


	</main>
</body>



</html>

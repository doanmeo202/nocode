<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Trang chủ</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	rel="stylesheet">

</head>
<body>
	<hr>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
				<!--left col-->
				<%@include file="CM/MenuFE.jsp"%>
			</div>
			<!--/col-3-->
			<div class="col-sm-9">
				<div class="container-fluid">
					<div class="row">
						<section class="col-12">
							<table class="rtable" style="width: 100%;">
								<thead>
									<tr>
										<th class="left">Tramg chủ</th>
										<th class="right">Mã Trang: index</th>
									</tr>

								</thead>

							</table>
						</section>

					</div>
				</div>
				<div class="m-3">
					<div
						class="p-2 title-page-1 bg-success text-white form-group shadow-sm row-selector">
						<span>Bảng tin hoạt động</span>
					</div>
					<section class="col-12 border shadow p-2">
						<table class="rtable" style="width: 100%;">
							<thead>
								<tr>
									<th>Chưa có hoạt động nổi bật</th>
								</tr>
							</thead>
						</table>
					</section>
				</div>
				<!--/tab-pane-->
			</div>
			<!--/tab-content-->

		</div>
		<!--/col-9-->
	</div>
	<!--/row-->


	<script
		src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"></script>
	<%@include file="CM/MsgFEUtils.jsp"%>
</body>
</html>

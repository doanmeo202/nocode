app.directive("navMenu", function () {
    return {
        restrict: 'E',
        scope: {
            name: '@'
        },
        link: function (scope, el) {
            scope.$watch('name', function (newValue) {
                var menu = JSON.parse(sessionStorage.getItem("menu"));
                var apiGetMenu1 = proxy + `getMenu?WEB_USED=${newValue}`;
                $.when($.ajax({
                    url: apiGetMenu1,
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                })).then(function (success, error) {
                
                    var jsonData = JSON.stringify(success.data[0].menu);
                    if (jsonData) {
                        sessionStorage.setItem("menu", jsonData);
                    }
                    menu = success.data[0].menu;
                    const element = React.createElement(Menu, { menu: menu });
                    ReactDOM.createRoot(el[0]).render(element);
                })
                scope.$on('$destroy', function () {
                    ReactDOM.unmountComponentAtNode(el[0]);
                });
            });
        }
    };
})
app.directive("apitable", function () {
    return {
        restrict: 'E',
        scope: {
            data: '@',
            dataall: '@'
        },
        link: function (scope, el) {

            // Cập nhật component React khi thuộc tính name của directive thay đổi
            scope.$watch('data', function (newValue) {
                scope.$watch('dataall', function (newValue1) {
                   if(newValue&&newValue1){
                    const element = React.createElement(Table, { data: JSON.parse(newValue), dataAll: JSON.parse(newValue1) });
                    ReactDOM.createRoot(el[0]).render(element);
                   }
                });
                
               

            });
           


        }
    };
})
app.directive("batchcard", function () {
    return {
        restrict: 'E',
        scope: {
            jobs: '@'
        },
        link: function (scope, el) {
           
            scope.$watch('jobs', function (newValue) {
                if(newValue!=undefined&&newValue!=""){
                    
                    const element = React.createElement(BatchCardList, { jobs: JSON.parse(newValue)});
                        ReactDOM.createRoot(el[0]).render(element);
                }
                
            });
        }
    };
})




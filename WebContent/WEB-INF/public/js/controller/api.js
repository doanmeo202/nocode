function apiCtrl($scope, $state,$http) {
    var apiGetMenu1 = proxy + `getApiList?webUsed=sd`;
    var jsonData1 = JSON.parse(sessionStorage.getItem("dataApi1"));
    var URI = JSON.parse(sessionStorage.getItem("URI"));
    $scope.data = jsonData1;
    $scope.dataall = URI;
    $scope.items = [];
    $scope.result = '';
    $scope.formData = {};
    $scope.formData.items = [];
    $.when(
        $.ajax({
            url: apiGetMenu1,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false
        })
    ).then(function (returndata, error) {
        var success=returndata.data;
        console.log(success);
        jsonData1 = JSON.stringify(success[0].getDTZSAP01);
        URI = JSON.stringify(success[0].getURI);

        if (jsonData1) {
            sessionStorage.setItem('dataApi1', jsonData1);
        }
        if (URI) {
            sessionStorage.setItem('URI', URI);
        }
        
        dataApi1 = JSON.parse(jsonData1);
        $scope.data = dataApi1;
        $scope.dataall = URI;
        $scope.$apply();
    });
    $scope.addItem = function () {
        var nameLengt=document.getElementsByName('SER_NO').length;
        $scope.items.push({ PARAM: '', PARAM_TYPE: 'java.lang.String',SER_NO: Number(nameLengt)+1});
    };
    
    $scope.submitForm = function (x) {
       
        $scope.formData.items = [];
        $scope.formData.items = ($scope.items.slice());
        var formData =new FormData();
        formData.append("API_KEY",$scope.formData.API_KEY);
        formData.append("NEED_CHECK",$scope.formData.NEED_CHECK);
        formData.append("NEED_LOG",$scope.formData.NEED_LOG);
        formData.append("URI_PATH",$scope.formData.URI_PATH);
        formData.append("CALL_METHOD",$scope.formData.CALL_METHOD);
        formData.append("CALL_MOD",$scope.formData.CALL_MOD);
        formData.append("RETURN_TYPE",$scope.formData.RETURN_TYPE);
        formData.append("PARAM_NAME",$scope.formData.PARAM_NAME);
        formData.append("PARAM_TYPE",$scope.formData.PARAM_TYPE);
        formData.append("SER_NO",$scope.formData.SER_NO);
        formData.append("STATUS",x);
        
        $.ajax({
            url: proxy + `setDTZSAP01`,
            type: 'POST',
            cache: false,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (success) {
            console.log(success);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseJSON.returnMess);
        });
        
    };
    $scope.removeItem = function (index) {
        $scope.items.splice(index, 1);
    };

    $scope.dataFromReact = '';

    document.addEventListener('tableApi', function (event) {
        $scope.$apply(function () {
            var tempArr = event.detail;
            $scope.items = [];
            tempArr.forEach(element => {
                $scope.formData.API_KEY = element.api_key;
                $scope.formData.URI_PATH = element.uri_path;
                $scope.formData.NEED_LOG = element.need_log;
                $scope.formData.NEED_CHECK = element.need_check;
                $scope.formData.CALL_MOD = element.call_mod;
                if (element.param_name && element.param_type) {
                    var index = $scope.items.findIndex(function (item) {
                        return item.PARAM === element.param_name && item.PARAM_TYPE === element.param_type;
                    });
                    if (index === -1) {
                        $scope.items.push({ PARAM: element.param_name, PARAM_TYPE: element.param_type,SER_NO:element.ser_no });
                    }
                }

            });
        });
    });
}
app.controller('apiCtrl', apiCtrl);

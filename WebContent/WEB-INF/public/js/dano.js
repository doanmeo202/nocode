var http = {
	callApi : function(action, json, callback) {
		$.ajax({
			method : 'POST',
			url : action,
			dataType : "json",
			data : json,
			processData : false,
			contentType : false,
			beforeSend : function(x) {
				MsgFEUtils.showFrontLoading();
			},
			success : function(resp) {
				callback(resp); // Gọi hàm callback và truyền giá trị resp
			},
			error : function(error) {
				MsgFEUtils.showToast(MsgFEUtils.ERROR,
						"Có lỗi xảy ra vui lòng thực hiện lại sau!");
			},
			complete : function() {
				MsgFEUtils.hideFrontLoading();
			}
		});
	}
};

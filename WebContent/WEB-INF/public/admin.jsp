<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html ng-app="app">

<head ng-controller="headCtrl">
  <meta charset="UTF-8" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="assets\images\favicon.ico">
  <!-- Bootstrap Css -->
  <%
String htmlBase = request.getContextPath() + "";
String cssBase = request.getContextPath() + "/css";
String imageBase = request.getContextPath() + "/images";
String font = request.getContextPath() + "/fonts";

pageContext.setAttribute("htmlBase", htmlBase);
pageContext.setAttribute("cssBase", cssBase);
pageContext.setAttribute("imageBase", imageBase);
pageContext.setAttribute("font", font);

%>
  <link href="<%=htmlBase%>/assets\css\bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">
  <!-- Icons Css -->
  <link href="<%=htmlBase%>/assets\css\icons.min.css" rel="stylesheet" type="text/css">
  <!-- App Css-->
  <link href="<%=htmlBase%>/assets\css\app.min.css" id="app-style" rel="stylesheet" type="text/css">
  <script src="<%=htmlBase%>/js/babel.min.js"></script>
  <script src="<%=htmlBase%>/js/react.development.js"></script>
  <script src="<%=htmlBase%>/js/react-dom.development.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
  <script src="https://unpkg.com/@uirouter/angularjs/release/angular-ui-router.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"></script>
  <script src="<%=htmlBase%>/js/natural-cron.min.js"></script>
  <title ng-bind="title"></title>
  <meta ng-bind="description" content="{{description}}">

</head>

<body data-sidebar="dark">
  <div id="layout-wrapper">
    <header id="page-topbar">
      <div class="navbar-header">
        <div class="d-flex">
          <!-- LOGO -->
          <div class="navbar-brand-box">
            <a href="index.html" class="logo logo-dark">
              <span class="logo-sm">
                <img src="assets\images\logo.svg" alt="" height="22">
              </span>
              <span class="logo-lg">
                <img src="assets\images\logo-dark.png" alt="" height="17">
              </span>
            </a>

            <a href="index.html" class="logo logo-light">
              <span class="logo-sm">
                <img src="assets\images\logo-light.svg" alt="" height="22">
              </span>
              <span class="logo-lg">
                <img src="assets\images\logo-light.png" alt="" height="19">
              </span>
            </a>
          </div>

          <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
            <i class="fa fa-fw fa-bars"></i>
          </button>

          <!-- App Search-->
          <form class="app-search d-none d-lg-block">
            <div class="position-relative">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="bx bx-search-alt"></span>
            </div>
          </form>
        </div>

        <div class="d-flex">

          <div class="dropdown d-inline-block d-lg-none ml-2">
            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="mdi mdi-magnify"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
              aria-labelledby="page-header-search-dropdown">

              <form class="p-3">
                <div class="form-group m-0">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">
              <img class="" src="assets\images\flags\us.jpg" alt="Header Language" height="16">
            </button>
            <div class="dropdown-menu dropdown-menu-right">

              <!-- item-->
              <a href="javascript:void(0);" class="dropdown-item notify-item">
                <img src="assets\images\flags\spain.jpg" alt="user-image" class="mr-1" height="12"> <span
                  class="align-middle">Spanish</span>
              </a>

              <!-- item-->
              <a href="javascript:void(0);" class="dropdown-item notify-item">
                <img src="assets\images\flags\germany.jpg" alt="user-image" class="mr-1" height="12"> <span
                  class="align-middle">German</span>
              </a>

              <!-- item-->
              <a href="javascript:void(0);" class="dropdown-item notify-item">
                <img src="assets\images\flags\italy.jpg" alt="user-image" class="mr-1" height="12"> <span
                  class="align-middle">Italian</span>
              </a>

              <!-- item-->
              <a href="javascript:void(0);" class="dropdown-item notify-item">
                <img src="assets\images\flags\russia.jpg" alt="user-image" class="mr-1" height="12"> <span
                  class="align-middle">Russian</span>
              </a>
            </div>
          </div>

          <div class="dropdown d-none d-lg-inline-block ml-1">
            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <i class="bx bx-customize"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <div class="px-lg-2">
                <div class="row no-gutters">
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\github.png" alt="Github">
                      <span>GitHub</span>
                    </a>
                  </div>
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\bitbucket.png" alt="bitbucket">
                      <span>Bitbucket</span>
                    </a>
                  </div>
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\dribbble.png" alt="dribbble">
                      <span>Dribbble</span>
                    </a>
                  </div>
                </div>

                <div class="row no-gutters">
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\dropbox.png" alt="dropbox">
                      <span>Dropbox</span>
                    </a>
                  </div>
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\mail_chimp.png" alt="mail_chimp">
                      <span>Mail Chimp</span>
                    </a>
                  </div>
                  <div class="col">
                    <a class="dropdown-icon-item" href="#">
                      <img src="assets\images\brands\slack.png" alt="slack">
                      <span>Slack</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="dropdown d-none d-lg-inline-block ml-1">
            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
              <i class="bx bx-fullscreen"></i>
            </button>
          </div>

          <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="bx bx-bell bx-tada"></i>
              <span class="badge badge-danger badge-pill">3</span>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
              aria-labelledby="page-header-notifications-dropdown">
              <div class="p-3">
                <div class="row align-items-center">
                  <div class="col">
                    <h6 class="m-0"> Notifications </h6>
                  </div>
                  <div class="col-auto">
                    <a href="#!" class="small"> View All</a>
                  </div>
                </div>
              </div>
              <div data-simplebar="init" style="max-height: 230px;">
                <div class="simplebar-wrapper" style="margin: 0px;">
                  <div class="simplebar-height-auto-observer-wrapper">
                    <div class="simplebar-height-auto-observer"></div>
                  </div>
                  <div class="simplebar-mask">
                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                      <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden;">
                        <div class="simplebar-content" style="padding: 0px;">
                          <a href="" class="text-reset notification-item">
                            <div class="media">
                              <div class="avatar-xs mr-3">
                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                  <i class="bx bx-cart"></i>
                                </span>
                              </div>
                              <div class="media-body">
                                <h6 class="mt-0 mb-1">Your order is placed</h6>
                                <div class="font-size-12 text-muted">
                                  <p class="mb-1">If several languages coalesce the grammar</p>
                                  <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                </div>
                              </div>
                            </div>
                          </a>
                          <a href="" class="text-reset notification-item">
                            <div class="media">
                              <img src="assets\images\users\avatar-3.jpg" class="mr-3 rounded-circle avatar-xs"
                                alt="user-pic">
                              <div class="media-body">
                                <h6 class="mt-0 mb-1">James Lemire</h6>
                                <div class="font-size-12 text-muted">
                                  <p class="mb-1">It will seem like simplified English.</p>
                                  <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                </div>
                              </div>
                            </div>
                          </a>
                          <a href="" class="text-reset notification-item">
                            <div class="media">
                              <div class="avatar-xs mr-3">
                                <span class="avatar-title bg-success rounded-circle font-size-16">
                                  <i class="bx bx-badge-check"></i>
                                </span>
                              </div>
                              <div class="media-body">
                                <h6 class="mt-0 mb-1">Your item is shipped</h6>
                                <div class="font-size-12 text-muted">
                                  <p class="mb-1">If several languages coalesce the grammar</p>
                                  <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                </div>
                              </div>
                            </div>
                          </a>

                          <a href="" class="text-reset notification-item">
                            <div class="media">
                              <img src="assets\images\users\avatar-4.jpg" class="mr-3 rounded-circle avatar-xs"
                                alt="user-pic">
                              <div class="media-body">
                                <h6 class="mt-0 mb-1">Salena Layfield</h6>
                                <div class="font-size-12 text-muted">
                                  <p class="mb-1">As a skeptical Cambridge friend of mine occidental.</p>
                                  <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="simplebar-placeholder" style="width: 0px; height: 0px;"></div>
                </div>
                <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                  <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
                </div>
                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">
                  <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
                </div>
              </div>
              <div class="p-2 border-top">
                <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                  <i class="mdi mdi-arrow-right-circle mr-1"></i> View More..
                </a>
              </div>
            </div>
          </div>

          <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="rounded-circle header-profile-user" src="assets\images\users\avatar-1.jpg"
                alt="Header Avatar">
              <span class="d-none d-xl-inline-block ml-1">Henry</span>
              <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <!-- item-->
              <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i> Profile</a>
              <a class="dropdown-item" href="#"><i class="bx bx-wallet font-size-16 align-middle mr-1"></i> My
                Wallet</a>
              <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i
                  class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
              <a class="dropdown-item" href="#"><i class="bx bx-lock-open font-size-16 align-middle mr-1"></i> Lock
                screen</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item text-danger" href="#"><i
                  class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a>
            </div>
          </div>

        </div>
      </div>
    </header>
    <div class="vertical-menu">

      <div data-simplebar="init" class="h-100">
        <div class="simplebar-wrapper" style="margin: 0px;">
          <div class="simplebar-height-auto-observer-wrapper">
            <div class="simplebar-height-auto-observer"></div>
          </div>
          <div class="simplebar-mask">
            <div class="simplebar-offset" style="right: -15px; bottom: 0px;">
              <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                <div class="simplebar-content" style="padding: 0px;">

                  <!--- Sidemenu -->
                  <div id="sidebar-menu" class="mm-active">
                    <!-- Left Menu Start -->
                    <nav-menu name="Wadmin" class="sidebar-nav">
                    </nav-menu>
                  </div>
                  <!-- Sidebar -->
                </div>
              </div>
            </div>
          </div>
          <div class="simplebar-placeholder" style="width: auto; height: 1199px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
          <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
          <div class="simplebar-scrollbar"
            style="height: 674px; transform: translate3d(0px, 0px, 0px); display: block;"></div>
        </div>
      </div>
    </div>


    <div class="main-content">
      <div class="page-content">
        <div class="container-fluid" ui-view></div>
      </div>
    </div>
  </div>
  
  
</body>

<script src="<%=htmlBase%>/js/jquery.min.js"></script>
<script src="<%=htmlBase%>/component/Table.js" type="text/babel"></script>
<script src="<%=htmlBase%>/component/Menu.js" type="text/babel"></script>
<script src="<%=htmlBase%>/component/Carousel.js" type="text/babel"></script>
<script src="<%=htmlBase%>/component/Component.js" type="text/babel"></script>
<script src="<%=htmlBase%>/component/BatchCard.js" type="text/babel"></script>
<script src="<%=htmlBase%>/js/app.js"></script>
<script>
var proxy=`${proxy}`;
var socketProxy=`${proxySoket}`;
var apiGetMenu = proxy+`getMenu?WEB_USED=Wadmin`;
function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $.when($.ajax({
        url: apiGetMenu,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
    })).then(function (success, error) {
        var routes = JSON.stringify(success.data[0].menu);
        
        if (routes) {
            sessionStorage.setItem("menu", routes);
            JSON.parse(routes).forEach(element => {
              if(element.template_url!=null){
                $stateProvider.state(element.slug, {
                    url: element.slug,
                    templateUrl: element.template_url
                });
              }else{
                $stateProvider
                .state(element.slug, {
                    url: element.slug,
                    templateUrl: "view/404.html"
                })
              }
            });
        }
    })
    $stateProvider
        .state('/', {
            url: "/",
            templateUrl: "view/dashboad.html"
        })
}

app.config(config)

</script>
<script src="<%=htmlBase%>/js/directive.js"></script>
<script src="<%=htmlBase%>/js/controller.js"></script>
<script src="<%=htmlBase%>/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<%=htmlBase%>/dist/js/app-style-switcher.js"></script>
<script src="<%=htmlBase%>/dist/js/waves.js"></script>
<script src="<%=htmlBase%>/dist/js/custom.js"></script>
<script src="<%=htmlBase%>/js/controller/dashboad.js"></script>
<script src="<%=htmlBase%>/js/controller/api.js"></script>
<script src="<%=htmlBase%>/js/controller/batch.js"></script>
<script src="<%=htmlBase%>/assets\libs\jquery\jquery.min.js"></script>
<script src="<%=htmlBase%>/assets\libs\bootstrap\js\bootstrap.bundle.min.js"></script>
<script src="<%=htmlBase%>/assets\libs\metismenu\metisMenu.min.js"></script>
<script src="<%=htmlBase%>/assets\libs\simplebar\simplebar.min.js"></script>
<script src="<%=htmlBase%>/assets\libs\node-waves\waves.min.js"></script>
<script src="<%=htmlBase%>/assets\js\app.js"></script>

</html>